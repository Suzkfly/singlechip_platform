/*==============================================================================
文件名：soc_type.h
修改记录：
    - 朱凯飞 2018.09.27 创建
文件说明：单片机类型定义
==============================================================================*/
#define SOC_STAIR1_MASK 0xC0
#define STC89   (0 << 6)
#define STC12   (1 << 6)
#define STC15   (2 << 6)
#define STC8    (3 << 6)

#define SOC_STAIR2_MASK 0x30
#define STC15W  (STC15 | (0 << 4))
#define STC15F  (STC15 | (1 << 4))
#define STC15L  (STC15 | (2 << 4))

#define SOC_STAIR3_MASK 0x0F

/* STC15W系列 */
#define STC15W4K08S4  (STC15W | 0)
#define STC15W4K16S4  (STC15W | 1)
#define STC15W4K24S4  (STC15W | 2)
#define STC15W4K32S4  (STC15W | 3)
#define STC15W4K40S4  (STC15W | 4)
#define STC15W4K48S4  (STC15W | 5)
#define STC15W4K56S4  (STC15W | 6)
#define STC15W4K60S4  (STC15W | 7)
#define IAP15W4K61S4  (STC15W | 8)
#define IRC15W4K63S4  (STC15W | 9)
#define STC15W1K08PWM (STC15W | 10)
#define STC15W1K16PWM (STC15W | 11)

/* STC15F系列 */
#define STC15F2K08S2  (STC15F | 0)
#define STC15F2K16S2  (STC15F | 1)
#define STC15F2K24S2  (STC15F | 2)
#define STC15F2K32S2  (STC15F | 3)
#define STC15F2K40S2  (STC15F | 4)
#define STC15F2K48S2  (STC15F | 5)
#define STC15F2K56S2  (STC15F | 6)
#define STC15F2K60S2  (STC15F | 7)
#define STC15F2K32S   (STC15F | 8)
#define STC15F2K60S   (STC15F | 9)
#define STC15F2K24AS  (STC15F | 10)
#define STC15F2K48AS  (STC15F | 11)
#define IAP15F2K61S2  (STC15F | 12)
#define IRC15F2K63S2  (STC15F | 13)
#define IAP15F2K61S   (STC15F | 14)

/* STC15L系列 */
#define STC15L2K08S2  (STC15L | 0)
#define STC15L2K16S2  (STC15L | 1)
#define STC15L2K24S2  (STC15L | 2)
#define STC15L2K32S2  (STC15L | 3)
#define STC15L2K40S2  (STC15L | 4)
#define STC15L2K48S2  (STC15L | 5)
#define STC15L2K56S2  (STC15L | 6)
#define STC15L2K60S2  (STC15L | 7)
#define STC15L2K32S   (STC15L | 8)
#define STC15L2K60S   (STC15L | 9)
#define STC15L2K24AS  (STC15L | 10)
#define STC15L2K48AS  (STC15L | 11)
#define IAP15L2K61S2  (STC15L | 12)
#define IAP15L2K61S   (STC15L | 14)
