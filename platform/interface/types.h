/*==============================================================================
文件名：types.h
修改记录：
    - 朱凯飞 2018.05.24 创建
文件说明：数据类型定义
==============================================================================*/
#ifndef _TYPES_H
#define _TYPES_H

#ifndef uchar
#define uchar unsigned char
#endif

#ifndef uint
#define uint unsigned int
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef NULL
#define NULL    ((void *)0)
#endif

#define OK        0
#define ERROR   (-1)

/* 定义数据类型 */
typedef unsigned char uint8_t;
typedef char           int8_t;
typedef unsigned int   uint16_t;
typedef int            int16_t;
typedef unsigned long  uint32_t;
typedef long           int32_t;

#endif
