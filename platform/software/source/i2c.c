/*==============================================================================
文件名：i2c.c
修改记录：
    - 朱凯飞 2018.10.04 创建
文件说明：软件模拟I2C
==============================================================================*/
#include "i2c.h"
#include "intrins.h"

/**
 * \brief 延时10us（大概延时）
 */
static void __delay10us (void) 
{
    uint8_t i;

    i = 4;
    while (--i);
}

/**
 * \brief I2C引脚初始化，将引脚配置为推挽输出
 */
void i2c_pin_init (void)
{
    P2M1 &= ~0x03;
    P2M0 &= ~0x03;         /* 准双向IO */

    I2C_SDA = 1;
    I2C_SCL = 1;
}

/**
 * \brief I2C起始信号
 */
void i2c_start (void)
{
    I2C_SDA = 1;
    __delay10us();
    I2C_SCL = 1;
    __delay10us();
    I2C_SDA = 0;
    __delay10us();
}


/**
 * \brief I2C停止信号
 */
void i2c_stop (void)
{
    I2C_SDA = 0;
    __delay10us();
    I2C_SCL = 1;
    __delay10us();
    I2C_SDA = 1;
    __delay10us();
}


/**
 * \brief 主机发送应答位0
 */
void i2c_ack (void)
{
    I2C_SCL = 0;
    __delay10us();
    I2C_SDA = 0;
    __delay10us();
    I2C_SCL = 1;
    __delay10us();
    I2C_SCL = 0;
}


/**
 * \brief 主机发送非应答位1
 */
void i2c_nack (void)
{
    I2C_SCL = 0;
    __delay10us();
    I2C_SDA = 1;
    __delay10us();
    I2C_SCL = 1;
}

/**
 * \brief 发送一个字节
 */
int8_t i2c_send_byte (uint8_t byte)
{
    uint16_t i;
    uint8_t byte_flag, j=0;
    uint8_t ret = 0;

    byte_flag = byte;
    I2C_SCL = 0;
    __delay10us();
    for (i = 0; i < 8; i++) {
        I2C_SDA = byte_flag >> 7; /* 高位先送 */
        __delay10us();
        I2C_SCL = 1;
        byte_flag <<= 1;
        __delay10us();
        I2C_SCL = 0;
        __delay10us();
    }

    I2C_SDA = 1;
    __delay10us();
    I2C_SCL = 1;            /* 等待应答时时钟线为1 */
    i = 1;
    while (I2C_SDA && i) {   /* 等待从机发送应答位0 */
        i++;
        if (i > 5) {
            ret = -1;
            break;
        }
    }
    I2C_SCL = 0;

    return ret;
}


/**
 * \brief 读取一个字节
 */
uint8_t i2c_read_byte (void)
{
    uint8_t byte, i;
    I2C_SCL = 0;
    __delay10us();
    I2C_SDA = 1;  /* 释放数据线 */
    __delay10us();
    for(i = 0; i < 8; i++) {
        I2C_SCL = 1;
        __delay10us();
        byte <<= 1;
        byte |= I2C_SDA;    /* 高位先读 */
        I2C_SCL = 0;
        __delay10us();
    }
    
    return byte;
}