/*==============================================================================
文件名：delay_work.c
修改记录：
    - 朱凯飞 2018.09.27 创建
文件说明：定时器1用作延迟作业定时器
==============================================================================*/
#include "delay_work.h"
#include "includes.h"

/**
 * \brief 延迟作业初始化
 */
void delay_work_init (void)
{
#if (DELAY_WORK_TIMER == TIMER1)
    TMOD &= 0x0F;

    /* 如果使用的是STC89就设为工作模式1 */
    #if (SOC_TYPE & SOC_STAIR1_MASK == STC89)
        TMOD |= 0x10;    /* 设定定时器1为16位工作模式 */
    #endif
    
    TL1 = (uint16_t)(65536 - SYS_CLK / 12000) % 256;
    TH1 = (uint16_t)(65536 - SYS_CLK / 12000) / 256;
#elif (DELAY_WORK_TIMER == TIMER3)
    T4T3M &= ~T4T3M_T3_MASK;        /* 16位定时器；不输出时钟 */

    T3L = (uint16_t)(65536 - SYS_CLK / 12000) % 256;
    T3H = (uint16_t)(65536 - SYS_CLK / 12000) / 256;
#endif

}

/**
 * \brief 延迟作业开始
 */
void delay_work_start (void)
{
#if (DELAY_WORK_TIMER == TIMER1)
    ET1  = 1;
    EA   = 1;
    TR1  = 1; 
#elif (DELAY_WORK_TIMER == TIMER3)
    IE2 |= IE2_ET3;                 /* 使能定时器3溢出中断 */
    EA = 1;                         /* 使能总中断 */
    T4T3M |= T4T3M_T3R;             /* 定时器3开始运行 */
#endif   
}

/**
 * \brief 延迟作业停止
 */
void delay_work_stop (void)
{
#if (DELAY_WORK_TIMER == TIMER1)
    TR1 = 0;    
    ET1 = 0;
#elif (DELAY_WORK_TIMER == TIMER3)
    T4T3M &= ~T4T3M_T3R;          
    IE2 &= ~IE2_ET3; 
#endif
}

/* 指向第一个任务的指针 */
delay_work_reg_t *__gp_delay_work_first = NULL;

/**
 * \brief 注册一个延迟作业
 */
int8_t delay_work_register (delay_work_reg_t      *p_delay_work_arg, 
                            delay_work_server_t    delay_work_server,
                            void                 *p_arg,
                            uint16_t               ms)
{
    delay_work_reg_t *p_cur_task = NULL;

    /* 参数检查 */
    if ((p_delay_work_arg == NULL) || (ms == 0))
        return -1;

    p_delay_work_arg->delay_work_server = delay_work_server;
    p_delay_work_arg->ms  = ms;
    p_delay_work_arg->ticks_rest = ms;
    p_delay_work_arg->p_arg = p_arg;
    p_delay_work_arg->p_next = NULL;

    if (__gp_delay_work_first == NULL) {
        __gp_delay_work_first = p_delay_work_arg;
    } else {
        p_cur_task = __gp_delay_work_first;

        while (p_cur_task->p_next != NULL) {
            p_cur_task = p_cur_task->p_next;
        }
        p_cur_task->p_next = p_delay_work_arg;
    }

    return OK;
}

/**
 * \brief 注销一个已经注册的任务
 */
int8_t delay_work_unregister (delay_work_reg_t *p_delay_work_arg)
{
    delay_work_reg_t *p_cur_task = NULL;

    /* 参数检查 */
    if (p_delay_work_arg == NULL)
        return -1;

    if (p_delay_work_arg == __gp_delay_work_first) {
        __gp_delay_work_first = __gp_delay_work_first->p_next;
    } else {
        p_cur_task = __gp_delay_work_first;
        while ((p_cur_task->p_next != NULL) && (p_cur_task->p_next != p_delay_work_arg)){
            p_cur_task = p_cur_task->p_next;
        }
        if (p_cur_task->p_next == NULL) {
            return -1;
        } else {
            p_cur_task->p_next = p_cur_task->p_next->p_next;
        }
    }
}


/* 定时器中断服务函数 */
void timer_handler (void) 
#if (DELAY_WORK_TIMER == TIMER1)
interrupt 3
#elif (DELAY_WORK_TIMER == TIMER3)
interrupt 19
#endif
{
    delay_work_reg_t *p_cur_task = NULL;

    /* 如果使用的是STC89需要重新载入值 */
#if (SOC_TYPE & SOC_STAIR1_MASK == STC89)
    TL1 = (uint16_t)(65536 - SYS_CLK / 12000) % 256;
    TH1 = (uint16_t)(65536 - SYS_CLK / 12000) / 256;
#endif

    p_cur_task = __gp_delay_work_first;

    while (p_cur_task != NULL) {
        p_cur_task->ticks_rest--;
        if (p_cur_task->ticks_rest == 0) {
            /* p_cur_task->ticks_rest = p_cur_task->ms;     */
            delay_work_unregister(p_cur_task);  /* 注销掉当前任务 */
            p_cur_task->delay_work_server(p_cur_task->p_arg);
        }
        p_cur_task = p_cur_task->p_next;
    }
}
