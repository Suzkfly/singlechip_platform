/*==============================================================================
文件名：battry_per.c
修改记录：
    - 朱凯飞 2022.02.01 创建
文件说明：通过电池电压获取电池剩余电量百分比。只适用于若干节3.7V锂电池
==============================================================================*/
#include "battery_per.h"

/* 一节电池的电量剩余百分比与电压的对应关系 */
struct contrast {
    uint8_t per;     /* 百分比 */
    float   vol;     /* 电压 */
};

code struct contrast table[] = {{100, 4.2}, 
                                {90, 4.08}, 
                                {80, 4.0},
                                {70, 3.93},
                                {60, 3.87},
                                {50, 3.82},
                                {40, 3.79},
                                {30, 3.77},
                                {20, 3.73},
                                {15, 3.70},
                                {10, 3.68},
                                {5, 3.5},
                                {0, 2.5}};
/**
 * \brief 通过电池电压获取电池剩余电量百分比。
 *
 * \param[in] vol：电池电量
 * \param[in] cnt：电池节数
 *
 * \retval 电池电量百分比
 */
uint8_t get_battery_per(float vol, uint8_t cnt)
{
    uint8_t i = 0;
    float per = 0;
    float k = 0;
    float b = 0;

    vol /= cnt;     /* 得到单个电池的电量 */

    if (vol >= table[0].vol) {
        per = 100;
    } else if (vol <= table[sizeof(table) / sizeof(table[0]) - 1].vol) {
        per = 0;
    } else {
        for (i = 0; i < sizeof(table) / sizeof(table[0]); i++) {
            if(vol < table[i].vol && vol >= table[i+1].vol) {
                break;
            }
        }
        k = (table[i].per - table[i+1].per) / (table[i].vol - table[i+1].vol);
        b = table[i].per - table[i].vol * k;
        per = vol * k + b;
    }

    return (uint8_t)per;
}