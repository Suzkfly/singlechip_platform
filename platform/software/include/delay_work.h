/*==============================================================================
文件名：delay_work.h
修改记录：
    - 朱凯飞 2018.09.27 创建
文件说明：定时器1用作延迟作业定时器
==============================================================================*/
#ifndef __DELAY_WORK_H
#define __DELAY_WORK_H

#include "includes.h"

#define TIMER0  0
#define TIMER1  1
#define TIMER2  2
#define TIMER3  3
#define TIMER4  4

/* 定义延迟作业使用的定时器 */
#define DELAY_WORK_TIMER    TIMER3

#if (DELAY_WORK_TIMER == TIMER3)
#include "stc15_timer3.h"
#endif

/* 定时器服务函数类型 */
typedef void (* delay_work_server_t)(void *);


/* 定义注册的延迟作业参数类型 */
typedef struct delay_work_reg {
    delay_work_server_t    delay_work_server;   /* 需要注册的函数 */
    void                 *p_arg;                /* 传递给注册函数的参数 */
    uint16_t               ms;                  /* 每隔这么多毫秒调用一次注册的函数 */
    uint16_t               ticks_rest;          /* 剩余的节拍数 */
    struct delay_work_reg *p_next;              /* 下一个函数 */
} delay_work_reg_t;


/**
 * \brief 延迟作业初始化
 *
 * \note 延迟作业为1ms的整数倍，最小定时1ms
 */
extern void delay_work_init (void);

/**
 * \brief 延迟作业开始
 */
extern void delay_work_start (void);

/**
 * \brief 延迟作业停止
 */
//extern void delay_work_stop (void);

/**
 * \brief 注册一个延迟作业
 *
 * \param[in] p_delay_work_arg  : 指向任务参数的指针,对应的任务参数生存周期必须是整个程序的
 *                                生存周期（全局变量，静态变量和main函数中的变量）
 * \param[in] delay_work_server : 注册要执行的函数
 * \param[in] p_arg:            ：传递给执行函数的参数
 * \param[in] ms                : 要定时的毫秒数
 *                          
 * \retval 见int8_t类型
 *
 * \note 注册的ms不能为0且不能大于65535
 */
int8_t delay_work_register (delay_work_reg_t      *p_delay_work_arg, 
                            delay_work_server_t    delay_work_server,
                            void                 *p_arg,
                            uint16_t               ms);

/**
 * \brief 注销一个已经注册的任务
 *
 * \param[in] p_delay_work_arg : 要注销的任务
 */
//int8_t delay_work_unregister (delay_work_reg_t *p_delay_work_arg);

#endif
