/*==============================================================================
文件名：i2c.h
修改记录：
    - 朱凯飞 2018.10.04 创建
文件说明：软件模拟I2C，使用STC15系列单片机，在11.0292MHz频率下，I2C频率大约100K
==============================================================================*/
#ifndef __I2C_H
#define __I2C_H

#include "includes.h"

/* 定义I2C引脚 */
sbit I2C_SDA = P1^4;
sbit I2C_SCL = P1^3;

/**
 * \brief I2C引脚初始化
 *
 * \note 该函数实体要根据定义的I2C引脚进行修改
 */
extern void i2c_pin_init (void);

/**
 * \brief I2C起始信号
 */
extern void i2c_start (void);

/**
 * \brief I2C停止信号
 */
extern void i2c_stop (void);

/**
 * \brief 主机发送应答位0
 */
extern void i2c_ack (void);

/**
 * \brief 主机发送非应答位1
 */
extern void i2c_nack (void);
/**
 * \brief 发送一个字节
 *
 * \param[in] byte：要发送的字节
 *
 * \retval 0：发送成功 -1：发送失败（无应答）
 */
extern int8_t i2c_send_byte (uint8_t byte);

/**
 * \brief 读取一个字节
 *
 * \retval 接收到的字节
 */
extern uint8_t i2c_read_byte (void);

#endif