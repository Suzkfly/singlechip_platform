/*==============================================================================
文件名：battry_per.h
修改记录：
    - 朱凯飞 2022.02.01 创建
文件说明：通过电池电压获取电池剩余电量百分比。只适用于若干节3.7V锂电池
==============================================================================*/
#ifndef __BATTERY_PER_H
#define __BATTERY_PER_H

#include "types.h"

/**
 * \brief 通过电池电压获取电池剩余电量百分比。
 *
 * \param[in] vol：电池电量
 * \param[in] cnt：电池节数
 *
 * \retval 电池电量百分比
 */
extern uint8_t get_battery_per(float vol, uint8_t cnt);


#endif
