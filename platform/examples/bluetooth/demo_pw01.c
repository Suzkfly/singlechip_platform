/*==============================================================================
文件名：demo_pw01.c
修改记录：
    - 朱凯飞 2018.10.03创建
文件说明：PW01蓝牙模块包分析例程

操作步骤：
    1. 串口2（P1.0为RXD，P1.1为TXD）连接PW01模块，串口1连接电脑，波特率9600
    2. 在手机中打开蓝牙调试助手，发送的数据格式需与pw01.h中定义的一致
实验现象：
    1. 发送数据后返回执行状态
==============================================================================*/
#include "pw01.h"
#include "stc15_uart1.h"
#include "stdio.h"
#include "delay.h"

/**
 * \brief PW01蓝牙模块包分析例程入口
 */
void demo_pw01_entry (void)
{
    uint8_t pack_cnt;
    xdata uint8_t dat_buf[5][32] = { 0 };
    char show_buf[32] = { 0 };
    int temp;
    uint8_t i, j;

    stc15_uart1_init(9600);
    stc15_uart1_sendstr("system startup\r\n");

    pw01_init();

    while (1) {
        delay_ms(1000);
        pack_cnt = pw01_get_pack(dat_buf);
        if (pack_cnt > 0) {
            for (i = 0; i < pack_cnt; i++) {
                temp = i;
                sprintf(show_buf, "pack %d : ", temp);
                stc15_uart1_sendstr(show_buf);
                
                for (j = 0; j < dat_buf[i][2] + 4; j++) {
                    temp = dat_buf[i][j];
                    sprintf(show_buf, "%d ", temp);
                    stc15_uart1_sendstr(show_buf);
                }

                stc15_uart1_sendstr("\r\n");

                if (0 == pw01_sum_check(dat_buf[i])) {
                    stc15_uart1_sendstr("check sum OK\r\n");
                } else {
                    stc15_uart1_sendstr("check sum ERROR\r\n");
                }
            }
        }
    }
}

void uart1_interrupt (void) interrupt 4
{
    if (RI) {
        RI = 0;
    }
}
