/*==============================================================================
文件名：demo_delay_work_led.c
修改记录：
    - 朱凯飞 2018.09.27创建
文件说明：延迟作业测试例程

操作步骤：
    1. P1.0和P1.1各连接一个LED
实验现象：
    1. P1.0中的LED每隔500ms闪烁一次，P1.1中的LED每隔1000ms闪烁一次
==============================================================================*/
#include "delay_work.h"
#include "led.h"

/* 定义两个延时任务 */
static xdata delay_work_reg_t __g_led0_task;
static xdata delay_work_reg_t __g_led1_task;


void led0_toggle (void *p_arg)
{
    led_toggle((uint8_t)p_arg);
    delay_work_register(&__g_led0_task, led0_toggle, 0, 500);
}

void led1_toggle (void *p_arg)
{
    led_toggle((uint8_t)p_arg);
    delay_work_register(&__g_led1_task, led1_toggle, 1, 1000);
}

/**
 * \brief 延迟作业测试例程
 */
void demo_delay_work_led_entry (void)
{
    delay_work_init();
    delay_work_register(&__g_led0_task, led0_toggle, 0, 500);
    delay_work_register(&__g_led1_task, led1_toggle, 1, 1000);
    delay_work_start();
}

