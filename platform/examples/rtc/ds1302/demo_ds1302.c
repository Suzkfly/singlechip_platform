/*==============================================================================
文件名：demo_ds1302.c
修改记录：
    - 朱凯飞 2019.01.10 创建
文件说明：DS1302测试demo
操作步骤：
    1. 连接串口和串口调试助手，设置波特率为9600。（P3.0为RXD，P3.1为TXD）

实验现象：
    1. 串口调试助手上每隔1秒打印出时间，起始时间为2019-01-10 23:59:55 4
==============================================================================*/
#include <stdio.h>
#include "stc15_uart1.h"
#include "string.h"
#include "delay.h"
#include "ds1302.h"


void print_time (ds1302_time_t *p_time)
{
     char show_buf[32];
     char *p = show_buf;

     *p++ = '2';
     *p++ = '0';
     *p++ = (p_time->year >> 4) + '0';
     *p++ = (p_time->year & 0xF) + '0';
     *p++ = '-';

     *p++ = (p_time->mounth >> 4) + '0';
     *p++ = (p_time->mounth & 0xF) + '0';
     *p++ = '-';

     *p++ = (p_time->day >> 4) + '0';
     *p++ = (p_time->day & 0xF) + '0';
     *p++ = ' ';

     *p++ = (p_time->hour >> 4) + '0';
     *p++ = (p_time->hour & 0xF) + '0';
     *p++ = ':';

     *p++ = (p_time->min >> 4) + '0';
     *p++ = (p_time->min & 0xF) + '0';
     *p++ = ':';

     *p++ = (p_time->sec >> 4) + '0';
     *p++ = (p_time->sec & 0xF) + '0';
     *p++ = ' ';

     *p++ = (p_time->week & 0xF) + '0';
     *p++ = '\r';
     *p++ = '\n';
     *p++ = '\0';

     printf(show_buf);
}

/**
 * \brief DS1302测试例程入口
 */
void demo_ds1302_entry (void)
{
	ds1302_time_t time = {0x19, 0x01, 0x10, 0x23, 0x59, 0x55, 0x04};  /* 设定初始时间为2019-01-01 18:59:55 4 */
    uint8_t sec_old = 0;

    stc15_uart1_init(9600);
	ds1302_init();
	ds1302_write_time(&time);
    while (1) {
		ds1302_read_time(&time);
        if (sec_old != time.sec) {
            sec_old = time.sec;
            print_time(&time);
        }

		delay_ms(100);
    }
}
