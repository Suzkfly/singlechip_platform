/*==============================================================================
文件名：demo_motor1.c
修改记录：
    - 朱凯飞 2018.10.23创建
文件说明：直流电机测试程序，使用TC118S驱动

注：本测试程序使用的电机为智能锁中的电机
==============================================================================*/
#include "motor1.h"
#include "delay.h"

sbit KEY1 = P3^5;
sbit KEY2 = P3^4;

sbit TEST_YELLOW = P2^7;
sbit TEST_BLACK  = P2^6;

uint8_t is_locked()
{
    if ((TEST_YELLOW == 1) && (TEST_BLACK == 0))
        return 1;
    return 0;
}

uint8_t is_unlocked()
{
    if ((TEST_YELLOW == 0) && (TEST_BLACK == 1))
        return 1;
    return 0;
}

void lock()
{
    motor1_forward();
}

void unlock()
{
    motor1_back();
}

/**
 * \brief 直流电机测试例程
 */
void demo_motor1_entry (void)
{
	P0M1 = 0;
	P0M0 = 0x0c;
    while (1) {
#if 0
		if (KEY1 == 0) {
			motor1_forward();
		} else if (KEY2 == 0) {
            motor1_back();
        } else {
			motor1_brake();
        }
#else
            lock();
//            while (!is_locked());
//            motor1_brake();
//            delay_ms(1000);
//
//            unlock();
//            while (!is_unlocked());
//            motor1_brake();
//            delay_ms(1000);
#endif
    }
}