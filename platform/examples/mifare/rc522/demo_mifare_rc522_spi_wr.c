/*==============================================================================
文件名：demo_mifare_rc522_spi.c
修改记录：
    - 朱凯飞 2018.09.17创建
文件说明：RC522操作对mifare卡块进行读写例程（SPI接口）

操作步骤：
    1. 将RC522模块按如下方式连接：SS--P1.2 MOSI--P1.3  MISO--P1.4  SCLK--P1.5  RST--P1.6
    2. 连接串口（P3.0与TXD连接，P3.1与RXD连接），设定波特率为9600
    3. 将mifare卡置于天线感应区
实验现象：
    1. 见串口打印信息
==============================================================================*/
#include "mifare_rc522.h"
#include "delay.h"
#include "stc15_uart1.h"
#include "stdio.h"

#define BLOCK_NUM   10  /* 定义要操作的块号（不能将块号设为 n*4-1） */

#define DEBUG_INFO  stc15_uart1_sendstr

/**
 * \brief 将输入的字符按16进制打印，弥补sprintf的不足
 */
static void __print_buf (const uint8_t *p_buf, uint8_t len)
{
    uint8_t i = 0;
    uint8_t show = 0;

    for (i = 0; i < len; i++) {
        show = (p_buf[i] >> 4) & 0xF;      /* 显示高位 */
        if (show < 0xA) {
            stc15_uart1_sendbyte(show + '0');
        } else {
            stc15_uart1_sendbyte(show + 'a' - 10);
        }

        show = p_buf[i] & 0xF;            /* 显示低位 */
        if (show < 0xA) {
            stc15_uart1_sendbyte(show + '0');
        } else {
            stc15_uart1_sendbyte(show + 'a' - 10);
        }

        stc15_uart1_sendbyte(' ');
    }
}    

/**
 * \brief SPI主机模式测试例程
 */
void demo_mifare_rc522_spi_wr_entry (void)
{
    xdata uint8_t p_req[2] = { 0 };
    xdata uint8_t p_uid[4] = { 0 };
    uint8_t sak = 0;
   	xdata uint8_t keya[6] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
    uint8_t status = 0;
    xdata uint8_t block_data[16] = { 0 };
    uint8_t i;

    stc15_uart1_init(9600);
    DEBUG_INFO("system started\r\n");

    /* 注意：SPI配置：高位先传；空闲时钟为低电平；前沿采样，后沿输出 */
    rc522_init();

    while (1) {
  
		status = rc522_picca_request(PICCA_REQALL, p_req);/* 寻卡，得到卡类型 */
        if (status == MIFARE_ST_OK) {
            DEBUG_INFO("ATQA:");
            __print_buf(p_req, sizeof(p_req));
            DEBUG_INFO("\r\n");

            status = rc522_picca_anticoll(p_uid);        /* 防冲撞，得到UID */
            if (status == MIFARE_ST_OK) {
                DEBUG_INFO("UID:");
                __print_buf(p_uid, sizeof(p_uid));
                DEBUG_INFO("\r\n");

                status = rc522_picca_select(p_uid, &sak);      /* 选卡，选卡成功后可以对卡片进行秘钥验证 */
                if (status == MIFARE_ST_OK) {
                    DEBUG_INFO("SAK:");
                    __print_buf(&sak, 1);
                    DEBUG_INFO("\r\n");

                    status = rc522_picca_authent(PICCA_AUTHENT1A, p_uid, keya, BLOCK_NUM);  /* 卡秘钥验证 */
                    if (status == MIFARE_ST_OK) {
                        DEBUG_INFO("picca authent success\r\n");

                        /* 设定要写入卡的数据 */
                        for (i = 0; i < 16; i++) {
                            block_data[i] = i;
                        }

                        status = rc522_picca_write(BLOCK_NUM, block_data);    /* 写数据 */
                        if (status == MIFARE_ST_OK) {
                            DEBUG_INFO("write block data:");
                            __print_buf(block_data, sizeof(block_data));
                            DEBUG_INFO("\r\n");

                            /* 清空数据 */
                            for (i = 0; i < 16; i++) {
                                block_data[i] = 0;
                            }
                            
                            status = rc522_picca_read(BLOCK_NUM, block_data); /* 读数据 */
                            if (status == MIFARE_ST_OK) {
                                DEBUG_INFO("read block data:");
                                __print_buf(block_data, sizeof(block_data));
                                DEBUG_INFO("\r\n");

                                status = rc522_picca_halt();
                                if (status == MIFARE_ST_OK) {
                                    DEBUG_INFO("halt success\r\n");
                                } else {
                                    DEBUG_INFO("halt failed\r\n");
                                }
                            } else {
                                DEBUG_INFO("picca read failed\r\n");
                            }
                        } else {
                            DEBUG_INFO("picca write failed\r\n");
                        }
                    } else {
                        DEBUG_INFO("picca authent failed\r\n");
                    }
                } else {
                    DEBUG_INFO("picca select failed\r\n");
                }
            } else {
                DEBUG_INFO("picca anticoll failed\r\n");
            } 
        } else {
            DEBUG_INFO("picca request failed\r\n");
        }

        DEBUG_INFO("\r\n");

        delay_ms(1000);
    }
}

/**
 * \brief 串口1中断函数 
 */
void stc15_uart1_interrupt (void) interrupt 4
{
    if (RI) {
        RI = 0;
    }
}

