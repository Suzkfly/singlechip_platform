/*==============================================================================
demo_im1266.c
修改记录：
    - 朱凯飞 2022.04.25创建
文件说明：IM1266测试程序。IM1266是电能计量模块，可以测量电压、电流、功率、电能等参数。

操作步骤：
    1. 将串口1连接到电脑，当做调试串口
    2. 将串口3连接到IM1266模块，IM1266供电3.3V或者5V都可以
实验现象：
    1. 每隔2秒在调试串口打印出电压、电流、功率、电能等信息
==============================================================================*/
#include "stc15_uart1.h"
#include "stc15_uart3.h"
#include "im1266.h"
#include "delay.h"
#include <stdio.h>

/**
 * \brief AT24C256测试程序入口
 */
void demo_im1266_entry (void)
{
	im1266_dat_t dat;

    //delay_ms(2000);             /* 延时2S等待电源稳定 */
	stc15_uart1_init(9600);
	im1266_init(UART3);

    if (0 == im1266_electricity_clear(UART3)) {
        printf("electricity clear succeed\r\n");
    } else {
        printf("electricity clear failed\r\n");
    }
	
    while (1) {
		delay_ms(2000);
		im1266_get_all_dat(UART3, &dat);

		printf("voltage = %fV\r\n", dat.voltage);
		printf("current = %fA\r\n", dat.current);
		printf("active_power = %fW\r\n", dat.active_power);
		printf("electricity = %fkWH\r\n", dat.electricity);
		printf("power_factor = %f\r\n", dat.power_factor);
		printf("co2 = %fkg\r\n", dat.co2);
		printf("temperature = %f℃\r\n", dat.temperature);
		printf("frequency = %fHz\r\n", dat.frequency);
	}
}