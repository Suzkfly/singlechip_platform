/*==============================================================================
文件名：demo_eint_led.h
修改记录：
    - 朱凯飞 2018.05.23 创建
对应电路：sch1
例程说明：按下KEY3，LED1翻转；按下KEY4，LED2翻转。
==============================================================================*/
#include "includes.h"
#include "led.h"
#include "stc89_interrupt.h"
#include "stc89_eint.h"

/* LED0翻转 */
static void __led0_toggle (void *p_arg)
{
    uint8_t *p = p_arg;          /* 防止编译警告 */
    led_toggle(0);
}

/* LED1翻转 */
static void __led1_toggle (void *p_arg)
{
    uint8_t *p = p_arg;          /* 防止编译警告 */
    led_toggle(1);
}

/**
 * \brief 外部中断控制LED例程入口
 */
void demo_eint_led_entry (void)
{
    /* 连接外部中断0回调函数 */
    interrupt_cb_connect(INT_VECTOR_EINT0,  
                         __led0_toggle,
                         NULL);
    eint0_trigger_set(EINT_TRIGGER_FALL);    /* 下降沿触发      */
    interrupt_enable(INT_VECTOR_EINT0);      /* 使能外部中断0   */
    interrupt_unlock();                      /* 使能总中断      */

    /* 连接外部中断1回调函数 */
    interrupt_cb_connect(INT_VECTOR_EINT1,  
                         __led1_toggle,
                         NULL);
    eint1_trigger_set(EINT_TRIGGER_FALL);    /* 下降沿触发      */
    interrupt_enable(INT_VECTOR_EINT1);      /* 使能外部中断1   */
    interrupt_unlock();                      /* 使能总中断      */
    while (1);
}