/*==============================================================================
文件名：demo_mc20_tcp_entry.c
修改记录：
    - 朱凯飞 2018.09.27创建
文件说明：MC20 TCP测试例程

操作步骤：
    1. P1.0和P1.1各连接一个LED
实验现象：
    1. P1.0中的LED每隔500ms闪烁一次，P1.1中的LED每隔1000ms闪烁一次
==============================================================================*/
//todo
#include "mc20.h"
#include "stc15_uart3.h"

/**
 * \brief MC20 TCP测试例程
 */
void demo_mc20_tcp_entry (void)
{
    uint8_t once_init = 0;

    stc15_uart3_init(9600);
    mc20_init();

    while (1) {
        if ((once_init == 0) && mc20_baud_init_is_finished()) {
             once_init = 1;

             
        }
    }
}