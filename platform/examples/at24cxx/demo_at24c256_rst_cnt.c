/*==============================================================================
demo_at24c256_rst_cnt.c
修改记录：
    - 朱凯飞 2018.10.04创建
文件说明：AT24C256测试程序

操作步骤：
    1. 连接AT24C256模块和串口1
实验现象：
    1. 每次复位后串口1打印出从AT24C256的0地址中读取出的数据，并将加一的值写回
==============================================================================*/
#include "stc15_uart1.h"
#include "at24cxx.h"

/**
 * \brief AT24C256测试程序入口
 */
void demo_at24c256_rst_cnt_entry (void)
{
	uchar value;

    stc15_uart1_init(9600);
    i2c_pin_init();

	value = at24cxx_read_byte(0x00);
	at24cxx_write_byte(0x00, value + 1);

    stc15_uart1_sendbyte(value);

	while(1);		
}