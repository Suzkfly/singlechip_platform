/*==============================================================================
文件名：stc89_eint.h
修改记录：
    - 朱凯飞 2018.05.23 创建
文件说明：外部中断驱动
==============================================================================*/
#include "includes.h"

/**
 * \area 函数裁剪
 * @{
 */
//#define 
/**
 * @}
 */

/**< \brief 定义中断回调函数类型 */
typedef void (* pfn_interrupt_cb_t) (void *p_arg);

/**
 * \area 外部中断触发方式
 * @{
 */
#define EINT_TRIGGER_LOW    0   /**< \brief 低电平触发 */
#define EINT_TRIGGER_FALL   1   /**< \brief 下降沿触发 */
/**
 * @}
 */

/**
 * \brief 设定外部中断0触发方式
 *
 * \param[in] mode : 触发方式
 *              #EINT_TRIGGER_LOW   低电平触发
 *              #EINT_TRIGGER_FALL  下降沿触发
 */
void eint0_trigger_set (uint8_t mode);

/**
 * \brief 设定外部中断1触发方式
 *
 * \param[in] mode : 触发方式
 *              #EINT_TRIGGER_LOW   低电平触发
 *              #EINT_TRIGGER_FALL  下降沿触发
 */
void eint1_trigger_set (uint8_t mode);