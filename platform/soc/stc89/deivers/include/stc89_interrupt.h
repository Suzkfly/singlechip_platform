/*==============================================================================
文件名：stc89_interrupt.h
修改记录：
    - 朱凯飞 2018.05.23 创建
文件说明：STC89系列单片机包含4个外部中断，3个定时器中断，1个串口中断。
==============================================================================*/
#include "includes.h"

/**< \brief 定义STC89系列单片机包含的中断个数 */
#define STC89_INTERRUPT_CNT  8 

/**< \brief 定义外部中断回调函数类型 */
typedef void (* pfn_interrrupt_cb_t) (void *p_arg);

/**< \brief 定义中断服务 */
typedef struct interrupt_server {
    pfn_interrrupt_cb_t pfn;    /* 中断回调函数 */
    void *p_arg;                /* 给中断回调函数传递的参数 */
}interrupt_server_t;

/**
 * \area 函数裁剪
 * @{
 */
#define EN_FUNC_INTERRUPT_ENABLE
//#define EN_FUNC_INTERRUPT_DISABLE
#define EN_FUNC_INTERRUPT_UNLOCK
//#define EN_FUNC_INTERRUPT_LOCK
#define EN_FUNC_INTERRUPT_CB_CONNECT
/**
 * @}
 */  

/**
 * \area 定义中断向量号
 * @{
 */
#define INT_VECTOR_EINT0     0  /* 外部中断0   */
#define INT_VECTOR_TIMER0    1  /* 定时器中断0 */
#define INT_VECTOR_EINT1     2  /* 外部中断1   */
#define INT_VECTOR_TIMER1    3  /* 定时器中断1 */
#define INT_VECTOR_UART1     4  /* 串口中断1   */
#define INT_VECTOR_TIMER2    5  /* 定时器中断2 */
#define INT_VECTOR_EINT2     6  /* 外部中断2   */
#define INT_VECTOR_EINT3     7  /* 外部中断3   */
/**
 * @}
 */

/**
 * \brief 中断使能
 *
 * \param[in] int_vector : 中断向量号
 */ 
void interrupt_enable (uint8_t int_vector);

/**
 * \brief 中断禁能
 *
 * \param[in] int_vector : 中断向量号
 */ 
void interrupt_disable (uint8_t int_vector);

/**
 * \brief 解除中断锁定（开启总中断） 
 */
void interrupt_unlock (void);

/**
 * \brief 中断锁定（关闭总中断） 
 */
void interrupt_lock (void);

/**
 * \brief 连接中断回调函数
 *
 * \param[in] int_vector : 中断向量号
 * \param[in] pfn_cb     : 中断回调函数
 * \param[in] p_arg      : 中断回调函数的参数
 *
 * \return  OK：连接成功 ERROR：连接失败
 */
int8_t interrupt_cb_connect (uint8_t int_vector, 
                             pfn_interrrupt_cb_t pfn_cb, 
                             void *p_arg);