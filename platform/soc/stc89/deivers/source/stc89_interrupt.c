/*==============================================================================
文件名：stc89_interrupt.c
修改记录：
    - 朱凯飞 2018.05.23 创建
文件说明：该文件定义了所有中断的中断回调函数以及连接中断回调函数的接口。
==============================================================================*/
#include "stc89_interrupt.h"

/**< \brief 定义中断回调函数指针 */
static interrupt_server_t __g_interrupt_server[STC89_INTERRUPT_CNT];

/* 中断使能 */
#ifdef EN_FUNC_INTERRUPT_ENABLE 
void interrupt_enable (uint8_t int_vector)
{
    switch (int_vector) {
        case INT_VECTOR_EINT0 :
            EX0 = 1;
        break;

        case INT_VECTOR_TIMER0 :
            ET0 = 1;
        break;

        case INT_VECTOR_EINT1 :
            EX1 = 1;
        break;

        case INT_VECTOR_TIMER1 :
            ET1 = 1;
        break;

        case INT_VECTOR_UART1 :
            ES = 1;
        break;

        case INT_VECTOR_TIMER2 :
            ET2 = 1;
        break;

        case INT_VECTOR_EINT2 :
            XICON |= (1 << 2);
        break;

        case INT_VECTOR_EINT3 :
            XICON |= (1 << 6);
        break;
    }
}
#endif

/* 中断禁能 */
#ifdef EN_FUNC_INTERRUPT_DISABLE 
void interrupt_disable (uint8_t int_vector)
{
    switch (int_vector) {
        case INT_VECTOR_EINT0 :
            EX0 = 0;
        break;

        case INT_VECTOR_TIMER0 :
            ET0 = 0;
        break;

        case INT_VECTOR_EINT1 :
            EX1 = 0;
        break;

        case INT_VECTOR_TIMER1 :
            ET1 = 0;
        break;

        case INT_VECTOR_UART1 :
            ES = 0;
        break;

        case INT_VECTOR_TIMER2 :
            ET2 = 0;
        break;      

        case INT_VECTOR_EINT2 :
            XICON &= ~(1 << 2);
        break;

        case INT_VECTOR_EINT3 :
            XICON &= ~(1 << 6);
        break;
    }
}
#endif

/* 解除中断锁定（开启总中断） */
#ifdef EN_FUNC_INTERRUPT_UNLOCK
void interrupt_unlock (void)
{
    EA = 1;
}
#endif

/* 中断锁定（关闭总中断） */
#ifdef EN_FUNC_INTERRUPT_LOCK
void interrupt_lock (void)
{
    EA = 0;
}
#endif

/* 连接中断回调函数 */
#ifdef EN_FUNC_INTERRUPT_CB_CONNECT
int8_t interrupt_cb_connect (uint8_t int_vector, 
                             pfn_interrrupt_cb_t pfn_cb, 
                             void *p_arg)
{
    /* 参数检查 */
    if ((int_vector >= STC89_INTERRUPT_CNT) || 
        (pfn_cb == NULL)){
        return ERROR;
    }

    __g_interrupt_server[int_vector].pfn   = pfn_cb;
    __g_interrupt_server[int_vector].p_arg = p_arg;

    return OK;
}
#endif

void eint0_handler (void) interrupt INT_VECTOR_EINT0
{
    if (__g_interrupt_server[INT_VECTOR_EINT0].pfn) {
        __g_interrupt_server[INT_VECTOR_EINT0].pfn(__g_interrupt_server[INT_VECTOR_EINT0].p_arg);
    }
}

void eint1_handler (void) interrupt INT_VECTOR_EINT1
{
    if (__g_interrupt_server[INT_VECTOR_EINT1].pfn) {
        __g_interrupt_server[INT_VECTOR_EINT1].pfn(__g_interrupt_server[INT_VECTOR_EINT1].p_arg);
    }
}

void eint2_handler (void) interrupt INT_VECTOR_EINT2
{
    if (__g_interrupt_server[INT_VECTOR_EINT2].pfn) {
        __g_interrupt_server[INT_VECTOR_EINT2].pfn(__g_interrupt_server[INT_VECTOR_EINT2].p_arg);
    }
}

void eint3_handler (void) interrupt INT_VECTOR_EINT3
{
    if (__g_interrupt_server[INT_VECTOR_EINT3].pfn) {
        __g_interrupt_server[INT_VECTOR_EINT3].pfn(__g_interrupt_server[INT_VECTOR_EINT3].p_arg);
    }
}

void tiemr0_handler (void) interrupt INT_VECTOR_TIMER0
{
    if (__g_interrupt_server[INT_VECTOR_TIMER0].pfn) {
        __g_interrupt_server[INT_VECTOR_TIMER0].pfn(__g_interrupt_server[INT_VECTOR_TIMER0].p_arg);
    }
}

void tiemr1_handler (void) interrupt INT_VECTOR_TIMER1
{
    if (__g_interrupt_server[INT_VECTOR_TIMER1].pfn) {
        __g_interrupt_server[INT_VECTOR_TIMER1].pfn(__g_interrupt_server[INT_VECTOR_TIMER1].p_arg);
    }
}

void tiemr2_handler (void) interrupt INT_VECTOR_TIMER2
{
    if (__g_interrupt_server[INT_VECTOR_TIMER2].pfn) {
        __g_interrupt_server[INT_VECTOR_TIMER2].pfn(__g_interrupt_server[INT_VECTOR_TIMER2].p_arg);
    }
}

void uart1_handler (void) interrupt INT_VECTOR_UART1
{
    if (__g_interrupt_server[INT_VECTOR_UART1].pfn) {
        __g_interrupt_server[INT_VECTOR_UART1].pfn(__g_interrupt_server[INT_VECTOR_UART1].p_arg);
    }
}


