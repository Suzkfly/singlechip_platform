/*==============================================================================
文件名：stc89_eint.c
修改记录：
    - 朱凯飞 2018.05.23 创建
文件说明：外部中断驱动
==============================================================================*/
#include "stc89_eint.h"

/**
 * \brief 设定外部中断0触发方式
 */
void eint0_trigger_set (uint8_t mode)
{
    IT0 = mode;
}

/**
 * \brief 设定外部中断1触发方式
 */
void eint1_trigger_set (uint8_t mode)
{
    IT1 = mode;
}


