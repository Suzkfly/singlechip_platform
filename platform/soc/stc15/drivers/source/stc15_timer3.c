/*==============================================================================
文件名：stc15_timer3.c
修改记录：
    - 朱凯飞 2018.09.16 创建
文件说明：定时器3驱动程序
==============================================================================*/
#include "stc15_timer3.h"
#include "prj_config.h"

/**
 * \brief 初始化定时器3
 */
void stc15_timer3_init (double overflow_us)
{
    T4T3M &= ~T4T3M_T3_MASK;        /* 16位定时器；不输出时钟 */

    if (T4T3M & T4T3M_T3x12) {      /* 不分频 */
        T3L = (uint16_t)(65536 - overflow_us * SYS_CLK / 1000000) % 256;
        T3H = (uint16_t)(65536 - overflow_us * SYS_CLK / 1000000) / 256;
    } else {                        /* 分频 */
        T3L = (uint16_t)(65536 - overflow_us * SYS_CLK / 12000000) % 256;
        T3H = (uint16_t)(65536 - overflow_us * SYS_CLK / 12000000) / 256;
    }

    IE2 |= IE2_ET3;                 /* 使能定时器3溢出中断 */
    EA = 1;                         /* 使能总中断 */
    T4T3M |= T4T3M_T3R;             /* 定时器3开始运行 */
}