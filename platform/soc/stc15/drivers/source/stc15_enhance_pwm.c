/*==============================================================================
文件名：stc15_enhance_pwm.c
修改记录：
    - 朱凯飞 2018.10.08 创建
文件说明：PWM驱动文件
==============================================================================*/
#include "stc15_enhance_pwm.h"

/**
 * \brief PWM初始化
 */
void stc15_enhance_pwm_init (void)
{
    /* 初始化PWM管脚 */
    P3M0 &= ~(1 << 7);
    P3M1 &= ~(1 << 7);
    P2M0 &= ~(0x7 << 1);
    P2M1 &= ~(0x7 << 1);
    P1M0 &= ~(0x3 << 6);
    P1M1 &= ~(0x3 << 6);

    P_SW2 |= P_SW2_EAXSFR;                  /* 使能访问扩展RAM区域中的寄存器 */
    
    /* 配置PWM通道6,4,2初始电平为高电平 */
    PWMCFG = PWMCFG_C6INI_HIGH | PWMCFG_C4INI_HIGH | PWMCFG_C2INI_HIGH;
        
    PWMIF = 0;      /* 禁能PWM中断 */
    PWMFDCR = 0;    /* 禁能外部异常 */
    
    PWMCKS = 11;    /* 选择时钟源为系统时钟12分频 */
    
    /* 全部不使能中断，PWM引脚使用第一组引脚 */
    PWM7CR = 0;     /* PWM7--P1.7 */
    PWM6CR = 0;     /* PWM6--P1.6 */
    PWM5CR = 0;     /* PWM5--P2.3 */
    PWM4CR = 0;     /* PWM4--P2.2 */
    PWM3CR = 0;     /* PWM3--P2.1 */
    PWM2CR = 0;     /* PWM2--P3.7 */
    
    PWMC = 3000;    /* 设置PWM周期（16位寄存器，低15位有效） */
    
    /* 预留3个时钟作为死区时间 */
    PWM7T1 = 3;     /* 设置PWM7第1次反转的PWM计数 */
    PWM7T2 = 997;   /* 设置PWM7第2次反转的PWM计数 */
    PWM6T1 = 0;     /* 设置PWM6第1次反转的PWM计数 */
    PWM6T2 = 1000;  /* 设置PWM6第2次反转的PWM计数 */
                                 
    /* 预留3个时钟作为死区时间 */
    PWM5T1 = 1003;  /* 设置PWM5第1次反转的PWM计数 */
    PWM5T2 = 1997;  /* 设置PWM5第2次反转的PWM计数 */
    PWM4T1 = 1000;  /* 设置PWM4第1次反转的PWM计数 */
    PWM4T2 = 2000;  /* 设置PWM4第2次反转的PWM计数 */
                                 
    /* 预留3个时钟作为死区时间 */
    PWM3T1 = 2003;   /* 设置PWM3第1次反转的PWM计数 */
    PWM3T2 = 2997;   /* 设置PWM3第2次反转的PWM计数 */
    PWM2T1 = 2000;   /* 设置PWM2第1次反转的PWM计数 */
    PWM2T2 = 3000;   /* 设置PWM2第2次反转的PWM计数 */
                                 
    /* 使能6路PWM */
    PWMCR = PWMCR_ENC7O | PWMCR_ENC6O | PWMCR_ENC5O | PWMCR_ENC4O | PWMCR_ENC3O | PWMCR_ENC2O;
    PWMCR |= PWMCR_ENPWM;         /* 使能增强型PWM发生器 */
    
    P_SW2 &= ~P_SW2_EAXSFR;       /* 禁能访问扩展RAM区域中的寄存器 */
}


