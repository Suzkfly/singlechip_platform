/*==============================================================================
文件名：stc15_timer0.c
修改记录：
    - 朱凯飞 2018.09.16 创建
文件说明：定时器0驱动程序
==============================================================================*/
#include "stc15_timer0.h"
#include "prj_config.h"

/**
 * \brief 初始化定时器0
 */
void stc15_timer0_init (double overflow_us)
{
    TMOD &= ~TMOD_T0_MASK;

    AUXR &= ~AUXR_T0x12;            /* 定时器12分频 */

    INT_CLKO &= ~INT_CLKO_T0CLKO;   /* 不对外输出时钟 */

    if (AUXR & AUXR_T0x12) {        /* 不分频 */
        TL0 = (uint16_t)(65536 - overflow_us * SYS_CLK / 1000000) % 256;
        TH0 = (uint16_t)(65536 - overflow_us * SYS_CLK / 1000000) / 256;
    } else {                        /* 分频 */
        TL0 = (uint16_t)(65536 - overflow_us * SYS_CLK / 12000000) % 256;
        TH0 = (uint16_t)(65536 - overflow_us * SYS_CLK / 12000000) / 256;
    }

    ET0 = 1;                        /* 使能定时器0溢出中断 */
    EA = 1;                         /* 使能总中断 */
    TR0 = 1;                        /* 定时器0开始运行 */
}