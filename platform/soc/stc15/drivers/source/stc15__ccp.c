/*==============================================================================
文件名：stc15_ccp.c
修改记录：
    - 朱凯飞 2018.10.07 创建
文件说明：CCP捕获驱动文件
==============================================================================*/
#include "stc15_ccp.h"

/**
 * \brief CCP初始化
 */
void stc15_ccp_init (void)
{
    /* 确定CCP管脚 */
    P_SW1 &= ~P_SW1_CCP_S_MASK; 
        
    P_SW1 |= P_SW1_CCP_S1;         /* P1.2--ECI  P1.1--CCP0  P1.0--CCP1  P3.7--CCP2 */
    //P_SW1 |= P_SW1_CCP_S2;         /* P3.4--ECI  P3.5--CCP0  P3.6--CCP1  P3.7--CCP2 */
    //P_SW1 |= P_SW1_CCP_S3;         /* P2.4--ECI  P2.5--CCP0  P2.6--CCP1  P2.7--CCP2 */

    CCON = 0;
    
    CH = 0;
    CL = 0;
    CCAP0L = 0;
    CCAP0H = 0;                     /* 清除计数器 */
    
    CMOD = CMOD_CPS_SYSCLK | CMOD_CPS_ECF;                        /* 设置PCA时钟源为系统时钟，使能PCA溢出中断 */
    
    CCAPM0 = CCAPM0_CAPP0 | CCAPM0_CAPN0 | CCAPM0_ECCF0;          /* 使能下降沿和上升沿捕获和捕获中断 */
    CR = 1;                                                       /* PCA定时器开始工作   */
    EA = 1;
}

