/*==============================================================================
文件名：stc15_timer2.c
修改记录：
    - 朱凯飞 2018.09.16 创建
文件说明：定时器2驱动程序
==============================================================================*/
#include "stc15_timer2.h"
#include "prj_config.h"

/**
 * \brief 初始化定时器2（需要调用stc15_timer2_start才开始运行）
 */
void stc15_timer2_init (double overflow_us)
{
    AUXR &= ~AUXR_T2x12;            /* 定时器12分频 */

    INT_CLKO &= ~INT_CLKO_T2CLKO;   /* 不对外输出时钟 */

    if (AUXR & AUXR_T2x12) {        /* 不分频 */
        T2L = (uint16_t)(65536 - overflow_us * SYS_CLK / 1000000) % 256;
        T2H = (uint16_t)(65536 - overflow_us * SYS_CLK / 1000000) / 256;
    } else {                        /* 分频 */
        T2L = (uint16_t)(65536 - overflow_us * SYS_CLK / 12000000) % 256;
        T2H = (uint16_t)(65536 - overflow_us * SYS_CLK / 12000000) / 256;
    }

    IE2 |= IE2_ET2;                 /* 使能定时器2溢出中断 */
    EA = 1;                         /* 使能总中断 */
    AUXR &= ~AUXR_T2R;              /* 定时器2不运行 */
}

/**
 * \brief 定时器2开始运行
 */
void stc15_timer2_start(void)
{
    AUXR |= AUXR_T2R;               /* 定时器2开始运行 */
}

/**
 * \brief 判断定时器2是否正在运行
 *
 * \retval 0：没运行，1：正在运行
 */
uint8_t stc15_timer2_is_running(void)
{
    if (AUXR & AUXR_T2R) {
        return 1;
    }
    return 0;
}


/**
 * \brief 定时器2停止运行
 */
void stc15_timer2_stop(void)
{
    AUXR &= ~AUXR_T2R;               /* 定时器2停止运行 */
}
