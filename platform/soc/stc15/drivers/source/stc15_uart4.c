/*==============================================================================
文件名：stc15_uart4.c
修改记录：
    - 朱凯飞 2018.09.15 创建
文件说明：串口4驱动文件
==============================================================================*/
#include "stc15_uart4.h"
#include "prj_config.h"

/**
 * \brief 串口4初始化
 */
void stc15_uart4_init (uint32_t baud_rate)
{
    S4CON &= ~S4CON_S4SM0;      /* 8位数据,可变波特率 */   
    S4CON |= S4CON_S4ST4;       /* 定时器4作为波特率发生器 */   
    S4CON |= S4CON_S4REN;       /* 允许接收 */   

    T4T3M |= T4T3M_T4x12;       /* 定时器4不分频，定时器时钟频率为系统时钟频率 */	

    P_SW2 &= ~P_SW2_S4_S;       /* P0.2为RXD，P0.3为TXD */      

    T4L = (65536 - (SYS_CLK / 4 / baud_rate)) % 256;
    T4H = (65536 - (SYS_CLK / 4 / baud_rate)) / 256;            /* 设定定时初值 */
    IE2 &= ~IE2_ET4;            /* 禁止定时器4中断 */
    T4T3M |= T4T3M_T4R;         /* 开启定时器3 */
    EA = 1;                     /* 允许总中断 */
    IE2 |= IE2_ES4;             /* 允许串口4中断 */
}

/**
 * \brief 串口4发送一个字节
 */
void stc15_uart4_sendbyte (uint8_t byte)
{
    S4BUF = byte;
    while ((S4CON & S4CON_S4TI) == 0);
    S4CON &= ~S4CON_S4TI;
}

/**
 * \brief 串口4发送多个字节
 */
void stc15_uart4_sendnbytes (const uint8_t *p_bytes, uint8_t len)
{
    while (len--) {
        stc15_uart4_sendbyte(*p_bytes++);
    }
}
