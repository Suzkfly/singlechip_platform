/*==============================================================================
文件名：stc15_spi.c
修改记录：
    - 朱凯飞 2018.09.16 创建
文件说明：SPI驱动程序
==============================================================================*/
#include "stc15_spi.h"

/**
 * \brief SPI初始化
 *
 * \note 初始化为主机不需要使能中断
 */
void stc15_spi_init (void)
{
    SPCTL = 0;
    /* 主机模式，系统时钟32分频；高位先发；前沿采样，后沿输出 */
    SPCTL = SPCTL_SSIG | SPCTL_MSTR;         

    P_SW1 &= ~P_SW1_SPIS_MASK;          /* SS--P1.2  MOSI--P1.3  MISO--P1.4  SCLK--P1.5 */

    SPSTAT = SPSTAT_SPIF | SPSTAT_WCOL; /* 写1清除中断标志和冲突标识 */

    SPI_SS = 1;

    SPCTL |= SPCTL_SPEN;                /* SPI使能 */
}

/**
 * \brief SPI数据读写
 */
uint8_t stc15_spi_wr_byte (uint8_t byte)
{
    SPDAT = byte;
    while (!(SPSTAT & SPSTAT_SPIF));        /* 等待发送完成 */
    SPSTAT = SPSTAT_SPIF | SPSTAT_WCOL;     /* 清除SPI状态位 */
    return SPDAT;                           /* 返回SPI数据  */
}
