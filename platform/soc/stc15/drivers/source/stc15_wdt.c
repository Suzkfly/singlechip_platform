/*==============================================================================
文件名：文件名：stc15_wdt.c
修改记录：
    - 朱凯飞 2018.10.08 创建
文件说明：看门狗驱动文件
==============================================================================*/
#include "stc15_wdt.h"

/**
 * \brief 看门狗初始化
 */
void stc15_wdt_init (uint8_t prescaler)
{
    prescaler &= WDT_CONTR_PS_MASK;
    
    WDT_CONTR = WDT_CONTR_EN_WDT | prescaler;
}

/**
 * \brief 喂狗
 */
void stc15_wdt_feed (void)
{
    WDT_CONTR |= WDT_CONTR_CLR_WDT;
}

