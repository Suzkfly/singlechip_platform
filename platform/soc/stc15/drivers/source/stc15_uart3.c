/*==============================================================================
文件名：stc15_uart3.c
修改记录：
    - 朱凯飞 2018.09.15 创建
文件说明：串口3驱动文件
==============================================================================*/
#include "stc15_uart3.h"
#include "prj_config.h"

/**
 * \brief 串口3初始化
 */
void stc15_uart3_init (uint32_t baud_rate)
{
    PCON &= ~PCON_SMOD;         /* 波特率不倍速  */

    S3CON &= ~S3CON_S3SM0;      /* 8位数据,可变波特率 */   
    S3CON |= S3CON_S3ST3;       /* 定时器3作为波特率发生器 */   
    S3CON |= S3CON_S3REN;       /* 允许接收 */   

    T4T3M |= T4T3M_T3x12;       /* 定时器3不分频，定时器时钟频率为系统时钟频率 */	

    P_SW2 &= ~P_SW2_S3_S;       /* P0.0为RXD，P0.1为TXD */      

    T3L = (65536 - (SYS_CLK / 4 / baud_rate)) % 256;
    T3H = (65536 - (SYS_CLK / 4 / baud_rate)) / 256;            /* 设定定时初值 */
    IE2 &= ~IE2_ET3;            /* 禁止定时器3中断 */
    T4T3M |= T4T3M_T3R;         /* 开启定时器3 */
    EA = 1;                     /* 允许总中断 */
    IE2 |= IE2_ES3;             /* 允许串口3中断 */
}

/**
 * \brief 串口3发送一个字节
 */
void stc15_uart3_sendbyte (uint8_t byte)
{
    S3BUF = byte;
    while ((S3CON & S3CON_S3TI) == 0);
    S3CON &= ~S3CON_S3TI;
}

/**
 * \brief 串口3发送多个字节
 */
void stc15_uart3_sendnbytes (const uint8_t *p_bytes, uint8_t len)
{
    while (len--) {
        stc15_uart3_sendbyte(*p_bytes++);
    }
}

/**
 * \brief 串口3发送字符串
 */
void stc15_uart3_sendstr (const uint8_t *p_bytes)
{
    while (*p_bytes) {
        stc15_uart3_sendbyte(*p_bytes++);
    }
}

