/*==============================================================================
文件名：stc15_uart1.c
修改记录：
    - 朱凯飞 2018.09.15 创建
文件说明：串口1驱动文件
==============================================================================*/
#include "stc15_uart1.h"
#include "prj_config.h"

/* 定义则可以使用printf函数打印 */
#define PRINT_EN

/**
 * \brief 串口1初始化
 */
void stc15_uart1_init (uint32_t baud_rate)
{
    PCON &= ~PCON_SMOD;         /* 波特率不倍速  */

    SCON &= ~SCON_SM_MASK;
    SCON |= SCON_SM_MODE1;      /* 8位数据,可变波特率 */
    SCON |= SCON_REN;           /* 允许接收 */    
    
    AUXR |= AUXR_T1x12;         /* 定时器1不分频，定时器时钟频率为系统时钟频率 */
    AUXR &= ~AUXR_SIST2;        /* 选择定时器1作为波特率发生器 */ 

    AUXR1 &= ~AUXR1_S1_MASK; 
    AUXR1 |= AUXR1_S1_P30_P31;  /* P3.0为RXD，P3.1为TXD */     

    TMOD &= 0x0F;               /* 设定定时器1为16位自动重装方式 */
    TL1 = (65536 - (SYS_CLK / 4 / baud_rate)) % 256;       
    TH1 = (65536 - (SYS_CLK / 4 / baud_rate)) / 256;            /* 设定定时初值 */
    ET1 = 0;                    /* 禁止定时器1中断 */
    TR1 = 1;                    /* 开启定时器1 */

#ifdef PRINT_EN
    ES = 0;                     /* 不允许串口中断 */
    TI = 1;                     /* 加这一句是为了能使用printf打印 */
#else
	EA = 1;
    ES = 1;                     /* 允许串口中断 */
#endif
}

/**
 * \brief 串口1发送一个字节
 */
void stc15_uart1_sendbyte (uint8_t byte)
{
    SBUF = byte;
    while (!TI);
    TI = 0;
}

///**
// * \brief 串口1发送多个字节
// */
//void stc15_uart1_sendnbytes (const uint8_t *p_bytes, uint8_t len)
//{
//    while (len--) {
//        stc15_uart1_sendbyte(*p_bytes++);
//    }
//}

/**
 * \brief 串口1发送字符串
 */
//void stc15_uart1_sendstr (const uint8_t *p_bytes)
//{
//    EA = 0;
//    while (*p_bytes) {
//        stc15_uart1_sendbyte(*p_bytes++);
//    }
//    EA = 1;
//}
