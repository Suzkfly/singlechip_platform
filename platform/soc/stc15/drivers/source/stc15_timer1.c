/*==============================================================================
文件名：stc15_timer1.c
修改记录：
    - 朱凯飞 2018.09.16 创建
文件说明：定时器1驱动程序
==============================================================================*/
#include "stc15_timer1.h"
#include "prj_config.h"

/**
 * \brief 初始化定时器1
 */
void stc15_timer1_init (double overflow_us)
{
    TMOD &= ~TMOD_T1_MASK;

    AUXR &= ~AUXR_T1x12;            /* 定时器12分频 */

    INT_CLKO &= ~INT_CLKO_T1CLKO;   /* 不对外输出时钟 */

    if (AUXR & AUXR_T1x12) {        /* 不分频 */
        TL1 = (uint16_t)(65536 - overflow_us * SYS_CLK / 1000000) % 256;
        TH1 = (uint16_t)(65536 - overflow_us * SYS_CLK / 1000000) / 256;
    } else {                        /* 分频 */
        TL1 = (uint16_t)(65536 - overflow_us * SYS_CLK / 12000000) % 256;
        TH1 = (uint16_t)(65536 - overflow_us * SYS_CLK / 12000000) / 256;
    }

    ET1 = 1;                        /* 使能定时器1溢出中断 */
    EA = 1;                         /* 使能总中断 */
    TR1 = 1;                        /* 定时器1开始运行 */
}