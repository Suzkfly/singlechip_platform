/*==============================================================================
文件名：stc15_pwm.c
修改记录：
    - 朱凯飞 2018.10.07 创建
文件说明：PWM驱动文件
==============================================================================*/
#include "stc15_pwm.h"

/**
 * \brief PWM初始化
 */
void stc15_pwm_init (void)
{
    /* 确定PWM管脚 */
    P_SW1 &= ~P_SW1_CCP_S_MASK; 
        
    P_SW1 |= P_SW1_CCP_S1;         /* P1.2--ECI  P1.1--CCP0  P1.0--CCP1  P3.7--CCP2 */
    //P_SW1 |= P_SW1_CCP_S2;         /* P3.4--ECI  P3.5--CCP0  P3.6--CCP1  P3.7--CCP2 */
    //P_SW1 |= P_SW1_CCP_S3;         /* P2.4--ECI  P2.5--CCP0  P2.6--CCP1  P2.7--CCP2 */

    CCON = 0;

    CH = 0;
    CL = 0;     /* 清除PCA计数器 */

    CMOD = CMOD_CPS_SYSCLK;                     /* 设置PCA时钟源为系统时钟，不使能PCA计数器溢出中断 */
                                   
    PCA_PWM0 = PCA_PWM1_EBS_8BITS;              /* PCA模块0工作于8位PWM  */
    CCAP0H = CCAP0L = 128;                      /* PWM0的占空比为128 / 256 */
    CCAPM0 = CCAPM0_ECOM0 | CCAPM0_PWM0;        /* PCA工作在PWM模式 */

    PCA_PWM1 = PCA_PWM1_EBS_7BITS;              /* PCA模块1工作于7位PWM   */
    CCAP1H = CCAP1L = 64;                       /* PWM1的占空比为64 / 128 */
    CCAPM1 = CCAPM0_ECOM1 | CCAPM0_PWM1;        /* PCA工作在PWM模式 */   

    PCA_PWM2 = PCA_PWM1_EBS_6BITS;              /* PCA模块2工作于6位PWM   */
    CCAP2H = CCAP2L = 32;                       /* PWM2的占空比为32 / 64  */
    CCAPM2 = CCAPM0_ECOM2 | CCAPM0_PWM2;        /* PCA工作在PWM模式 */

    CR = 1;                                     /* PCA定时器开始工作   */
}

