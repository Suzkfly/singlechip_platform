/*==============================================================================
文件名：stc15_iap.c
修改记录：
    - 朱凯飞 2018.10.02 创建
文件说明：单片机内部iap驱动文件
==============================================================================*/
#include "stc15_iap.h"

/**
 * \brief ISP/IAP命令触发
 *
 * \note 每次IAP操作时，都要对IAP_TRIG先写入5AH，再写入A5H，ISP/IAP命令才会生效
 */
static void __iap_trig (void)
{
    IAP_TRIG = 0x5A;
    IAP_TRIG = 0xA5;
}

/**
 * \brief 设置CPU等待时间
 */
static void __iap_set_wt (void)
{
    const uint32_t wt_table[7] = {24000000, 20000000, 12000000, 6000000, 3000000, 2000000, 1000000};
    uint8_t i, wt = 0;

    for (i = 0; i < sizeof(wt_table); i++) {
        if (SYS_CLK <= wt_table[i]) {
            wt++;
        } else {
            break;
        }
    }

    IAP_CONTR &= ~IAP_CONTR_WT_MASK;
    IAP_CONTR |= wt;
}

/**
 * \brief 软件复位
 */
void stc15_soft_reset (void)
{
    IAP_CONTR |= IAP_CONTR_SWRST;
}

/**
 * \brief IAP功能初始化
 */
void stc15_iap_init (void)
{
    IAP_CONTR |= IAP_CONTR_IAPEN;   /* IAP使能 */
    __iap_set_wt();
}

/**
 * \brief 扇区擦除
 */
int8_t stc15_iap_sector_erase (uint8_t sector_addr)
{
    uint16_t addr;

    if (PCON & PCON_LVDF) {
        PCON &= ~PCON_LVDF;
        return -1;
    }

    addr = sector_addr * STC15_SECTOR_BYTES;

    IAP_CMD = IAP_CMD_ERASE;

    IAP_ADDRH = addr >> 8;
    IAP_ADDRL = addr & 0xFF;

    __iap_trig();

    /* 判断操作是否成功 */
    if (IAP_CONTR & IAP_CONTR_CMD_FAIL) {
        IAP_CONTR &= ~IAP_CONTR_CMD_FAIL;
        return -1;
    }
    return 0;
}

/**
 * \brief 往指定的FLASH地址中写入一个字节
 */
static void __iap_write_byte (uint16_t addr, uint8_t dat)
{
    IAP_CMD = IAP_CMD_WRITE;

    IAP_ADDRH = addr >> 8;
    IAP_ADDRL = addr & 0xFF;

    IAP_DATA = dat;

    __iap_trig();
}


/**
 * \brief 往指定地址写入数据
 */
int8_t stc15_iap_write (uint16_t addr, uint8_t *p_dat, uint16_t len)
{
    uint16_t i;

    /* 低压禁止操作 */
    if (PCON & PCON_LVDF) {
        PCON &= ~PCON_LVDF;
        return -1;
    }

    /* 数据不能跨扇区 */
    if (addr % STC15_SECTOR_BYTES + len > STC15_SECTOR_BYTES) {
        return -1;
    }

    if (len > STC15_SECTOR_BYTES) {
        return -1;
    }

    /* 写入数据 */
    for (i = 0; i < len; i++) {
        __iap_write_byte(addr, p_dat[i]);
        addr++;
    }

    /* 判断操作是否成功 */
    if (IAP_CONTR & IAP_CONTR_CMD_FAIL) {
        IAP_CONTR &= ~IAP_CONTR_CMD_FAIL;
        return -1;
    }
    return 0;
}

/**
 * \brief 从指定的FLASH地址中读取一个字节
 */
static uint8_t __iap_read_byte (uint16_t addr)
{
    IAP_CMD = IAP_CMD_READ;

    IAP_ADDRH = addr >> 8;
    IAP_ADDRL = addr & 0xFF;

    __iap_trig();

    return IAP_DATA;
}


/**
 * \brief 从指定地址中读出数据
 */
int8_t stc15_iap_read (uint16_t addr, uint8_t *p_dat, uint16_t len)
{
    uint16_t i;

    /* 低压禁止操作 */
    if (PCON & PCON_LVDF) {
        PCON &= ~PCON_LVDF;
        return -1;
    }

    /* 数据不能跨扇区 */
    if (addr % STC15_SECTOR_BYTES + len > STC15_SECTOR_BYTES) {
        return -1;
    }

    if (len > STC15_SECTOR_BYTES) {
        return -1;
    }

    /* 读取数据 */
    for (i = 0; i < len; i++) { 
        p_dat[i] = __iap_read_byte(addr);
        addr++;
    }

    /* 判断操作是否成功 */
    if (IAP_CONTR & IAP_CONTR_CMD_FAIL) {
        IAP_CONTR &= ~IAP_CONTR_CMD_FAIL;
        return -1;
    }
    return 0;
}

