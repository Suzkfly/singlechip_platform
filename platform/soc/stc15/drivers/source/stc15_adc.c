/*==============================================================================
文件名：stc15_adc.c
修改记录：
    - 朱凯飞 2018.10.05 创建
文件说明：ADC驱动文件
==============================================================================*/
#include "stc15_adc.h"
#include "delay.h"
#include "intrins.h"

#include "stdio.h"

/**
 * \brief ADC初始化
 */
void stc15_adc_init (void)
{
    IE &= ~IE_EADC;                     /* 禁能ADC中断 */
    CLK_DIV &= ~CLK_DIV_ADRJ;           /* ADC_RES存放高8位，ADC_RESL存放低2位 */
    P1ASF = 0;                          /* 不使能任何通道 */
    ADC_CONTR |= ADC_CONTR_ADC_POWER;   /* 开启ADC电源 */

    delay_ms(10);                       /* 延时10ms等待ADC电源稳定 */
}

/**
 * \brief 获取10位AD转换结果
 */
static uint16_t __get_adc_result(uint8_t ch)
{   
    uint16_t bandgap_result;
    
    if (ch <= 7) {
        ADC_CONTR = ADC_CONTR_ADC_POWER | STC15_ADC_SPEED | ADC_CONTR_ADC_START | ch;
        _nop_();                        /* 等待4个NOP  */
        _nop_();
        _nop_();
        _nop_();
        while ((ADC_CONTR & ADC_CONTR_ADC_FLAG) == 0);
        ADC_CONTR &= ~ADC_CONTR_ADC_FLAG;
        bandgap_result = (ADC_RES << 2) | (ADC_RESL & 0x03);
    }

    return  bandgap_result;
}


/**
 * \brief 同步获取AD转换电压（阻塞）
 */
uint16_t stc15_get_vol (uint8_t chan)
{
    uint8_t code *p_rom_param = NULL;
    xdata uint32_t rom_bandgap = 0, chan_result = 0, bandgap_result = 0;

    if (chan <= 7) {
        /* 从ROM中读取BandGap电压值 */
        p_rom_param = (uint8_t *)ID_ADDR_ROM;
        rom_bandgap = *p_rom_param++;
        rom_bandgap <<= 8;
        rom_bandgap += *p_rom_param;

        /* 读取通道电压 */
        ADC_RES = 0;       
        ADC_RESL = 0;
        P1ASF = 1 << chan;  
        __get_adc_result(chan);
        __get_adc_result(chan);      /* 读取三次以便结果稳定 */
        chan_result = __get_adc_result(chan);


        /* 读取BandGap电压 */
        ADC_RES = 0;          
        ADC_RESL = 0;
        P1ASF = 0;
        __get_adc_result(0);
        __get_adc_result(0);
        bandgap_result = __get_adc_result(0);

        chan_result = rom_bandgap * chan_result / bandgap_result; 
    }

    return (uint16_t)chan_result;
}

/**
 * \brief 获取系统电压
 */
uint16_t stc15_get_sys_vol (void)
{
    uint8_t code *p_rom_param = NULL;
    xdata uint32_t rom_bandgap = 0, bandgap_result = 0, system_vol = 0;

    /* 从ROM中读取BandGap电压值 */
    p_rom_param = (uint8_t *)ID_ADDR_ROM;
    rom_bandgap = *p_rom_param++;
    rom_bandgap <<= 8;
    rom_bandgap += *p_rom_param;

    /* 读取BandGap电压 */
    ADC_RES = 0;          
    ADC_RESL = 0;
    P1ASF = 0;
    __get_adc_result(0);
    __get_adc_result(0);
    bandgap_result = __get_adc_result(0);

    system_vol = rom_bandgap * 1024 / bandgap_result; 

    return (uint16_t)system_vol;
}


