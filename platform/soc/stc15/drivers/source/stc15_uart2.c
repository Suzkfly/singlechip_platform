/*==============================================================================
文件名：stc15_uart2.c
修改记录：
    - 朱凯飞 2018.09.15 创建
文件说明：串口2驱动文件
==============================================================================*/
#include "stc15_uart2.h"
#include "prj_config.h"

/**
 * \brief 串口2初始化
 */
void stc15_uart2_init (uint32_t baud_rate)
{
    PCON &= ~PCON_SMOD;         /* 波特率不倍速  */

    S2CON &= ~S2CON_S2SM0;      /* 8位数据,可变波特率 */      
    S2CON |= S2CON_S2REN;       /* 允许接收 */   

    AUXR |= AUXR_T2x12;         /* 定时器2不分频，定时器时钟频率为系统时钟频率 */	

    P_SW2 &= ~P_SW2_S2_S;       /* P1.0为RXD，P1.1为TXD */      

    T2L = (65536 - (SYS_CLK / 4 / baud_rate)) % 256;
    T2H = (65536 - (SYS_CLK / 4 / baud_rate)) / 256;            /* 设定定时初值 */
    IE2 &= ~IE2_ET2;            /* 禁止定时器2中断 */
    AUXR |= AUXR_T2R;           /* 开启定时器2 */
    EA = 1;                     /* 允许总中断 */
    IE2 |= IE2_ES2;             /* 允许串口2中断 */
}

/**
 * \brief 串口2发送一个字节
 */
void stc15_uart2_sendbyte (uint8_t byte)
{
    S2BUF = byte;
    while ((S2CON & S2CON_S2TI) == 0);
    S2CON &= ~S2CON_S2TI;
}

/**
 * \brief 串口2发送多个字节
 */
void stc15_uart2_sendnbytes (const uint8_t *p_bytes, uint8_t len)
{
    while (len--) {
        stc15_uart2_sendbyte(*p_bytes++);
    }
}

/**
 * \brief 串口2发送字符串
 */
//void stc15_uart2_sendstr (const uint8_t *p_bytes)
//{
//    while (*p_bytes) {
//        stc15_uart2_sendbyte(*p_bytes++);
//    }
//}
