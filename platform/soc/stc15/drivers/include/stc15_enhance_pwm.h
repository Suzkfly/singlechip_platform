/*==============================================================================
文件名：文件名：stc15_enhance_pwm.h
修改记录：
    - 朱凯飞 2018.10.08 创建
文件说明：PWM驱动文件
==============================================================================*/
#ifndef __STC15_ENHANCE_PWM_H
#define __STC15_ENHANCE_PWM_H

#include "stc15_pca.h"
#include "includes.h"

#define P_SW2_EAXSFR            (1 << 7)    /* 扩展SFR访问控制使能 */
    
#define PWMCFG_CBTADC           (1 << 6)    /* PWM计数器归零时触发ADC转换 */
#define PWMCFG_C7INI_HIGH       (1 << 5)    /* 设置PWM7输出端口的初始电平为高电平 */
#define PWMCFG_C6INI_HIGH       (1 << 4)    /* 设置PWM6输出端口的初始电平为高电平 */
#define PWMCFG_C5INI_HIGH       (1 << 3)    /* 设置PWM5输出端口的初始电平为高电平 */
#define PWMCFG_C4INI_HIGH       (1 << 2)    /* 设置PWM4输出端口的初始电平为高电平 */
#define PWMCFG_C3INI_HIGH       (1 << 1)    /* 设置PWM3输出端口的初始电平为高电平 */
#define PWMCFG_C2INI_HIGH       (1 << 0)    /* 设置PWM2输出端口的初始电平为高电平 */

#define PWMCR_ENPWM             (1 << 7)    /* 使能增强型PWM发生器（必须所有配置都完成后才使能） */
#define PWMCR_ECBI              (1 << 6)    /* PWM计数器归零中断使能 */
#define PWMCR_ENC7O             (1 << 5)    /* PWM7输出使能 */
#define PWMCR_ENC6O             (1 << 4)    /* PWM6输出使能 */
#define PWMCR_ENC5O             (1 << 3)    /* PWM5输出使能 */
#define PWMCR_ENC4O             (1 << 2)    /* PWM4输出使能 */
#define PWMCR_ENC3O             (1 << 1)    /* PWM3输出使能 */
#define PWMCR_ENC2O             (1 << 0)    /* PWM2输出使能 */

#define PWMIF_CBIF              (1 << 6)    /* PWM计数器归零中断标识位 */
#define PWMIF_C7IF              (1 << 5)    /* PWM7中断标识（需要软件清零） */
#define PWMIF_C6IF              (1 << 4)    /* PWM6中断标识（需要软件清零） */
#define PWMIF_C5IF              (1 << 3)    /* PWM5中断标识（需要软件清零） */
#define PWMIF_C4IF              (1 << 2)    /* PWM4中断标识（需要软件清零） */
#define PWMIF_C3IF              (1 << 1)    /* PWM3中断标识（需要软件清零） */
#define PWMIF_C2IF              (1 << 0)    /* PWM2中断标识（需要软件清零） */

#define PWMFDCR_ENFD            (1 << 5)    /* PWM外部异常检测功能使能 */
#define PWMFDCR_FLTFLIO         (1 << 4)    /* 1：发生外部异常时，PWM输出口立即被设为高阻态，外部异常取消时，PWM输出口回复到以前的模式；0：不作任何改变 */
#define PWMFDCR_EFDI            (1 << 3)    /* PWM异常检测中断使能 */
#define PWMFDCR_FDCMP           (1 << 2)    /* 设定PWM异常检测源为内部比较器的输出（当CMP+ > CMP-时触发PWM异常） */
#define PWMFDCR_FDIO            (1 << 1)    /* 设定PWM异常检测源为P2.4的状态（P2.4电平为高时触发PWM异常） */
#define PWMFDCR_FDIF            (1 << 0)    /* PWM异常中断标志 */

#define PWMCKS_SELT2            (1 << 4)    /* PWM时钟源选择。0：系统时钟经过分频后的时钟；1：定时器2的溢出脉冲 */
#define PWMCKS_PS_MASK          (0xF << 0)  /* 系统时钟预分频参数（当SELT2为0时PWM时钟为系统时钟/(PS[3:0] + 1) */

#define PWM2CR_PWM2_CS          (1 << 3)    /* PWM2输出管脚选择位。0：P3.7；1：P2.7 */
#define PWM2CR_EPWM2I           (1 << 2)    /* PWM2中断使能 */
#define PWM2CR_EC2T2SI          (1 << 1)    /* 当PWM波形发生器内部计数值与T2计数器锁设定的值相匹配时，PWM的波形翻转，同时置位C2IF，可触发中断 */
#define PWM2CR_EC2T1SI          (1 << 0)    /* 当PWM波形发生器内部计数值与T1计数器锁设定的值相匹配时，PWM的波形翻转，同时置位C2IF，可触发中断 */

#define PWM3CR_PWM3_CS          (1 << 3)    /* PWM3输出管脚选择位。0：P2.1；1：P4.5 */
#define PWM3CR_EPWM3I           (1 << 2)    /* PWM3中断使能 */
#define PWM3CR_EC3T2SI          (1 << 1)    /* 当PWM波形发生器内部计数值与T2计数器锁设定的值相匹配时，PWM的波形翻转，同时置位C3IF，可触发中断 */
#define PWM3CR_EC3T1SI          (1 << 0)    /* 当PWM波形发生器内部计数值与T1计数器锁设定的值相匹配时，PWM的波形翻转，同时置位C3IF，可触发中断 */

#define PWM4CR_PWM4_CS          (1 << 3)    /* PWM4输出管脚选择位。0：P2.2；1：P4.4 */
#define PWM4CR_EPWM4I           (1 << 2)    /* PWM4中断使能 */
#define PWM4CR_EC4T2SI          (1 << 1)    /* 当PWM波形发生器内部计数值与T2计数器锁设定的值相匹配时，PWM的波形翻转，同时置位C4IF，可触发中断 */
#define PWM4CR_EC4T1SI          (1 << 0)    /* 当PWM波形发生器内部计数值与T1计数器锁设定的值相匹配时，PWM的波形翻转，同时置位C4IF，可触发中断 */

#define PWM5CR_PWM5_CS          (1 << 3)    /* PWM5输出管脚选择位。0：P2.3；1：P4.2 */
#define PWM5CR_EPWM5I           (1 << 2)    /* PWM5中断使能 */
#define PWM5CR_EC5T2SI          (1 << 1)    /* 当PWM波形发生器内部计数值与T2计数器锁设定的值相匹配时，PWM的波形翻转，同时置位C5IF，可触发中断 */
#define PWM5CR_EC5T1SI          (1 << 0)    /* 当PWM波形发生器内部计数值与T1计数器锁设定的值相匹配时，PWM的波形翻转，同时置位C5IF，可触发中断 */

#define PWM6CR_PWM6_CS          (1 << 3)    /* PWM6输出管脚选择位。0：P1.6；1：P0.7 */
#define PWM6CR_EPWM6I           (1 << 2)    /* PWM6中断使能 */
#define PWM6CR_EC6T2SI          (1 << 1)    /* 当PWM波形发生器内部计数值与T2计数器锁设定的值相匹配时，PWM的波形翻转，同时置位C6IF，可触发中断 */
#define PWM6CR_EC6T1SI          (1 << 0)    /* 当PWM波形发生器内部计数值与T1计数器锁设定的值相匹配时，PWM的波形翻转，同时置位C6IF，可触发中断 */

#define PWM7CR_PWM7_CS          (1 << 3)    /* PWM7输出管脚选择位。0：P1.7；1：P0.6 */
#define PWM7CR_EPWM7I           (1 << 2)    /* PWM7中断使能 */
#define PWM7CR_EC7T2SI          (1 << 1)    /* 当PWM波形发生器内部计数值与T2计数器锁设定的值相匹配时，PWM的波形翻转，同时置位C7IF，可触发中断 */
#define PWM7CR_EC7T1SI          (1 << 0)    /* 当PWM波形发生器内部计数值与T1计数器锁设定的值相匹配时，PWM的波形翻转，同时置位C7IF，可触发中断 */

/**
 * \brief PWM初始化
 */
extern void stc15_enhance_pwm_init (void);


#endif
