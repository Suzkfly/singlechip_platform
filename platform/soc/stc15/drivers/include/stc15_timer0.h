/*==============================================================================
文件名：stc15_timer0.h
修改记录：
    - 朱凯飞 2018.09.16 创建
文件说明：定时器0驱动程序
==============================================================================*/
#ifndef __STC15_TIMER0_H
#define __STC15_TIMER0_H

#include "stc15f2k60s2.h"
#include "types.h"

#define TMOD_T0_MASK       (0x0F << 0)
#define TMOD_T0_GATE       (1 << 3)    /* 置1时只有EINT0脚为1且TR0为1时才能打开定时/计数器 */
#define TMOD_T0_CT         (1 << 2)    /* 置0对内部时钟源记数（作定时器），置1对外部时钟源记数（计数器） */
#define TMOE_T0_MODE_MASK  (0x3 << 0)  /* 定时器0工作模式 */
#define TMOD_T0_MODE0      (0 << 0)    /* 16位自动重装载模式 */
#define TMOD_T0_MODE1      (1 << 0)    /* 16位非自动重装载模式 */
#define TMOD_T0_MODE2      (2 << 0)    /* 8位自动重装载模式 */
#define TMOD_T0_MODE3      (3 << 0)    /* 不可屏蔽中断的16位重装载模式 */

#define AUXR_T0x12      (1 << 7)    /* 定时器0速度控制位。0：12分频；1：不分频 */

#define INT_CLKO_T0CLKO (1 << 0)    /* 为1时将P3.5配置为定时器0的时钟输出，定时器0每溢出一次，P3.5翻转一次 */

/**
 * \brief 初始化定时器0
 *
 * \param[in]  overflow_us ：溢出时间
 *
 * \note 12M晶振下最长定时65536us，11.0592M晶振下最长定时71111us
 * \note 定时器默认12分频
 */
extern void stc15_timer0_init (double overflow_us);

#endif