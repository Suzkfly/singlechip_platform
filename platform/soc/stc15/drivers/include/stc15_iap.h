/*==============================================================================
文件名：stc15_iap.h
修改记录：
    - 朱凯飞 2018.10.02 创建
文件说明：单片机内部iap驱动文件
==============================================================================*/
#ifndef __STC15_IAP_H
#define __STC15_IAP_H

#include "includes.h"

#define IAP_CMD_STANDBY     0   /* 待机模式，无ISP操作 */
#define IAP_CMD_READ        1   /* 字节读 */
#define IAP_CMD_WRITE       2   /* 字节写 */
#define IAP_CMD_ERASE       3   /* 扇区擦除 */

#define IAP_CONTR_IAPEN     (1 << 7)    /* ISP/IAP功能允许位 */
#define IAP_CONTR_SWBS      (1 << 6)    /* 软件复位后应用程序启动区域。0：用户应用程序区；1：系统ISP监控程序区 */
#define IAP_CONTR_SWRST     (1 << 5)    /* 写1单片机复位 */
#define IAP_CONTR_CMD_FAIL  (1 << 4)    /* 读出为1表示IAP地址非法 */
#define IAP_CONTR_WT_MASK   0x07        /* CPU等待时间 */

#define PCON_LVDF           (1 << 5)    /* 低压检测标识，低压置一，必须由软件清除（低压时禁止对EEPRMO操作） */


#define STC15_SECTOR_BYTES  512 /* 定义一个扇区中包含的字节数 */

/**
 * \brief 软件复位
 */
extern void stc15_soft_reset (void);

/**
 * \brief IAP功能初始化
 */
extern void stc15_iap_init (void);

/**
 * \brief 扇区擦除
 *
 * \param[in] sector_addr：要擦除的扇区地址
 *
 * \retval  0：成功 -1：失败
 *
 * \note 一个扇区512个字节
 */
extern int8_t stc15_iap_sector_erase (uint8_t sector_addr);

/**
 * \brief 往指定的FLASH地址中写入数据
 *
 * \param[in] addr：要写入的FLASH地址
 * \param[in] p_dat：要写入的数据地址
 * \param[in] len：要写入的数据长度
 *
 * \retval  0：成功 -1：失败
 *
 * \note 写入的数据不能跨扇区
 */
extern int8_t stc15_iap_write (uint16_t addr, uint8_t *p_dat, uint16_t len);


/**
 * \brief 从指定的FLASH地址中读取数据
 *
 * \param[in] addr：要读取的FLASH地址
 * \param[out] p_dat：存放读取出来的数据的地址
 * \param[in] len：要读取的数据长度
 *
 * \retval  0：成功 -1：失败
 *
 * \note 读取的数据不能跨扇区
 */
extern int8_t stc15_iap_read (uint16_t addr, uint8_t *p_dat, uint16_t len);


#endif
