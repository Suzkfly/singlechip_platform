/*==============================================================================
文件名：stc15_adc.h
修改记录：
    - 朱凯飞 2018.10.05 创建
文件说明：ADC驱动文件
注意：
    必须使用内部振荡器
==============================================================================*/
#ifndef __STC15_ADC_H
#define __STC15_ADC_H

#include "includes.h"

/* BandGap相关参数  */
#define ID_ADDR_RAM 0xEF        //对于只有256字节RAM的MCU(大部分系列)存放地址为0EFH
//#define ID_ADDR_RAM 0x6F       //对于只有128字节RAM的MCU(stc15f/w100系列)存放地址为06fH

//注意:需要在下载代码时选择"在ID号前添加重要测试参数"选项,才可在程序中获取此参数
//容量不同的单片机请更改不同数值
//#define ID_ADDR_ROM 0x03f7      //1K程序空间的MCU
//#define ID_ADDR_ROM 0x07f7      //2K程序空间的MCU
//#define ID_ADDR_ROM 0x0bf7      //3K程序空间的MCU
//#define ID_ADDR_ROM 0x0ff7      //4K程序空间的MCU
//#define ID_ADDR_ROM 0x13f7      //5K程序空间的MCU
//#define ID_ADDR_ROM 0x1ff7      //8K程序空间的MCU
//#define ID_ADDR_ROM 0x27f7      //10K程序空间的MCU
//#define ID_ADDR_ROM 0x2ff7      //12K程序空间的MCU
//#define ID_ADDR_ROM 0x3ff7      //16K程序空间的MCU
//#define ID_ADDR_ROM 0x4ff7      //20K程序空间的MCU
//#define ID_ADDR_ROM 0x5ff7      //24K程序空间的MCU
//#define ID_ADDR_ROM 0x6ff7      //28K程序空间的MCU
//#define ID_ADDR_ROM 0x7ff7      //32K程序空间的MCU
//#define ID_ADDR_ROM 0x9ff7      //40K程序空间的MCU
//#define ID_ADDR_ROM 0xbff7      //48K程序空间的MCU
//#define ID_ADDR_ROM 0xcff7      //52K程序空间的MCU
//#define ID_ADDR_ROM 0xdff7      //56K程序空间的MCU
//#define ID_ADDR_ROM 0xeff7      //60K程序空间的MCU
#define ID_ADDR_ROM 0xf3f7      //61K程序空间的MCU

/* ADC转换一次所需时钟选择列表 */

/* 定义ADC转换需要的时钟 */
#define STC15_ADC_SPEED    STC15_ADC_SPEED_540

#define ADC_CONTR_ADC_POWER    (1 << 7)    /* ADC电源控制 */
#define ADC_CONTR_SPEED_MASK   (0x3 << 5)  /* ADC速度控制 */
#define STC15_ADC_SPEED_90     (3 << 5)
#define STC15_ADC_SPEED_180    (2 << 5)
#define STC15_ADC_SPEED_360    (1 << 5)
#define STC15_ADC_SPEED_540    (0 << 5)
#define ADC_CONTR_ADC_FLAG     (1 << 4)    /* AD转换完成标识 */
#define ADC_CONTR_ADC_START    (1 << 3)    /* 启动控制，写1开始转换，转换完成后自动变为0 */
#define ADC_CONTR_CHS_MASK     (0x7 << 0)  /* 模拟输入通道选择 */

#define CLK_DIV_ADRJ            (1 << 5)   /* AD转换结果数据格式调整位 */

#define IE_EADC                 (1 << 5)   /* ADC中断使能位 */

/**
 * \brief ADC初始化
 */
extern void stc15_adc_init (void);

/**
 * \brief 同步获取AD转换电压（阻塞）
 *
 * \note 该函数获取到的电压值是经过校准过的，直接得到mv电压值
 */
extern uint16_t stc15_get_vol (uint8_t chan);

/**
 * \brief 获取系统电压
 */
extern uint16_t stc15_get_sys_vol (void);

#endif
