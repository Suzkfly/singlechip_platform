/*==============================================================================
文件名：stc15_timer2.h
修改记录：
    - 朱凯飞 2018.09.16 创建
文件说明：定时器2驱动程序
==============================================================================*/
#ifndef __STC15_TIMER2_H
#define __STC15_TIMER2_H

#include "stc15f2k60s2.h"
#include "types.h"

#define AUXR_T2R            (1 << 4)    /* 定时器2允许控制位 */
#define AUXR_T2_CT          (1 << 3)    /* 置0对内部时钟源记数（作定时器），置1对外部时钟源记数（计数器） */
#define AUXR_T2x12          (1 << 2)    /* 定时器2速度控制位。0：12分频；1：不分频 */

#define IE2_ET2             (1 << 2)    /* 定时器2中断允许位 */

#define INT_CLKO_T2CLKO     (1 << 2)    /* 为1时将P3.0配置为定时器2的时钟输出，定时器2每溢出一次，P3.0翻转一次 */

/**
 * \brief 初始化定时器2，（需要调用stc15_timer2_start才开始运行）
 *
 * \param[in]  overflow_us ：溢出时间
 *
 * \note 12M晶振下最长定时65536us，11.0592M晶振下最长定时71111us
 * \note 定时器默认12分频
 * \note 定时器2固定为16位自动重装载模式
 */
extern void stc15_timer2_init (double overflow_us);

/**
 * \brief 定时器2开始运行
 */
extern void stc15_timer2_start(void);

/**
 * \brief 判断定时器2是否正在运行
 *
 * \retval 0：没运行，1：正在运行
 */
extern uint8_t stc15_timer2_is_running(void);

/**
 * \brief 定时器2停止运行
 */
extern void stc15_timer2_stop(void);


#endif