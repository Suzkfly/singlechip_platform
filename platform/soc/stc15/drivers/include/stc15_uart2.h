/*==============================================================================
文件名：stc15_uart2.h
修改记录：
    - 朱凯飞 2018.09.15 创建
文件说明：串口2驱动文件
==============================================================================*/
#ifndef __STC15_UART2_H
#define __STC15_UART2_H

#include "includes.h"

/********************************常用寄存器位**********************************/
#define S2CON_S2SM0      (1 << 7)    /* 串行口2的工作方式，为0时8位模式，为1时为9位模式*/
#define S2CON_S2REN      (1 << 4)    /* 允许接收 */
#define S2CON_S2TI       (1 << 1)    /* 发送中断请求 */
#define S2CON_S2RI       (1 << 0)    /* 接收中断请求 */

#define PCON_SMOD        (1 << 7)    /* 为1时串口1,2,3的波特率都加倍，否则不加倍 */

#define AUXR_T2R         (1 << 4)    /* 定时器2运行控制位 */
#define AUXR_T2x12       (1 << 2)    /* 定时器2速度控制位，0：为系统时钟的12分频。1：不分频 */

#define IE2_ET2          (1 << 2)    /* 定时器2中断允许位 */
#define IE2_ES2          (1 << 0)    /* 串行口2中断允许位 */

#define P_SW2_S2_S       (1 << 0)    /* 串口2所在位置，为0时：RXD:P1.0,TXD:P1.1，为1时：RXD:P4.6,TXD:P4.7 */


/**
 * \brief 串口2初始化
 *
 * \param[in] baud_rate：波特率
 *
 * \return 无
 *
 * \note 11.0592MHz晶振下最低波特率42，最高波特率2764800，串行口2只能选用定时器2作为波特率发生器
 */
extern void stc15_uart2_init (uint32_t baud_rate);

/**
 * \brief 串口2发送一个字节
 *
 * \param[in] byte：要发送的字节
 *
 * \return 无
 */
extern void stc15_uart2_sendbyte (uint8_t byte);

/**
 * \brief 串口2发送多个字节
 *
 * \param[in] p_bytes：要发送的数据地址
 * \param[in] len：要发送的字节数
 *
 * \return 无
 */
extern void stc15_uart2_sendnbytes (const uint8_t *p_bytes, uint8_t len);

/**
 * \brief 串口2发送字符串
 */
extern void stc15_uart2_sendstr (const uint8_t *p_bytes);


#endif