/*==============================================================================
文件名：stc15_spi.h
修改记录：
    - 朱凯飞 2018.09.17 创建
文件说明：SPI驱动程序
注意：15F和15L系列单片机不支持从模式；15W系列主从模式都支持。
==============================================================================*/
#ifndef __STC15_SPI_H
#define __STC15_SPI_H

#include "stc15f2k60s2.h"
#include "types.h"
#include "prj_config.h"

/* 定义SS使能管脚。主模式下有效。SS是一个输入信号，将SPI初始化为主模式时若要在
   据之前将SS拉低，发送完一个流（可能包含多个字节）后拉高，则需要手动操作IO口 */
sbit SPI_SS = P1^2; 

/* 由于SPI读写接口公用一个，因此在调用stc15_spi_wr_byte只读不写时应传入该值 */
#define STC15_SPI_NODATA    255

/**
 * \area  SPI控制寄存器
 * @{
 */
#define SPCTL_SSIG      (1 << 7)    /* SS引脚忽略控制位。0：由SPCTL_MSTR位确定器件主从模式；1：由SS引脚确定器件主从模式(0不忽略；1忽略） */
#define SPCTL_SPEN      (1 << 6)    /* SPI使能位 */
#define SPCTL_DORD      (1 << 5)    /*设定SPI位收发顺序。0：高位先发；1：低位先发 */
#define SPCTL_MSTR      (1 << 4)    /* 主从模式选择位。0：从机；1：主机 */
#define SPCTL_CPOL      (1 << 3)    /* SPI时钟极性。0：SCLK空闲时为低电平，前沿为上升沿，后沿为下降沿；1则相反 */
#define SPCTL_CPHA      (1 << 2)    /* 时钟相位选择。0：数据在SS为低（SSIG=0）时被驱动，在SCLK的后沿被改变并在前沿被采样；1：数据在SCLK的前沿驱动，后沿采样 */
#define SPCTL_SPR_MASK  (0x3 << 0)  /* SPI时钟频率选择控制位 */
/* STC15W与STC15F/L系列单片机在配置相同的情况下时钟分频系数不同 */
#if (SOC_TYPE & SOC_STAIR2_MASK == STC15W)
#define SPCTL_SPR_CPUCLK_DIV4    (0 << 0)    /* 系统时钟的4分频 */
#define SPCTL_SPR_CPUCLK_DIV8    (1 << 0)    /* 系统时钟的8分频 */
#define SPCTL_SPR_CPUCLK_DIV16   (2 << 0)    /* 系统时钟的16分频 */
#define SPCTL_SPR_CPUCLK_DIV32   (3 << 0)    /* 系统时钟的32分频 */
#elif ((SOC_TYPE & SOC_STAIR2_MASK == STC15F) || (SOC_TYPE & SOC_STAIR2_MASK == STC15L))
#define SPCTL_SPR_CPUCLK_DIV4    (0 << 0)    /* 系统时钟的4分频 */
#define SPCTL_SPR_CPUCLK_DIV16   (1 << 0)    /* 系统时钟的16分频 */
#define SPCTL_SPR_CPUCLK_DIV64   (2 << 0)    /* 系统时钟的64分频 */
#define SPCTL_SPR_CPUCLK_DIV128  (3 << 0)    /* 系统时钟的128分频 */
#endif
/** @} */

/**
 * \area  SPI状态寄存器
 * @{
 */
#define SPSTAT_SPIF     (1 << 7)    /* SPI传输完成标识。可用来触发中断。写1清零 */
#define SPSTAT_WCOL     (1 << 6)    /* SPI写冲突标识。正在传输时写SPDAT寄存器则置位。写1清零*/
/** @} */

#define IE2_ESPI        (1 << 1)    /* SPI中断允许位 */

/**
 * \area  SPI引脚位置切换
 * @{
 */
#define P_SW1_SPIS_MASK (0x3 << 2)  
#define P_SW1_SPIS0     (0 << 2)    /* SS--P1.2  MOSI--P1.3  MISO--P1.4  SCLK--P1.5 */
#define P_SW1_SPIS1     (1 << 2)    /* SS--P2.4  MOSI--P2.3  MISO--P2.2  SCLK--P2.1 */
#define P_SW1_SPIS2     (2 << 2)    /* SS--P5.4  MOSI--P4.0  MISO--P4.1  SCLK--P4.3 */
#define P_SW1_SPIS3     (3 << 2)    /* 无效 */
/** @} */


/************************************函数声明***********************************/
/**
 * \brief SPI初始化
 */
extern void stc15_spi_init (void);

/**
 * \brief SPI发送数据
 *
 * \brief param[in] byte：要发送的字节
 *
 * \retval 读取到的字节
 *
 * \note 只读不写的时候传入的byte应为STC15_SPI_NODATA
 */
extern uint8_t stc15_spi_wr_byte (uint8_t byte);

#endif