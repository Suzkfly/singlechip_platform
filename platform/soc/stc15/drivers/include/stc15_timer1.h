/*==============================================================================
文件名：stc15_timer1.h
修改记录：
    - 朱凯飞 2018.09.16 创建
文件说明：定时器1驱动程序
==============================================================================*/
#ifndef __STC15_TIMER1_H
#define __STC15_TIMER1_H

#include "stc15f2k60s2.h"
#include "types.h"

#define TMOD_T1_MASK       (0x0F << 4)
#define TMOD_T1_GATE       (1 << 7)    /* 置1时只有EINT0脚为1且TR0为1时才能打开定时/计数器 */
#define TMOD_T1_CT         (1 << 6)    /* 置0对内部时钟源记数（作定时器），置1对外部时钟源记数（计数器） */
#define TMOE_T1_MODE_MASK  (0x3 << 4)  /* 定时器1工作模式 */
#define TMOD_T1_MODE0      (0 << 4)    /* 16位自动重装载模式 */
#define TMOD_T1_MODE1      (1 << 4)    /* 16位非自动重装载模式 */
#define TMOD_T1_MODE2      (2 << 4)    /* 8位自动重装载模式 */
#define TMOD_T1_MODE3      (3 << 4)    /* 定时/计数器无效 */

#define AUXR_T1x12         (1 << 6)    /* 定时器1速度控制位。0：12分频；1：不分频 */

#define INT_CLKO_T1CLKO    (1 << 1)    /* 为1时将P3.4配置为定时器1的时钟输出，定时器1每溢出一次，P3.4翻转一次 */

/**
 * \brief 初始化定时器1
 *
 * \param[in]  overflow_us ：溢出时间
 *
 * \note 12M晶振下最长定时65536us，11.0592M晶振下最长定时71111us
 * \note 定时器默认12分频
 */
extern void stc15_timer1_init (double overflow_us);

#endif