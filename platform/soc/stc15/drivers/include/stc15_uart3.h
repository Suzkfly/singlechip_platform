/*==============================================================================
文件名：stc15_uart3.h
修改记录：
    - 朱凯飞 2018.09.15 创建
文件说明：串口3驱动文件
==============================================================================*/
#ifndef __STC15_UART3_H
#define __STC15_UART3_H

#include "stc15f2k60s2.h"
#include "types.h"

/********************************常用寄存器位**********************************/
#define S3CON_S3SM0      (1 << 7)    /* 串行口3的工作方式，为0时8位模式，为1时为9位模式*/
#define S3CON_S3ST3      (1 << 6)    /* 0：定时器2作为波特率发生器；1：定时器3作为波特率发生器 */
#define S3CON_S3REN      (1 << 4)    /* 允许接收 */
#define S3CON_S3TI       (1 << 1)    /* 发送中断请求 */
#define S3CON_S3RI       (1 << 0)    /* 接收中断请求 */

#define PCON_SMOD        (1 << 7)    /* 为1时串口1,2,3的波特率都加倍，否则不加倍 */

#define T4T3M_T3R        (1 << 3)    /* 定时器3运行控制位 */
#define T4T3M_T3x12      (1 << 1)    /* 定时器3速度控制位，0：为系统时钟的12分频。1：不分频 */

#define IE2_ET3          (1 << 5)    /* 定时器3中断允许位 */
#define IE2_ES3          (1 << 3)    /* 串行口3中断允许位 */

#define P_SW2_S3_S       (1 << 0)    /* 串口3所在位置，为0时：RXD:P0.0,TXD:P0.1，为1时：RXD:P5.0,TXD:P5.1 */

/**
 * \brief 串口3初始化
 *
 * \param[in] baud_rate：波特率
 *
 * \return 无
 *
 * \note 11.0592MHz晶振下最低波特率42，最高波特率2764800，串行口2只能选用定时器2作为波特率发生器
 */
extern void stc15_uart3_init (uint32_t baud_rate);

/**
 * \brief 串口3发送一个字节
 *
 * \param[in] byte：要发送的字节
 *
 * \return 无
 */
extern void stc15_uart3_sendbyte (uint8_t byte);

/**
 * \brief 串口3发送多个字节
 *
 * \param[in] p_bytes：要发送的数据地址
 * \param[in] len：要发送的字节数
 *
 * \return 无
 */
extern void stc15_uart3_sendnbytes (const uint8_t *p_bytes, uint8_t len);

/**
 * \brief 串口3发送字符串
 */
extern void stc15_uart3_sendstr (const uint8_t *p_bytes);

#endif