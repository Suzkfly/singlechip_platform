/*==============================================================================
文件名：文件名：stc15_wdt.h
修改记录：
    - 朱凯飞 2018.10.08 创建
文件说明：看门狗驱动文件
==============================================================================*/
#ifndef __STC15_WDT_H
#define __STC15_WDT_H

#include "includes.h"

#define WDT_CONTR_WDT_FLAG      (1 << 7)    /* 看门狗溢出标识。由硬件置一，软件清零 */
#define WDT_CONTR_EN_WDT        (1 << 5)    /* 启动看门狗 */
#define WDT_CONTR_CLR_WDT       (1 << 4)    /* 写1喂狗 */
#define WDT_CONTR_IDLE_WDT      (1 << 3)    /* 0：看门狗在空闲模式下不计数；1：空闲模式下记数 */
#define WDT_CONTR_PS_MASK       (0x7 << 0)  /* 看门狗分频，分频值为2^n */


/**
 * \brief 看门狗初始化
 *
 * \param[in] prescaler：预分频值为2^prescaler，范围0~7
 *
 * \note 看门狗溢出时间 = (12 * 2^prescaler * 32768) / FOSC
 */
extern void stc15_wdt_init (uint8_t prescaler);

/**
 * \brief 喂狗
 */
extern void stc15_wdt_feed (void);

#endif
