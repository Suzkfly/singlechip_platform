/*==============================================================================
文件名：delay.h
修改记录：
    - 朱凯飞 2018.09.15 创建
文件说明：软件延时（只能大概延时）
==============================================================================*/
#ifndef __DELAY_H
#define __DELAY_H

#include "types.h"

/**
 * \brief 毫秒软件延时函数
 */
extern void delay_ms (uint16_t xms);

#endif
