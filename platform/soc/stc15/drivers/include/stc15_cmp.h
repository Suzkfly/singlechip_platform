/*==============================================================================
文件名：stc15_cmp.h
修改记录：
    - 朱凯飞 2018.10.05 创建
文件说明：比较器驱动文件
==============================================================================*/
#ifndef __STC15_CMP_H
#define __STC15_CMP_H

#include "includes.h"

#define CMPCR1_CMPEN        (1 << 7)    /* 使能比较器模块 */
#define CMPCR1_CMPIF        (1 << 6)    /* 比较器中断标志，需要软件清除 */
#define CMPCR1_PIE          (1 << 5)    /* 比较器上升沿中断使能标志 */
#define CMPCR1_NIE          (1 << 4)    /* 比较器下降沿中断使能标志 */
#define CMPCR1_PIS          (1 << 3)    /* 比较器正极选择位。0：外部P5.5为比较器的正极输入端；1：由ADCIS[2:0]决定 */
#define CMPCR1_NIS          (1 << 2)    /* 比较器负极选择位。0：外部P5.4为比较器的正极输入端；1：选择内部BandGap电压BGV为比较器负极输入端 */
#define CMPCR1_CMPOE        (1 << 1)    /* 比较结果输出控制位。1：将比较结果输出到P1.2 */
#define CMPCR1_CMPRES       (1 << 0)    /* 比较器比较结果标志位。0：CMP+ > CMP-；1：CMP+ < CMP- */
    
#define CMPCR2_INVCMPO      (1 << 7)    /* 比较器输出取反控制位，CMPCR1_CMPOE为1时有效。0：正常输出；1结果取反再输出到P1.2 */
#define CMPCR2_DISFLT       (1 << 6)    /* 去除比较器输出的0.1us滤波 */
#define CMPCR2_LCDTY_MASK   (0x3F << 0) /* 比较滤波时钟个数。比较过程必须持续这么多个时钟后才认为发生了比较事件 */

/**
 * \brief 初始化比较器模块
 *
 * \note P5.5作为CMP+，P5.4作为CMP-，比较结果输出到P1.2
 */
extern void stc15_cmp_init (void);

#endif
