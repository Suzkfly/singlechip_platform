/*==============================================================================
文件名：stc15_uart4.h
修改记录：
    - 朱凯飞 2018.09.15 创建
文件说明：串口4驱动文件
==============================================================================*/
#ifndef __STC15_UART4_H
#define __STC15_UART4_H

#include "stc15f2k60s2.h"
#include "types.h"

/********************************常用寄存器位**********************************/
#define S4CON_S4SM0      (1 << 7)    /* 串行口4的工作方式，为0时8位模式，为1时为9位模式*/
#define S4CON_S4ST4      (1 << 6)    /* 0：定时器2作为波特率发生器；1：定时器4作为波特率发生器 */
#define S4CON_S4REN      (1 << 4)    /* 允许接收 */
#define S4CON_S4TI       (1 << 1)    /* 发送中断请求 */
#define S4CON_S4RI       (1 << 0)    /* 接收中断请求 */

#define T4T3M_T4R        (1 << 7)    /* 定时器4运行控制位 */
#define T4T3M_T4x12      (1 << 5)    /* 定时器4速度控制位，0：为系统时钟的12分频。1：不分频 */

#define IE2_ET4          (1 << 6)    /* 定时器4中断允许位 */
#define IE2_ES4          (1 << 4)    /* 串行口4中断允许位 */

#define P_SW2_S4_S       (1 << 0)    /* 串口4所在位置，为0时：RXD:P0.2,TXD:P0.3，为1时：RXD:P5.2,TXD:P5.3 */

/**
 * \brief 串口4初始化
 *
 * \param[in] baud_rate：波特率
 *
 * \return 无
 *
 * \note 11.0592MHz晶振下最低波特率42，最高波特率2764800，串行口2只能选用定时器2作为波特率发生器
 */
extern void stc15_uart4_init (uint32_t baud_rate);

/**
 * \brief 串口4发送一个字节
 *
 * \param[in] byte：要发送的字节
 *
 * \return 无
 */
extern void stc15_uart4_sendbyte (uint8_t byte);

/**
 * \brief 串口4发送多个字节
 *
 * \param[in] p_bytes：要发送的数据地址
 * \param[in] len：要发送的字节数
 *
 * \return 无
 */
extern void stc15_uart4_sendnbytes (const uint8_t *p_bytes, uint8_t len);


#endif