/*==============================================================================
文件名：stc15_pca.h
修改记录：
    - 朱凯飞 2018.10.06 创建
文件说明：PCA功能驱动文件，包括比较/捕获和PWM功能
==============================================================================*/
#ifndef __STC15_PCA_H
#define __STC15_PCA_H

/**
 * \area PCA工作模式寄存器CMOD
 * @{
 */
#define CMOD_CIDL               (1 << 7)    /* 空闲模式下是否停止PCA计数器。0：继续工作；1：停止 */

#define CMOD_CPS_MASK           (0x7 << 1)  /* 计数脉冲源选择 */
#define CMOD_CPS_SYSCLK_DIV12   (0 << 1)    /* 系统时钟的12分频 */
#define CMOD_CPS_SYSCLK_DIV2    (1 << 1)    /* 系统时钟的2分频 */
#define CMOD_CPS_TIMER0_OF      (2 << 1)    /* 定时器0的溢出脉冲 */
#define CMOD_CPS_ECI            (3 << 1)    /* ECI引脚（P1.2/P3.4/P2.4)的输入时钟，最大SYSCLK/2 */
#define CMOD_CPS_SYSCLK         (4 << 1)    /* 系统时钟 */
#define CMOD_CPS_SYSCLK_DIV4    (5 << 1)    /* 系统时钟的4分频 */
#define CMOD_CPS_SYSCLK_DIV6    (6 << 1)    /* 系统时钟的6分频 */
#define CMOD_CPS_SYSCLK_DIV8    (7 << 1)    /* 系统时钟的8分频 */

#define CMOD_CPS_ECF            (1 << 0)    /* PCA计数溢出中断使能 */
/** @} */

/**
 * \area PCA控制寄存器CCON
 * @{
 */
#define CCON_CF                 (1 << 7)    /* PCA计数器溢出标识位 */
#define CCON_CR                 (1 << 6)    /* 启动PCA计数器 */
#define CCON_CCF2               (1 << 2)    /* PCA模块2中断标识，必须由软件清零 */
#define CCON_CCF1               (1 << 1)    /* PCA模块1中断标识，必须由软件清零 */
#define CCON_CCF0               (1 << 0)    /* PCA模块0中断标识，必须由软件清零 */
/** @} */

/**
 * \area PCA比较/捕获寄存器 CCAPM0
 * @{
 */
#define CCAPM0_ECOM0            (1 << 6)    /* 使能比较器功能。当使用PWM和匹配功能是需要使能 */
#define CCAPM0_CAPP0            (1 << 5)    /* 允许上升沿捕获 */
#define CCAPM0_CAPN0            (1 << 4)    /* 允许下降沿捕获 */
#define CCAPM0_MAT0             (1 << 3)    /* 允许匹配（PCA计数值与模块的比较/捕获寄存器的值的匹配将置位CCF0） */
#define CCAPM0_TOG0             (1 << 2)    /* 翻转控制（PCA计数值与模块的比较/捕获寄存器的值的匹配将使CCP0脚（P1.1/P3.5/P2.5）翻转）*/
#define CCAPM0_PWM0             (1 << 1)    /* 允许CCP0脚用作脉宽调节输出 */
#define CCAPM0_ECCF0            (1 << 0)    /* 使能CCF0（比较/捕获）中断 */
/** @} */

/**
 * \area PCA比较/捕获寄存器 CCAPM1
 * @{
 */
#define CCAPM0_ECOM1            (1 << 6)    /* 使能比较器功能。当使用PWM和匹配功能是需要使能 */
#define CCAPM0_CAPP1            (1 << 5)    /* 允许上升沿捕获 */
#define CCAPM0_CAPN1            (1 << 4)    /* 允许下降沿捕获 */
#define CCAPM0_MAT1             (1 << 3)    /* 允许匹配（PCA计数值与模块的比较/捕获寄存器的值的匹配将置位CCF1） */
#define CCAPM0_TOG1             (1 << 2)    /* 翻转控制（PCA计数值与模块的比较/捕获寄存器的值的匹配将使CCP1脚（P1.0/P3.6/P2.6）翻转）*/
#define CCAPM0_PWM1             (1 << 1)    /* 允许CCP1脚用作脉宽调节输出 */
#define CCAPM0_ECCF1            (1 << 0)    /* 使能CCF1（比较/捕获）中断 */
/** @} */

/**
 * \area PCA比较/捕获寄存器 CCAPM2
 * @{
 */
#define CCAPM0_ECOM2            (1 << 6)    /* 使能比较器功能。当使用PWM和匹配功能是需要使能 */
#define CCAPM0_CAPP2            (1 << 5)    /* 允许上升沿捕获 */
#define CCAPM0_CAPN2            (1 << 4)    /* 允许下降沿捕获 */
#define CCAPM0_MAT2             (1 << 3)    /* 允许匹配（PCA计数值与模块的比较/捕获寄存器的值的匹配将置位CCF2） */
#define CCAPM0_TOG2             (1 << 2)    /* 翻转控制（PCA计数值与模块的比较/捕获寄存器的值的匹配将使CCP2脚（P3.7/P2.7）翻转）*/
#define CCAPM0_PWM2             (1 << 1)    /* 允许CCP2脚用作脉宽调节输出 */
#define CCAPM0_ECCF2            (1 << 0)    /* 使能CCF2（比较/捕获）中断 */
/** @} */

/**
 * \area PCA模块PWM寄存器 PCA_PWM0
 * @{
 */
#define PCA_PWM0_EBS_MASK       (0x3 << 6)  /* PWM工作模式 */
#define PCA_PWM0_EBS_8BITS      (0 << 6)    /* PCA模块0工作在8位PWM模式 */
#define PCA_PWM0_EBS_7BITS      (1 << 6)    /* PCA模块0工作在7位PWM模式 */
#define PCA_PWM0_EBS_6BITS      (2 << 6)    /* PCA模块0工作在6位PWM模式 */

#define PCA_PWM0_EPC0H          (1 << 1)    /* PWM模式下与CCAP0H组成9位数 */
#define PCA_PWM0_EPC0L          (1 << 0)    /* PWM模式下与CCAP0L组成9位数 */
/** @} */

/**
 * \area PCA模块PWM寄存器 PCA_PWM1
 * @{
 */
#define PCA_PWM1_EBS_MASK       (0x3 << 6)  /* PWM工作模式 */
#define PCA_PWM1_EBS_8BITS      (0 << 6)    /* PCA模块1工作在8位PWM模式 */
#define PCA_PWM1_EBS_7BITS      (1 << 6)    /* PCA模块1工作在7位PWM模式 */
#define PCA_PWM1_EBS_6BITS      (2 << 6)    /* PCA模块1工作在6位PWM模式 */

#define PCA_PWM1_EPC0H          (1 << 1)    /* PWM模式下与CCAP1H组成9位数 */
#define PCA_PWM1_EPC0L          (1 << 0)    /* PWM模式下与CCAP1L组成9位数 */
/** @} */

/**
 * \area PCA模块PWM寄存器 PCA_PWM2
 * @{
 */
#define PCA_PWM2_EBS_MASK       (0x3 << 6)  /* PWM工作模式 */
#define PCA_PWM2_EBS_8BITS      (0 << 6)    /* PCA模块2工作在8位PWM模式 */
#define PCA_PWM2_EBS_7BITS      (1 << 6)    /* PCA模块2工作在7位PWM模式 */
#define PCA_PWM2_EBS_6BITS      (2 << 6)    /* PCA模块2工作在6位PWM模式 */

#define PCA_PWM2_EPC0H          (1 << 1)    /* PWM模式下与CCAP2H组成9位数 */
#define PCA_PWM2_EPC0L          (1 << 0)    /* PWM模式下与CCAP2L组成9位数 */
/** @} */

/**
 * \area 切换PCA管脚
 * @{
 */
#define P_SW1_CCP_S_MASK        (0x3 << 4)
#define P_SW1_CCP_S1            (0 << 4)    /* P1.2--ECI  P1.1--CCP0  P1.0--CCP1  P3.7--CCP2 */
#define P_SW1_CCP_S2            (1 << 4)    /* P3.4--ECI  P3.5--CCP0  P3.6--CCP1  P3.7--CCP2 */
#define P_SW1_CCP_S3            (2 << 4)    /* P2.4--ECI  P2.5--CCP0  P2.6--CCP1  P2.7--CCP2 */
/** @} */

#endif
