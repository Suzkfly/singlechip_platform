/*==============================================================================
文件名：文件名：stc15_power_control.h
修改记录：
    - 朱凯飞 2018.10.27 创建
文件说明：电源管理驱动文件
==============================================================================*/
#ifndef __STC15_POWER_CONTROL_H
#define __STC15_POWER_CONTROL_H

#include "includes.h"

#define PCON_LVDF      (1 << 5)    /* 低压检测标识，同时也是低压检测重点请求标志位 */
#define PCON_POF       (1 << 4)    /* 上电复位标识 */
#define PCON_GF1       (1 << 3)    /* 通用标识1，可以随意使用 */
#define PCON_GF0       (1 << 2)    /* 通用标识0，可以随意使用 */
#define PCON_PD        (1 << 1)    /* 置一后进入POWER DOWN模式无时钟工作，支持外部中断唤醒INT0/P3.2,INT1/P3.3,INT2/P3.6,INT3/P37,INT4/P3.0,管脚CCP0~2,RXD1~4,T0~4 */
#define PCON_IDL       (1 << 0)    /* 置一后进入IDLE模式不给CPU时钟，其余部件任可工作，可由外部中断，定时器中断，低压检测中断和AD转换中断唤醒 */

/**
 * \brief 单片机进入掉电模式
 */
extern void stc15_power_down (void);

/**
 * \brief 单片机进入空闲模式
 */
extern void stc15_idle (void);

#endif
