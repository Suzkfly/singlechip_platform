/*==============================================================================
文件名：stc15_uart1.h
修改记录：
    - 朱凯飞 2018.09.15 创建
文件说明：串口1驱动文件
==============================================================================*/
#ifndef __STC15_UART1_H
#define __STC15_UART1_H

#include "includes.h"

/********************************常用寄存器位**********************************/
#define SCON_SM_MASK    (3 << 6)    /*串口1工作方式 */
#define SCON_SM_MODE0   (0 << 6)    /* 同步移位串行方式 */
#define SCON_SM_MODE1   (1 << 6)    /* 8位UART，波特率可变 */
#define SCON_SM_MODE2   (2 << 6)    /* 9位UART */
#define SCON_SM_MODE3   (3 << 6)    /* 9位波特率，波特率可变 */
#define SCON_REN        (1 << 4)    /* 允许接收 */

#define PCON_SMOD       (1 << 7)    /* 为1时串口1,2,3的波特率都加倍，否则不加倍 */

#define AUXR_T1x12      (1 << 6)    /* 定时器12分频控制，为0时定时器1的速率为系统时钟的1/12 */
#define AUXR_SIST2      (1 << 0)    /* 选择定时器作为串口1波特率发生器，0：定时器1,1：定时器2 */

#define AUXR1_S1_MASK       (3 << 6)    /* 串行口1所在位置 */
#define AUXR1_S1_P30_P31    (0 << 6)    /* P3.0为RXD，P3.1为TXD */
#define AUXR1_S1_P36_P37    (1 << 6)    /* P3.6为RXD，P3.7为TXD */
#define AUXR1_S1_P16_P17    (2 << 6)    /* P1.6为RXD，P1.7为TXD（要使用内部时钟） */
//TODO 将引脚切换到P30 P31以外的引脚好像不起作用

/**
 * \brief 串口1初始化
 *
 * \param[in] baud_rate：波特率
 *
 * \return 无
 *
 * \note 11.0592MHz晶振下最低波特率42，最高波特率2764800
 */
extern void stc15_uart1_init (uint32_t baud_rate);

/**
 * \brief 串口1发送一个字节
 *
 * \param[in] byte：要发送的字节
 *
 * \return 无
 */
extern void stc15_uart1_sendbyte (uint8_t byte);

/**
 * \brief 串口1发送多个字节
 *
 * \param[in] p_bytes：要发送的数据地址
 * \param[in] len：要发送的字节数
 *
 * \return 无
 */
extern void stc15_uart1_sendnbytes (const uint8_t *p_bytes, uint8_t len);

/**
 * \brief 串口1发送字符串
 */
extern void stc15_uart1_sendstr (const uint8_t *p_bytes);

#endif