/*==============================================================================
文件名：stc15_demo_cmp.c
修改记录：
    - 朱凯飞 2018.10.05 创建
文件说明：STC15单片机内部比较器测试例程
操作步骤：
    1. 连接串口和串口调试助手，设置波特率为9600。（P3.0为RXD，P3.1为TXD）
    2. P5.5连接CMP+，P5.4连接CMP-，P1.2连接LED。

实验现象：
    1. 串口打印比较结果
    2. LED状态显示结果 
==============================================================================*/
#include "stc15_cmp.h"
#include "stc15_uart1.h"
#include "stdio.h"

/* 发生比较事件的标识 */
bit cmp_occur_flag = 0;

/**
 * \brief 比较器例程入口
 */
void demo_stc15_cmp_entry (void)
{
    stc15_cmp_init();
    stc15_uart1_init(9600);

    while (1) {
        if (cmp_occur_flag) {
            cmp_occur_flag = 0;

            if (CMPCR1 & CMPCR1_CMPRES) {   /* CMP- > CMP+ */
                stc15_uart1_sendstr("Cmp Occured : CMP+ > CMP-\r\n");
            } else {
                stc15_uart1_sendstr("Cmp Occured : CMP+ < CMP-\r\n");
            }
        }
    }
}

/**
 * \brief 比较中断函数
 */
void stc15_cmp_interrupt (void) interrupt 21
{
    CMPCR1 &= ~CMPCR1_CMPIF;    /* 清除中断标志 */
    cmp_occur_flag = 1;
}

