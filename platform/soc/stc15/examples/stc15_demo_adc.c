/*==============================================================================
文件名：stc15_demo_adc.c
修改记录：
    - 朱凯飞 2018.10.05 创建
文件说明：ADC测试例程
操作步骤：
    1. 连接串口和串口调试助手，设置波特率为9600。（P3.0为RXD，P3.1为TXD）
    2. 将P1.7 连接要采集电压的点

实验现象：
    1. 串口调试助手上每隔0.1秒打印出ADC采样值
说明：
    1. STC15的AD采集通道连接P1.0~P1.7
==============================================================================*/
#include "stc15_adc.h"
#include "stc15_uart1.h"
#include "delay.h"
#include "stdio.h"

/* 定义要进行转换的ADC通道 */
#define ADC_CHANNEL     7


/**
 * \brief ADC测试例程入口
 */
void demo_stc15_adc_entry (void)
{
    uint16_t vol;
    char show_buf[32] = { 0 };

    stc15_uart1_init(9600);    
    stc15_adc_init();

    while (1) {
        vol = stc15_get_vol(ADC_CHANNEL);
        printf("chan vol:%d mv\r\n", vol);
        vol = stc15_get_sys_vol();
        printf("system vol:%dmv\r\n", vol);

        delay_ms(1000);
    }
}
