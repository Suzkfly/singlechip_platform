/*==============================================================================
文件名：stc15_demo_ccp.c
修改记录：
    - 朱凯飞 2018.10.06创建
文件说明：CCP例程

操作步骤：
       1.P1.1连接需要测量的脉宽 （可以使用按键）
实验现象：
       1. 每次捕获到串口都打印出捕获的脉宽。如果长时间没有捕获到，则会打印出溢出
          信息 (11.0592MHz下约388秒 
==============================================================================*/
#include "stc15_ccp.h"
#include "stdio.h"
#include "stc15_uart1.h"

static uint16_t __g_overflow_cnt = 0;
static bit __g_ccp_flag = 0; 
static bit __g_overflow_flag = 0;


/**
 * \brief 获取捕获时间
 */
static float get_ccp_time (void)
{
     float ccp_cnt = 0;
     float time_us = 0;

     ccp_cnt = (float)__g_overflow_cnt * 65536 + (CCAP0H << 8) + CCAP0L;
     __g_overflow_cnt = 0;

     time_us = (float)ccp_cnt * 1000000 / SYS_CLK;

     return time_us;
}


/**
 * \brief CCP测试例程
 */
void stc15_demo_ccp_entry (void)
{
    float ccp_time = 0;

    stc15_uart1_init(9600);
    printf("system startup\r\n");

    stc15_ccp_init();
    while (1) {
       
        if (__g_ccp_flag) {
            __g_ccp_flag = 0;

            ccp_time = get_ccp_time();

            printf("ccp time:%f us\r\n", ccp_time);
        } 
        
        if (__g_overflow_flag) {
            __g_overflow_flag = 0;

            printf("over flow\r\n");
        } 
    }
}

/**
 * \brief CAP中断
 */
void ccp_interrupt (void) interrupt 7
{
    if (CCON & CCON_CCF0) {
        CCON &= ~CCON_CCF0;     /* 通道0匹配 */

        __g_ccp_flag = 1;
    }

    if (CCON & CCON_CF) {
        CCON &= ~CCON_CF;     /* 溢出中断 */

        __g_overflow_cnt++;
        if (__g_overflow_cnt == 0xFFFF) {
            __g_overflow_flag = 1;
        }
    }
}
