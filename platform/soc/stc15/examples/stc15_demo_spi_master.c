/*==============================================================================
文件名：stc15_demo_spi_master.c
修改记录：
    - 朱凯飞 2018.09.16创建
文件说明：SPI主模式例程

操作步骤：
    将逻辑分析仪按如下方式连接：SS--P1.2 MOSI--P1.3  MISO--P1.4  SCLK--P1.5
实验现象：
    SPI持续发出数据，每次数据值加1。
==============================================================================*/
#include "stc15_spi.h"

/**
 * \brief 简易延时函数
 */
static void delay (void)
{
    uint8_t i = 20;
    while (i--);
}

/**
 * \brief SPI主机模式测试例程
 */
void stc15_demo_spi_master_entry (void)
{
    uint8_t i = 0;

    stc15_spi_init();
    while (1) {
        SPI_SS = 0;
        stc15_spi_wr_byte(i++);
        SPI_SS = 1;
        delay();
    }
}