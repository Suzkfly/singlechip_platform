/*==============================================================================
文件名：stc15_demo_enhance_pwm.c
修改记录：
    - 朱凯飞 2018.10.08创建
文件说明：PWM例程

操作步骤：
       1.将逻辑分析仪接在P1.7,P1.6,P2.3,P2.2,P2.1,P3.7
实验现象：
       1. 输出3路互补PWM波，相位偏差1/3个周期，死区时间为3个PWM时钟周期
==============================================================================*/
#include "stc15_enhance_pwm.h"

/**
 * \brief PWM测试例程
 */
void stc15_demo_enhance_pwm_entry (void)
{
    stc15_enhance_pwm_init();
    while (1);
}
