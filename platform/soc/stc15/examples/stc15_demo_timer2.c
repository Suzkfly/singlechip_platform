/*==============================================================================
文件名：stc15_demo_timer2.c
修改记录：
    - 朱凯飞 2018.09.16创建
文件说明：定时器2例程

操作步骤：
    P1.0连接的LED以10Hz的频率闪烁
==============================================================================*/
#include "stc15_timer2.h"
#include "led.h"

/**
 * \brief 定时器2例程
 */
void stc15_demo_timer2 (void)
{
    stc15_timer2_init(50000);
}

/**
 * \brief 定时器2中断服务程序
 */
void stc15_timer2_interrupt(void) interrupt 12
{
    led_toggle(0);
}