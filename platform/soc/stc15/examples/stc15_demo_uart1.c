/*==============================================================================
文件名：stc15_demo_uart1.c
修改记录：
    - 朱凯飞 2018.09.15 创建
文件说明：串口1测试demo
操作步骤：
    1. 连接串口和串口调试助手，设置波特率为9600。（P3.0为RXD，P3.1为TXD）

实验现象：
    1. 串口调试助手上每隔1秒打印出"uart1 test"
    2. 将收到的数据回发
==============================================================================*/
#include "stc15_uart1.h"
#include "string.h"
#include "delay.h"

#define SEND_STR    "uart1 test\r\n"    /* 定义要发送的字符串 */

/**
 * \brief 串口1测试例程入口
 */
void stc15_demo_uart1_entry (void)
{
    stc15_uart1_init(9600);

    while (1) {
        stc15_uart1_sendbyte(0xaa);
        delay_ms(1000);
    }
}


/**
 * \brief 串口1中断函数 ，将收到的数据发回去
 */
void stc15_uart1_interrupt (void) interrupt 4
{
    if (RI) {
        RI = 0;
        SBUF = SBUF;
        while (!TI);
        TI = 0;
    }
}