/*==============================================================================
文件名：stc15_demo_uart4.c
修改记录：
    - 朱凯飞 2018.09.15 创建
文件说明：串口4测试demo
操作步骤：
    1. 连接串口和串口调试助手，设置波特率为9600。（P0.2为RXD，P0.3为TXD）

实验现象：
    1. 串口调试助手上每隔1秒打印出"uart4 test"
    2. 将收到的数据回发
==============================================================================*/
#include "stc15_uart4.h"
#include "string.h"
#include "delay.h"

#define SEND_STR    "uart4 test\r\n"    /* 定义要发送的字符串 */

/**
 * \brief 串口4测试例程入口
 */
void stc15_demo_uart4_entry (void)
{
    stc15_uart4_init(9600);

    while (1) {
        stc15_uart4_sendnbytes(SEND_STR, strlen(SEND_STR));
        delay_ms(1000);
    }
}


/**
 * \brief 串口4中断函数 ，将收到的数据发回去
 */
void stc15_uart4_interrupt (void) interrupt 18
{
    if (S4CON & S4CON_S4RI) {
        S4CON &= ~S4CON_S4RI;
        S4BUF = S4BUF;
        while ((S4CON & S4CON_S4TI) == 0);
        S4CON &= ~S4CON_S4TI;
    }
}