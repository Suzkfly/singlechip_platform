/*==============================================================================
文件名：stc15_demo_timer0.c
修改记录：
    - 朱凯飞 2018.09.16创建
文件说明：定时器0例程

操作步骤：
    P1.0连接的LED以10Hz的频率闪烁
==============================================================================*/
#include "stc15_timer0.h"
#include "led.h"

/**
 * \brief 定时器0例程
 */
void stc15_demo_timer0 (void)
{
    stc15_timer0_init(50000);
}

/**
 * \brief 定时器0中断服务程序
 */
void stc15_timer0_interrupt(void) interrupt 1
{
    led_toggle(0);
}