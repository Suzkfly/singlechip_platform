/*==============================================================================
文件名：stc15_demo_uart1_4.c
修改记录：
    - 朱凯飞 2018.09.15 创建
文件说明：串口4测试demo
操作步骤：
    1. 连接串口和串口调试助手，设置波特率为9600。

实验现象：
    1. 串口1到串口4一起工作，调试助手上打印出"uartx test"，x为串口号
    2. 将收到的数据回发
==============================================================================*/
#include "stc15_uart1.h"
#include "stc15_uart2.h"
#include "stc15_uart3.h"
#include "stc15_uart4.h"
#include "string.h"
#include "delay.h"

#define SEND_STR1    "uart1 test\r\n"    /* 定义要发送的字符串 */
#define SEND_STR2    "uart2 test\r\n"    /* 定义要发送的字符串 */
#define SEND_STR3    "uart3 test\r\n"    /* 定义要发送的字符串 */
#define SEND_STR4    "uart4 test\r\n"    /* 定义要发送的字符串 */

/**
 * \brief 串口4测试例程入口
 */
void stc15_demo_uart1_4_entry (void)
{
    stc15_uart1_init(9600);
    stc15_uart2_init(9600);
    stc15_uart3_init(9600);
    stc15_uart4_init(9600);

    while (1) {
        stc15_uart1_sendnbytes(SEND_STR1, strlen(SEND_STR1));
        stc15_uart2_sendnbytes(SEND_STR2, strlen(SEND_STR2));
        stc15_uart3_sendnbytes(SEND_STR3, strlen(SEND_STR3));
        stc15_uart4_sendnbytes(SEND_STR4, strlen(SEND_STR4));
        delay_ms(1000);
    }
}

/**
 * \brief 串口1中断函数 ，将收到的数据发回去
 */
void stc15_uart1_interrupt (void) interrupt 4
{
    if (RI) {
        RI = 0;
        SBUF = SBUF;
        while (!TI);
        TI = 0;
    }
}

/**
 * \brief 串口2中断函数 ，将收到的数据发回去
 */
void stc15_uart2_interrupt (void) interrupt 8
{
    if (S2CON & S2CON_S2RI) {
        S2CON &= ~S2CON_S2RI;
        S2BUF = S2BUF;
        while ((S2CON & S2CON_S2TI) == 0);
        S2CON &= ~S2CON_S2TI;
    }
}

/**
 * \brief 串口3中断函数 ，将收到的数据发回去
 */
void stc15_uart3_interrupt (void) interrupt 17
{
    if (S3CON & S3CON_S3RI) {
        S3CON &= ~S3CON_S3RI;
        S3BUF = S3BUF;
        while ((S3CON & S3CON_S3TI) == 0);
        S3CON &= ~S3CON_S3TI;
    }
}


/**
 * \brief 串口4中断函数 ，将收到的数据发回去
 */
void stc15_uart4_interrupt (void) interrupt 18
{
    if (S4CON & S4CON_S4RI) {
        S4CON &= ~S4CON_S4RI;
        S4BUF = S4BUF;
        while ((S4CON & S4CON_S4TI) == 0);
        S4CON &= ~S4CON_S4TI;
    }
}