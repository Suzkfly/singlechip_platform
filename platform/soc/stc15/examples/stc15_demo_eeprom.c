/*==============================================================================
文件名：stc15_demo_eeprom.c
修改记录：
    - 朱凯飞 2018.10.02 创建
文件说明：单片机内部EEPRMO测试例程
操作步骤：
    1. 连接串口和串口调试助手，设置波特率为9600。（P3.0为RXD，P3.1为TXD）

实验现象：
    1. 每隔1秒串口打印数据，数据值递增
==============================================================================*/
#include "stc15_iap.h"
#include "delay.h"
#include "stc15_uart1.h"
#include "stdio.h"

#define DATA_LEN    16  /* 定义读写的数据长度 */
#define DATA_ADDR   0   /* 要操作的地址 */

#ifndef DEBUG_INFO
#define DEBUG_INFO  stc15_uart1_sendstr
#endif

/**
 * \brief 单片机内部EEPRMO测试例程入口
 */
void stc15_demo_eeprom_entry (void)
{
    xdata uint8_t dat[DATA_LEN] = { 0 };
    int16_t ret, i;
    char show_buf[32] = { 0 };
    uint16_t int_temp;

    stc15_uart1_init(9600);
    DEBUG_INFO("system start\r\n");

    stc15_iap_init();

    /* 读取数据 */
    ret = stc15_iap_read(DATA_ADDR, dat, DATA_LEN);
    if (ret == -1) {
        DEBUG_INFO("read failed\r\n\r\n");
        delay_ms(1000);
        stc15_soft_reset();
    }

    /* 打印读取出的数据 */
    sprintf(show_buf, "read addr %d success\r\n", DATA_ADDR);
    DEBUG_INFO(show_buf);
    for (i = 0; i < DATA_LEN; i++) {
        int_temp = dat[i];
        sprintf(show_buf, "%d ", int_temp);
        DEBUG_INFO(show_buf);
    }
    DEBUG_INFO("\r\n");

    /* 扇区擦除 */
    ret = stc15_iap_sector_erase(DATA_ADDR / STC15_SECTOR_BYTES);
    if (ret == -1) {
        DEBUG_INFO("erase failed\r\n\r\n");
        delay_ms(1000);
        stc15_soft_reset();
    }

    /* 所有值加1 */
    for (i = 0; i < DATA_LEN; i++) {
        dat[i] = dat[i] + 1;
    }

    /* 数据写入 */
    ret = stc15_iap_write(DATA_ADDR, dat, DATA_LEN);
    if (ret == -1) {
        DEBUG_INFO("write failed\r\n\r\n");
        delay_ms(1000);
        stc15_soft_reset();
    }

    DEBUG_INFO("write success\r\n\r\n");
    delay_ms(1000);
    stc15_soft_reset();

    while (1);
}
