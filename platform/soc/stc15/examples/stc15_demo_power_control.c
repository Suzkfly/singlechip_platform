/*==============================================================================
文件名：stc15_demo_power_control.c
修改记录：
    - 朱凯飞 2018.10.08创建
文件说明：看门狗例程

操作步骤：
       1. 连接串口1（P3.0为RXD,P3.1为TXD）
       2. LED连接P1.0
       3. KEY1连接P1.2, KEY2连接P1.3,KEY3连接P3.2
实验现象：
       1. 系统启动后LED是点亮的，每隔1秒打印"system is running"
       2. 按下KEY1后系统进入IDLE模式，按KEY3可恢复，或按下KEY2系统进入POWER DOWN模式，按KEY3可恢复
==============================================================================*/
#include "includes.h"
#include "stc15_power_control.h"
#include "stc15_uart1.h"
#include "stc15_eint0.h"
#include "stdio.h"
#include "delay.h"
#include "led.h"

sbit KEY1 = P1^2;
sbit KEY2 = P1^3;

/**
 * \brief 电源管理测试例程
 */
void stc15_demo_power_control_entry (void)
{
    stc15_uart1_init(9600);
    led_on(0);
    stc15_eint0_init();
    printf("system startup\r\n");
    while (1) {
        delay_ms(1000);
        printf("system is running\r\n");

        if (KEY1 == 0) {
            printf("system will enter IDLE mode\r\n");
            delay_ms(1000);
            stc15_idle();
        }

        if (KEY2 == 0) {
            printf("system will enter POWER DOWN mode\r\n");
            delay_ms(1000);
            stc15_power_down();
        }
    }
}

/* 外部中断处理函数（什么也不做） */
void eint0_handler (void) interrupt 0
{

}
