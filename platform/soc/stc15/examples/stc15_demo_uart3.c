/*==============================================================================
文件名：stc15_demo_uart3.c
修改记录：
    - 朱凯飞 2018.09.15 创建
文件说明：串口3测试demo
操作步骤：
    1. 连接串口和串口调试助手，设置波特率为9600。（P0.0为RXD，P0.1为TXD）

实验现象：
    1. 串口调试助手上每隔1秒打印出"uart3 test"
    2. 将收到的数据回发
==============================================================================*/
#include "stc15_uart3.h"
#include "string.h"
#include "delay.h"

#define SEND_STR    "uart3 test\r\n"    /* 定义要发送的字符串 */

/**
 * \brief 串口3测试例程入口
 */
void stc15_demo_uart3_entry (void)
{
    stc15_uart3_init(9600);

    while (1) {
        stc15_uart3_sendnbytes(SEND_STR, strlen(SEND_STR));
        delay_ms(1000);
    }
}


/**
 * \brief 串口3中断函数 ，将收到的数据发回去
 */
void stc15_uart3_interrupt (void) interrupt 17
{
    if (S3CON & S3CON_S3RI) {
        S3CON &= ~S3CON_S3RI;
        S3BUF = S3BUF;
        while ((S3CON & S3CON_S3TI) == 0);
        S3CON &= ~S3CON_S3TI;
    }
}