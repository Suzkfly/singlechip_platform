/*==============================================================================
文件名：stc15_demo_timer1.c
修改记录：
    - 朱凯飞 2018.09.16创建
文件说明：定时器1例程

操作步骤：
    P1.0连接的LED以10Hz的频率闪烁
==============================================================================*/
#include "stc15_timer1.h"
#include "led.h"

/**
 * \brief 定时器1例程
 */
void stc15_demo_timer1 (void)
{
    stc15_timer1_init(50000);
}

/**
 * \brief 定时器1中断服务程序
 */
void stc15_timer1_interrupt(void) interrupt 3
{
    led_toggle(0);
}