/*==============================================================================
文件名：stc15_demo_wdt.c
修改记录：
    - 朱凯飞 2018.10.08创建
文件说明：看门狗例程

操作步骤：
       1.连接串口1（P3.0为RXD,P3.1为TXD）
实验现象：
       1. 每隔1秒喂狗一次，串口打印喂狗信息，喂5次狗后就不喂了，系统将复位
==============================================================================*/
#include "stc15_wdt.h"
#include "stc15_uart1.h"
#include "stdio.h"
#include "delay.h"

#define PCON_POF    (1 << 4)    /* 上点电复位标识 */

/**
 * \brief 看门狗测试例程
 */
void stc15_demo_wdt_entry (void)
{
    uint8_t i = 5;
    stc15_uart1_init(9600);
    printf("system reset\r\n");

    if (PCON & PCON_POF) {
        printf("last reset reason is power\r\n");
        PCON &= ~PCON_POF;
    }
    
    stc15_wdt_init(5);  /* 初始化看门狗，在11.0592MHz下大约2.3S溢出 */
    while (1) {
        while (i) {
            i--;
            delay_ms(1000);
            stc15_wdt_feed();
            printf("wdt feed\r\n");
        }
    }
}
