/*==============================================================================
文件名：rc522.h
修改记录：
    - 朱凯飞 2018.09.17 创建
文件说明：RC522驱动程序。暂时只定义了SPI操作方式。
==============================================================================*/
#ifndef __RC522_H
#define __RC522_H

#include "stc15_spi.h"
#include "mifare.h"

/* 以下三个宏只能留一个 */
//#define RC522_INTERFACE_SF_I2C  /* 使用软件I2C接口 */
#define RC522_INTERFACE_HW_SPI  /* 使用硬件SPI接口 */
//#define RC522_INTERFACE_SF_SPI  /* 使用软件SPI接口 */

#ifdef RC522_INTERFACE_SF_I2C
#include "i2c.h"

/* 定义RC522器件地址，低7位有效。
   如果EA管脚为低电平，b[6:3]固定为0101，b[2:0]由管脚ADR[2:0]决定
   如果EA管脚为高电平，则b[5:0]由ADR[5:0]决定，b[6]恒为0 */
#define RC522_I2C_ADDR   0x28
#endif


/***********************************管脚定义***********************************/
/* 为低时切断内部电源；上升沿复位芯片。若修改了管脚定义则要修改mifare_rc522.c中
   的__rc522_nrstpd_pin_conf函数 */
sbit RC522_NRSTPD = P1^6;

#ifndef RC522_INTERFACE_SF_I2C
sbit RC522_SS     = P1^2;
#endif

#ifdef RC522_INTERFACE_SF_SPI
sbit RC522_MOSI = P1^3; 
sbit RC522_MISO = P1^4; 
sbit RC522_SCK  = P1^5; 
#endif

/************************************宏定义************************************/
/**
 * \area 状态码定义
 * @{
 */
#define MIFARE_ST_OK        0
#define MIFARE_ST_ERROR    -1

#define MIFARE_ST_TIMEOUT           -2    /* 超时 */
#define MIFARE_ST_INVALID_VALUE     -3    /* 无效的值 */
/** @} */

/**
 * \area MFRC522寄存器定义
 * @{
 */

/* PAGE 0 */
#define     RFU00                        0X00
#define     RC522_REG_COMMAND            0x01
#define     RC522_REG_COMIEN             0x02
#define     RC522_REG_DIVLEN             0x03
#define     RC522_REG_COMIRQ             0x04
#define     RC522_REG_DIVIRQ             0x05
#define     RC522_REG_ERROR              0x06
#define     RC522_REG_STATUS1            0x07
#define     RC522_REG_STATUS2            0x08
#define     RC522_REG_FIFODATA           0x09
#define     RC522_REG_FIFOLEVEL          0x0A
#define     RC522_REG_WATERLEVEL         0x0B
#define     RC522_REG_CONTROL            0x0C
#define     RC522_REG_BITFRAMING         0x0D
#define     RC522_REG_COLL               0x0E
#define     RC522_REG_RFU0F              0x0F
/* PAGE 1 */
#define     RC522_REG_RFU10              0x10
#define     RC522_REG_MODE               0x11
#define     RC522_REG_TXMODE             0x12
#define     RC522_REG_RXMODE             0x13
#define     RC522_REG_TXCONTROL          0x14
#define     RC522_REG_TXAUTO             0x15    /* 资料上未定义该寄存器 */
#define     RC522_REG_TXSEL              0x16
#define     RC522_REG_RXSEL              0x17
#define     RC522_REG_RXTHRESHOLD        0x18
#define     RC522_REG_DEMOD              0x19
#define     RC522_REG_RFU1A              0x1A
#define     RC522_REG_RFU1B              0x1B
#define     RC522_REG_MIFARE             0x1C
#define     RC522_REG_RFU1D              0x1D
#define     RC522_REG_RFU1E              0x1E
#define     RC522_REG_SERIALSPEED        0x1F
/* PAGE 2 */
#define     RC522_REG_RFU20              0x20  
#define     RC522_REG_CRCRESULTM         0x21
#define     RC522_REG_CRCRESULTL         0x22
#define     RC522_REG_RFU23              0x23
#define     RC522_REG_MODWIDTH           0x24
#define     RC522_REG_RFU25              0x25
#define     RC522_REG_RFCFG              0x26
#define     RC522_REG_GSN                0x27
#define     RC522_REG_CWGSCFG            0x28
#define     RC522_REG_MODGSCFG           0x29
#define     RC522_REG_TMODE              0x2A
#define     RC522_REG_TPRESCALER         0x2B
#define     RC522_REG_TRELOADH           0x2C
#define     RC522_REG_TRELOADL           0x2D
#define     RC522_REG_TCOUNTERVALUEH     0x2E
#define     RC522_REG_TCOUNTERVALUEL     0x2F
/* PAGE 3 */
#define     RC522_REG_RFU30              0x30
#define     RC522_REG_TESTSEL1           0x31
#define     RC522_REG_TESTSEL2           0x32
#define     RC522_REG_TESTPINEN          0x33
#define     RC522_REG_TESTPINVALUE       0x34
#define     RC522_REG_TESTBUS            0x35
#define     RC522_REG_AUTOTEST           0x36
#define     RC522_REG_VERSION            0x37
#define     RC522_REG_ANALOGTEST         0x38
#define     RC522_REG_TESTDAC1           0x39  
#define     RC522_REG_TESTDAC2           0x3A   
#define     RC522_REG_TESTADC            0x3B   
#define     RC522_REG_RFU3C              0x3C   
#define     RC522_REG_RFU3D              0x3D   
#define     RC522_REG_RFU3E              0x3E   
#define     RC522_REG_RFU3F              0x3F
/** @} */


/**
 * \brief MFRC522命令字
 * @{
 */
#define RC522_PCD_IDLE              0x00               /* 取消当前命令    */
#define RC522_PCD_AUTHENT           0x0E               /* 验证密钥        */
#define RC522_PCD_RECEIVE           0x08               /* 接收数据        */
#define RC522_PCD_TRANSMIT          0x04               /* 发送数据        */
#define RC522_PCD_TRANSCEIVE        0x0C               /* 发送并接收数据  */
#define RC522_PCD_RESETPHASE        0x0F               /* 复位            */
#define RC522_PCD_CALCCRC           0x03               /* CRC计算         */
/** @} */


/***********************************函数声明***********************************/

/**
 * \brief 初始化RC522
 */
extern void rc522_init (void);

/**
 * \brief 寻卡，得到卡类型
 *
 * \param[in] req_mode：寻卡方式
 *                  #PICCA_REQIDL    寻天线区内未进入休眠状态的卡
 *                  #PICCA_REQALL    寻天线区内全部卡
 * \param[out] p_atq：保存应答信息的地址
 *            Mifare1 S50    | Mifare1 S70 | Mifare1 Light | Mifare0 UltraLight
 *       --------------------|-------------|---------------|-------------------
 *              0x0004       |    0x0002   |    0x0010     |      0x0044
 *       ----------------------------------------------------------------------
 *          Mifare3 DESFire  |   SHC1101   |    SHC1102    |      11RF32
 *       --------------------|-------------|---------------|-------------------
 *               0x0344      |    0x0004   |    0x3300     |      0x0004
 *
 * \retval 见状态码
 *
 * \note 卡进入天线后，从射频场中获取能量，从而得电复位，复位后卡处于IDLE模式，
 * 用两种请求模式的任一种请求时，此时的卡均能响应；若对某一张卡成功进行了挂起
 * 操作（Halt命令或DeSelect命令），则进入了Halt模式，此时的卡只响应ALL（0x52）
 * 模式的请求，除非将卡离开天线感应区后再进入。
 */
extern int8_t rc522_picca_request (uint8_t req_mode, uint8_t p_atq[2]);

/**
 * \brief 防冲撞
 *
 * \param[out] p_uid：获取到的UID
 *
 * \retval 见状态码
 */
extern int8_t rc522_picca_anticoll(uint8_t p_uid[4]);



/**
 * \brief Mifare卡的选择操作
 *
 *  需要成功执行一次防碰撞命令，并返回成功，才能进行卡选择操作，否则返回错误。
 *
 * \param[in]  anticoll_level : 防碰撞等级，可使用下列宏：
 *                             - #AM_FM175XX_PICCA_ANTICOLL_1
 *                             - #AM_FM175XX_PICCA_ANTICOLL_2
 *                             - #AM_FM175XX_PICCA_ANTICOLL_3
 * \param[in]  uid     : 前一个防碰撞函数获取的UID
 * \param[in]  uid_len : 前一个防碰撞函数获取的UID的长度（字节数）
 * \param[out] p_sak   : 返回的信息，若bit2为1，则表明UID不完整。若不想接收SAK则传入NULL。
 *
 * \retval 操作结果(详细说明见rc522.h中的状态码定义)
 *
 * \note  卡的序列号长度有三种：4字节、7字节和10字节。 4字节的只要用一级选择即可
 * 得到完整的序列号，如Mifare1 S50/S70等；7字节的要用二级选择才能得到完整的序列
 * 号，前一级所得到的序列号的最低字节为级联标志0x88，在序列号内只有后3字节可用，
 * 后一级选择能得到4字节序列号，两者按顺序连接即为7字节序列号，如UltraLight
 * 和DesFire等；10字节的以此类推，但至今还未发现此类卡。
 *
 * sak数据代表的含义：
 *  xxxx x1xx：UID不完整
 *  xx1x x0xx：UID完整，PICC遵循ISO/IEC 14443-4
 *  xx0x x0xx：UID完整，PICC不遵循ISO/IEC 14443-4
 */
int8_t rc522_picca_select (uint8_t p_uid[4], uint8_t p_sak[1]);

/**
 * \brief Mifare卡的挂起操作，使所选择的卡进入HALT状态
 *
 *  在HALT状态下，卡将不响应读卡器发出的IDLE模式的请求，除非将卡复位或离开天线感
 *  应区后再进入。但它会响应读卡器发出的ALL请求。
 *
 * \retval 操作结果(详细说明见rc522.h中的状态码定义)
 */
int8_t rc522_picca_halt (void);

/**
 * \brief Mifare卡密钥验证，将传入的密钥与卡的密钥进行验证
 *
 * \param[in] key_type   : 密钥类型，可以使用下列值：
 *                          - #PICCA_AUTHENT1A
 *                          - #PICCA_AUTHENT1B
 * \param[in] p_uid      : 卡序列号，4字节
 * \param[in] p_key      : 密钥，6字节。默认的密钥都是全1，即6个0xFF
 * \param[in] nblock     : 需要验证的卡块号，取值范围与卡类型有关，
 *                         - S50：0 ~ 63
 *                         - S70：0 ~ 255
 *                         - PLUS CPU 2K：0 ~ 127
 *                         - PLUS CPU 4K：0 ~ 255
 *
 * \retval 见rc522.h中的状态码定义
 *
 * \note PLUS CPU系列的卡的卡号有4字节和7字节之分，对于7字节卡号的卡，
 *       只需要将卡号的高4字节（等级2防碰撞得到的卡号）作为验证的卡号即可。
 *       有些卡不支持HALT指令
 *       加值操作必须先验证秘钥B
 *       减值操作必须先验证秘钥A
 *       块读写也必须验证秘钥，具体验证A还是B取决于控制块（每个扇区最后一个块）
 */
int8_t rc522_picca_authent (uint8_t            key_type,
                            const uint8_t      p_uid[4],
                            const uint8_t      p_key[6],
                            uint8_t            nblock);

/**
 * \brief Mifare卡数据读取
 *
 *     在验证成功之后，才能读相应的块数据，所验证的块号与读块号必须在同一个扇区内，
 * Mifare1卡从块号0开始按顺序每4个块1个扇区，若要对一张卡中的多个扇区进行操作，在
 * 对某一扇区操作完毕后，必须进行一条读命令才能对另一个扇区直接进行验证命令，否则
 * 必须从请求开始操作。对于PLUS CPU卡，若下一个读扇区的密钥和当前扇区的密钥相同，
 * 则不需要再次验证密钥，直接读即可。
 *
 * \param[in]  nblock     : 读取数据的块号
 *                          - S50：0 ~ 63
 *                          - S70：0 ~ 255
 *                          - PLUS CPU 2K：0 ~ 127
 *                          - PLUS CPU 4K：0 ~ 255
 * \param[out] p_buf      : 存放读取的数据，共16bytes
 *
 * \retval 操作结果(详细说明见rc522.h中的状态码定义)
 */
int8_t rc522_picca_read (uint8_t nblock, uint8_t p_buf[16]);

/**
 * \brief Mifare卡写数据，写之前必需成功进行密钥验证。
 *
 *      对卡内某一块进行验证成功后，即可对同一扇区的各个块进行写操作（只要访问条件允许），
 *  其中包括位于扇区尾的密码块，这是更改密码的唯一方法。对于PLUS CPU卡等级2、3的AES密
 *  钥则是在其他位置修改密钥。
 *
 * \param[in] nblock     : 读取数据的块号
 *                         - S50：0 ~ 63
 *                         - S70：0 ~ 255
 *                         - PLUS CPU 2K：0 ~ 127
 *                         - PLUS CPU 4K：0 ~ 255
 * \param[in] p_buf      : 写入数据缓冲区，大小必须为 16
 *
 * \retval 操作结果(详细说明见rc522.h中的状态码定义)
 */
int8_t rc522_picca_write (uint8_t nblock, const uint8_t p_buf[16]);

/**
 * \brief Mifare值操作，设置值块的值
 *
 * \param[in] p_dev      : fm175xx设备
 * \param[in] nblock     : 读取数据的值块地址
 * \param[in] value      : 设置的值
 *
 * \retval 操作结果(详细说明见rc522.h中的状态码定义)
 *
 * \note 该函数使用直接写数据的方式，将指定的块格式转换为数值块，并初始化数值块的值，也可以使用该函数改
 *       变数值块的值。对卡内某一块进行验证成功后，并且访问条件允许才能进行此操作。
 */
int8_t rc522_picca_val_set (uint8_t nblock, int32_t value);

/**
 * \brief Mifare值操作，获取值块的值
 *
 * \param[in]  nblock     : 读取数据的值块地址
 * \param[out] p_value    : 获取值的指针
 *
 * \retval 操作结果(详细说明见rc522.h中的状态码定义)
 *
 * \note 该函数使用直接写数据的方式，将指定的块格式转换为数值块，并初始化数值块的值，也可以使用该函数改
 *       变数值块的值。对卡内某一块进行验证成功后，并且访问条件允许才能进行此操作。
 */
int8_t rc522_picca_val_get (uint8_t nblock, int32_t *p_value);

/**
 * \brief Mifare值操作，对Mifare卡的值块进行加减操作
 *
 * \param[in] mode       : 值操作的模式，可以是加或减，使用下列宏：
 *                          - PICCA_INCREMENT
 *                          - PICCA_DECREMENT
 * \param[in] nblock     : 进行值操作的块号
 *                          - S50：0 ~ 63
 *                          - S70：0 ~ 255
 *                          - PLUS CPU 2K：0 ~ 127
 *                          - PLUS CPU 4K：0 ~ 255
 * \param[in] ntransblk  : 传输块号，计算结果值存放的块号
 * \param[in] value      : 4字节有符号数
 *
 * \retval 操作结果(详细说明见rc522.h中的状态码定义)
 *
 * \note 要进行此类操作，块数据必须要有值块的格式，可以使用
 * rc522_picca_val_set()函数将数据块初始化为值块的格式。可参考NXP的相关文
 * 档。若卡块 * 号与传输块号相同，则将操作后的结果写入原来的块内；若卡块号与传输块号不相同，则将操作后的
 * 结果写入传输块内，结果传输块内的数据被覆盖，原块内的值不变。处于等级2的PLUS CPU卡不支持值块操作，等
 * 级1、3支持。
 *       进行加值操作必须先验证KEYB，进行减值操作必须先验证KEYA。
 *       可以通过传入不同的块，并将value的值设为0来备份数据。
 */
int8_t rc522_picca_val_operate (uint8_t            mode,
                                uint8_t            nblock,
                                uint8_t            ntransblk,
                                int32_t            value);


#endif
