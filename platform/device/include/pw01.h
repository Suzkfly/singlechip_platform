/*==============================================================================
文件名：pw01.h
修改记录：
    - 朱凯飞 2018.10.03 创建
文件说明：pw01蓝牙模块串口透传包接收程序
包数据格式：
    +-------+--------+----------+----------+--------+--------+
    | 帧头  | 包序号 | 指令类型 | 数据长度 | 数据   | 校验和 |
    +=======+========+==========+==========+========+========+
    | 1byte | 1byte  | 1byte    | 1byte    | nbytes | 1byte  |
    +-------+--------+----------+----------+--------+--------+
==============================================================================*/
#ifndef __PW01_H
#define __PW01_H

#include "includes.h"

#define PW01_HEAD           0xAA    /* 帧头 */

#define PW01_MAX_PACK_CNT   5       /* 最多保存的包数 */
#define PW01_MAX_MSG_LEN    32      /* 一条数据中最大长度 */

/**
 * \brief PW01模块初始化
 */
extern void pw01_init (void);

/**
 * \brief 获取数据
 *
 * \param[out] pp_pack：保存数据的地址
 *
 * \retval 获取到的数据包个数
 */
uint8_t pw01_get_pack (uint8_t pp_pack[PW01_MAX_PACK_CNT][PW01_MAX_MSG_LEN]);

/**
 * \brief 对数据包进行和校验
 *
 * \param[in] p_pack：需要校验的数据地址
 *
 * \retval 0：校验通过 1：校验错误
 */
extern int8_t pw01_sum_check (uint8_t *p_pack);

#endif
