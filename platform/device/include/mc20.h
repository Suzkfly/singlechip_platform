/*==============================================================================
文件名：mc20.h
修改记录：
    - 朱凯飞 2018.09.27 创建
文件说明：MC20驱动程序
==============================================================================*/
#ifndef __MC20_H
#define __MC20_H

#include "includes.h"

/* 定义消息盒子中最多存储消息的条数（一个"\r\n"作为一条消息的结尾，一行只有"\r\n"无效 */
#define MAX_AT_MSG_CNT  5

/* 定义一条消息的最大长度 */
#define MAX_AT_MSG_LEN  32

/* 单条消息接收超时时间（一个单位10ms） */
#define RCV_TMOx10MS  20

/**
 * \brief mc20初始化
 *
 * \note 由于mc20使用自动波特率，因此在使用前应先等待模块波特率匹配成功。具体做法为
 *       每隔1秒发送一次"AT\r\n"，直到收到10个"OK\r\n"为止 
 *        调用该函数后要使用 mc20_init_isfinished函数查看是否初始化完成
 */
extern void mc20_init (void);


/**
 * \brief 检查mc20波特率是否初始化完成
 *
 * \retval 0：初始化未完成 1：初始化已完成
 */
extern uint8_t mc20_baud_init_is_finished (void);

/**
 * \brief 获取mc20接收到的消息  
 *
 * \param[out]  pp_msg：存放消息的指针
 *
 * \retval：获取到的消息条数
 */
extern uint8_t mc20_get_msg (uint8_t *pp_msg[MAX_AT_MSG_CNT]);

/**
 * \brief 发送AT指令 
 */
extern void mc20_send_at_cmd (const uint8_t *p_cmd);

#endif