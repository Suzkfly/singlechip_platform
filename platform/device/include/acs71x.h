/*==============================================================================
文件名：acs71x.h
修改记录：
    - 朱凯飞 2022.02.01 创建
文件说明：ACS71x系列流压转换芯片
==============================================================================*/
#ifndef __ACS71X_H
#define __ACS71X_H

/**
 * \brief 灵敏度对照表
 *         型号            测量范围（A)      灵敏度（mV/A）
 *   ACS714ELCTR-05B-T        ±5               185
 *   ACS714ELCTR-20A-T        ±20              100
 *   ACS714ELCTR-30A-T        ±30              66
 *   ACS714LLCTR-05B-T        ±5               185
 *   ACS714LLCTR-20A-T        ±20              100
 *   ACS714LLCTR-30A-T        ±30              66
 *   ACS714LLCTR-50A-T        ±50              40
 */
#define SENSITIVITY  66     /* 灵敏度 */

/**
 * \brief 获取电流
 *
 * \param[in] ch：STC15单片机自带的AD采集通道
 *
 * \retval 电流值（mA）
 *
 * \该函数基于STC15单片机自带的AD转换，如果要用其他的AD转换方法需要修改代码
 */
extern float get_current(uint8_t ch);

#endif
