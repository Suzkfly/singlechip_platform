/*==============================================================================
文件名：ds1302.h
修改记录：
    - 朱凯飞 2019.01.10 创建
文件说明：DS1302驱动文件
==============================================================================*/
#ifndef __DS1302_H
#define __DS1302_H

#include "includes.h"

/* 管脚定义 */
sbit DS1302_SCLK = P2^0;
sbit DS1302_IO = P3^7;
sbit DS1302_RST = P3^6;

/**< \brief 定义DS1302时间格式 */
typedef struct ds1302_time_struct {
    uint8_t year;
    uint8_t mounth;
    uint8_t day;
    uint8_t hour;
    uint8_t min;
    uint8_t sec;
    uint8_t week;
} ds1302_time_t;

/**
 * \brief DS1302引脚初始化
 *
 * \note 需要根据定义的引脚修改
 */
extern void ds1302_init (void);

/**
 *\ brief DS1302设置时间
 *
 * \param[in] p_time 要设置的时间
 */
extern void ds1302_write_time (ds1302_time_t *p_time);

/**
 *\ brief DS1302读取时间
 *
 * \param[out] p_time：读取到的时间
 */
extern void ds1302_read_time (ds1302_time_t *p_time);

#endif
