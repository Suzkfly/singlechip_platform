/*==============================================================================
文件名：at24cxx.h
修改记录：
    - 朱凯飞 2018.10.04 创建
文件说明：AT24CXX驱动
==============================================================================*/
#ifndef __AT24CXX_H
#define __AT24CXX_H

/* AT24CXX的有效地址位数，AT24C02为8，AT24C256为15 */
#define AT24CXX_MEMADDR_BITS    15

/* 定义7位I2C器件地址，低7位有效 */
#define AT24CXX_I2C_ADDR 0x50

#include "includes.h"
#include "i2c.h"


/**
 * \brief 向AT24CXX某个地址写入一个字节
 */
#if (AT24CXX_MEMADDR_BITS == 8)
extern void at24cxx_write_byte (uint8_t addr, uint8_t byte);
#elif ((AT24CXX_MEMADDR_BITS > 8) && (AT24CXX_MEMADDR_BITS <= 16))
extern void at24cxx_write_byte (uint16_t addr, uint8_t byte);
#endif

/**
 * \brief 从AT24CXX某个地址中读出字节
 */
#if (AT24CXX_MEMADDR_BITS == 8)
extern uint8_t at24cxx_read_byte (uint8_t addr);
#elif ((AT24CXX_MEMADDR_BITS > 8) && (AT24CXX_MEMADDR_BITS <= 16))
extern uint8_t at24cxx_read_byte (uint16_t addr);
#endif

#endif
