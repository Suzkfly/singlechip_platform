/*==============================================================================
文件名：led.h
修改记录：
    - 朱凯飞 2018.05.24 创建
文件说明：led驱动
==============================================================================*/
#ifndef __LED_H
#define __LED_H

#include "includes.h"

sbit LED0 = P1^0;    /* 定义LED0引脚 */
sbit LED1 = P1^1;    /* 定义LED1引脚 */

#define LED_ON   0   /* LED点亮电平为0 */
#define LED_OFF  1   /* LED熄灭电平为1 */

/**
 * \area 函数裁剪
 * @{
 */
//#define EN_FUNC_LED_ON
//#define EN_FUNC_LED_OFF
#define EN_FUNC_LED_TOGGLE
/**
 *@}
 */

/**
 * \brief 点亮LED
 *
 * \param[in] index : LED对应编号
 */
void led_on (uint8_t led_index);

/**
 * \brief 熄灭LED
 *
 * \param[in] index : LED对应编号
 */
void led_off (uint8_t led_index);

/**
 * \brief 熄灭LED
 *
 * \param[in] index : LED对应编号
 */
void led_toggle (uint8_t led_index);

#endif
