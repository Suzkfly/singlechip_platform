/*==============================================================================
文件名：motor1.h
修改记录：
    - 朱凯飞 2018.10.23 创建
文件说明：直流电机驱动程序
==============================================================================*/
#ifndef __MOTOR1_H
#define __MOTOR1_H

#include "includes.h"

sbit MOTOR1_INA = P3^6;
sbit MOTOR1_INB = P3^7;

/**
 * \brief 前进
 */
extern void motor1_forward (void);

/**
 * \brief 后退
 */
extern void motor1_back (void);

/**
 * \brief 滑行
 */
extern void motor1_skidding (void);

/**
 * \brief 刹车
 */
extern void motor1_brake (void);

#endif
