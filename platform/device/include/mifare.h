/*==============================================================================
文件名：mifare.h
修改记录：
    - 朱凯飞 2018.09.17 创建
文件说明：RC522驱动程序。暂时只定义了SPI操作方式
==============================================================================*/
#ifndef __MIFARE_H
#define __MIFARE_H

/**
 * \brief Mifare_One卡片命令
 * @{
 */
#define PICCA_REQIDL           0x26               /* 寻天线区内未进入休眠状态的卡*/
#define PICCA_REQALL           0x52               /* 寻天线区内全部卡         */
#define PICCA_ANTICOLL1        0x93               /* 防冲撞等级1              */
#define PICCA_ANTICOLL2        0x95               /* 防冲撞等级2              */
#define PICCA_AUTHENT1A        0x60               /* 验证A密钥                */
#define PICCA_AUTHENT1B        0x61               /* 验证B密钥                */
#define PICCA_READ             0x30               /* 读块                     */
#define PICCA_WRITE            0xA0               /* 写块                     */
#define PICCA_DECREMENT        0xC0               /* 扣款（减值）             */
#define PICCA_INCREMENT        0xC1               /* 充值（加值）             */
#define PICCA_RESTORE          0xC2               /* 调块数据到缓冲区         */
#define PICCA_TRANSFER         0xB0               /* 保存缓冲区中数据         */
#define PICCA_HALT             0x50               /* 休眠                     */
/** @} */

#endif
