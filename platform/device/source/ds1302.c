/*==============================================================================
文件名：ds1302.c
修改记录：
    - 朱凯飞 2019.01.10 创建
文件说明：DS1302驱动文件
==============================================================================*/
#include "ds1302.h"
#include "intrins.h"

/* 读写地址，年月日时分秒星期 */
static uint8_t code DS1302_ADDR_R[7] = {0X8D, 0X89, 0X87, 0X85, 0X83, 0X81, 0X8B};
static uint8_t code DS1302_ADDR_W[7] = {0X8C, 0X88, 0X86, 0X84, 0X82, 0X80, 0X8A};


/**
 * \brief 向DS1302写入一个字节
 */
static void __ds1302_write_byte (uint8_t byte)
{
    unsigned char i;
    for (i = 0; i < 8; i++) { 
        DS1302_SCLK = 0;
        DS1302_IO = byte & 0x01;
        byte >>= 1; 
        DS1302_SCLK = 1;       /* 上升沿有效 */
    }
}

/**
 * \brief 向DS1302的某个地址写入一个字节
 */
static void __ds1302_write_dat (uint8_t addr, uint8_t dat)
{
    DS1302_RST = 0;
    _nop_();
    DS1302_SCLK = 0;
    _nop_();
    DS1302_RST = 1;
    __ds1302_write_byte(addr);
    __ds1302_write_byte(dat);
    DS1302_RST=0;
}


/**
 * \brief 读DS1302中的数据
 */
uint8_t ds1302_read_dat (uint8_t addr)
{
    uint8_t i, dat, dat_flag;
    DS1302_SCLK = 0;
    _nop_();
    DS1302_RST = 1;
    __ds1302_write_byte(addr);
    for (i = 0; i < 8; i++) {
        DS1302_SCLK = 0;
        _nop_();
        dat_flag = DS1302_IO;     /* 下降沿读数据 */
        DS1302_SCLK = 1;
        dat = (dat >> 1) | (dat_flag << 7);
    }
    DS1302_RST=0;
    /* 下面这6句是必要的 */
    _nop_();
    DS1302_SCLK = 1;
    _nop_();
    DS1302_IO = 0;    /* 每次读完数据将IO口拉低一次，这6句中这一句绝对不能删 */
    _nop_();
    DS1302_IO = 1;
    return dat;
}

/**
 * \brief DS1302引脚初始化
 *
 * \note 需要根据定义的引脚修改
 */
void ds1302_init (void)
{
    P2M1 &= 0xFE;
    P2M0 |= 0X01;

    P3M1 &= 0X3F;
    P3M0 &= 0X3F;
    P3M0 |= (1 << 6);
}

/**
 *\ brief DS1302设置时间
 *
 * \param[in] p_time 要设置的时间
 */
void ds1302_write_time (ds1302_time_t *p_time)
{
    __ds1302_write_dat(0x8E, 0x00);          /* 禁止写保护 */
    __ds1302_write_dat(DS1302_ADDR_W[0], p_time->year);
    __ds1302_write_dat(DS1302_ADDR_W[1], p_time->mounth);
    __ds1302_write_dat(DS1302_ADDR_W[2], p_time->day);
    __ds1302_write_dat(DS1302_ADDR_W[3], p_time->hour);
    __ds1302_write_dat(DS1302_ADDR_W[4], p_time->min);
    __ds1302_write_dat(DS1302_ADDR_W[5], p_time->sec);
    __ds1302_write_dat(DS1302_ADDR_W[6], p_time->week);
    __ds1302_write_dat(0x8E, 0x80);          /* 开启写保护 */
}

/**
 * \brief 延时函数
 */
static void __delay (void)
{
    uint16_t i = 1000;
    while (i--);
}


/**
 *\ brief DS1302读取时间
 *
 * \param[out] p_time：读取到的时间
 */
void ds1302_read_time (ds1302_time_t *p_time)
{
    uint8_t i;
    uint8_t *p_temp = NULL;
    
    p_temp = (uint8_t *)p_time;
    for (i = 0; i < 7; i++) {
        *p_temp++ = ds1302_read_dat(DS1302_ADDR_R[i]);
        __delay();
    }
}
