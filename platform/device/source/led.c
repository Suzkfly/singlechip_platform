/*==============================================================================
文件名：led.c
修改记录：
    - 朱凯飞 2018.05.24 创建
文件说明：led驱动
==============================================================================*/
#include "led.h"

/* 点亮LED */
#ifdef EN_FUNC_LED_ON
void led_on (uint8_t led_index)
{
    if (led_index == 0) {
        LED0 = LED_ON;
    } else if (led_index == 1) {
        LED1 = LED_ON;
    }
}
#endif

/* 熄灭LED */
#ifdef EN_FUNC_LED_OFF
void led_off (uint8_t led_index)
{
    if (led_index == 0) {
        LED0 = LED_OFF;
    } else if (led_index == 1) {
        LED1 = LED_OFF;
    }
}
#endif

/* 熄灭LED */
#ifdef EN_FUNC_LED_TOGGLE
void led_toggle (uint8_t led_index)
{
    if (led_index == 0) {
        LED0 = !LED0;
    } else if (led_index == 1){
        LED1 = !LED1;
    }
}

#endif
