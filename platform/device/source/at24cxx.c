/*==============================================================================
文件名：at24cxx.c
修改记录：
    - 朱凯飞 2018.10.04 创建
文件说明：AT24CXX驱动
==============================================================================*/
#include "at24cxx.h"


/**
 * \brief 向AT24CXX某个地址写入一个字节
 */
#if (AT24CXX_MEMADDR_BITS == 8)
void at24cxx_write_byte (uint8_t addr, uint8_t byte)
{
    i2c_start();
    i2c_send_byte(AT24CXX_I2C_ADDR << 1);
    i2c_send_byte(addr);
    i2c_send_byte(byte);
    i2c_stop();
}
#elif ((AT24CXX_MEMADDR_BITS > 8) && (AT24CXX_MEMADDR_BITS <= 16))
void at24cxx_write_byte (uint16_t addr, uint8_t byte)
{
    uint8_t temp;
    
    i2c_start();
    i2c_send_byte(AT24CXX_I2C_ADDR << 1);
    
    temp = addr >> 8;   
    i2c_send_byte(temp);
    temp = addr & 0xFF;
    i2c_send_byte(temp);
    
    i2c_send_byte(byte);
    i2c_stop();
}
#endif


/**
 * \brief 从AT24CXX某个地址中读出字节
 */
#if (AT24CXX_MEMADDR_BITS == 8)
uint8_t at24cxx_read_byte(uint8_t addr)
{
    uint8_t byte;
    
    i2c_start();
    i2c_send_byte(AT24CXX_I2C_ADDR << 1);
    i2c_send_byte(addr);
    i2c_send_byte(addr);
    i2c_start();
    i2c_send_byte((AT24CXX_I2C_ADDR << 1) | 1);
    byte = i2c_read_byte();
    i2c_nack();         /* 主机发送非应答位1 */
    i2c_stop();
    return byte;
}
#elif ((AT24CXX_MEMADDR_BITS > 8) && (AT24CXX_MEMADDR_BITS <= 16))
uint8_t at24cxx_read_byte(uint16_t addr)
{
    uint8_t byte;
    uint8_t temp;
    
    i2c_start();
    i2c_send_byte(AT24CXX_I2C_ADDR << 1);
    
    temp = addr >> 8;
    i2c_send_byte(temp);
    temp = addr & 0xFF;    
    i2c_send_byte(temp);
    
    i2c_start();
    i2c_send_byte((AT24CXX_I2C_ADDR << 1) | 1);
    byte = i2c_read_byte();
    i2c_nack();         /* 主机发送非应答位1 */
    i2c_stop();
    
    return byte;
}    
#endif