/*==============================================================================
文件名：acs71x.c
修改记录：
    - 朱凯飞 2022.02.01 创建
文件说明：ACS71x系列流压转换芯片
==============================================================================*/
#include "includes.h"
#include "acs71x.h"
#include "stc15_adc.h"


/**
 * \brief 获取电流
 *
 * \param[in] ch：STC15单片机自带的AD采集通道
 *
 * \retval 电流值（mA）
 *
 * \该函数基于STC15单片机自带的AD转换，如果要用其他的AD转换方法需要修改代码
 */
float get_current(uint8_t ch)
{
    float cur = 0;
    uint16_t vol = 0;           /* 电压 */
    uint16_t vol_sys = 0;       /* 系统电压 */
    uint8_t i = 0;
    float temp_vol_sys = 0;
    float temp_vol_chan = 0;

    for (i = 0; i < 10; i++) {
        vol_sys += stc15_get_sys_vol();
        vol     += stc15_get_vol(ch);
    }
    temp_vol_sys  = (float)vol_sys / 10;
    temp_vol_chan = (float)vol / 10;

    temp_vol_chan = temp_vol_chan * 2.521;          /* 电阻分压，校准。根据实际情况而定2.521,2.520 */
    
    cur = (temp_vol_chan - temp_vol_sys / 2) / SENSITIVITY;         /* 减掉系统电压的一半，实际上是减掉芯片供电端的一半（芯片特性） */

    return cur;
}
