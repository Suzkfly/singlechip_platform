/*==============================================================================
文件名：mifare_rc522.c
修改记录：
    - 朱凯飞 2018.09.17 创建
文件说明：RC522驱动程序。暂时只有SPI接口
==============================================================================*/
#include "stc15_spi.h"
#include "mifare_rc522.h"
#include "delay.h"

/**
 * \brief 写RC522寄存器
 *
 * \param[in] reg：要写入的寄存器地址
 * \param[in] value：要写入的值
 *
 * \retval 0：成功 -1：失败
 *
 * \note SPI地址字节格式：
 *       B[7] 0：写
 *            1：读
 *       B[6-1] 地址
 *       B[0] RFU，恒为0
 */
static int8_t __write_reg (uint8_t reg, uint8_t value)
{ 
#ifdef RC522_INTERFACE_SF_I2C
    //reg = ((reg << 1) & 0x7E);
    i2c_start();
    if (i2c_send_byte(RC522_I2C_ADDR << 1)) {
        i2c_stop();
        return -1;
    }
        
    if (i2c_send_byte(reg)) {
        i2c_stop();
        return -1;
    }

    if (i2c_send_byte(value)) {
        i2c_stop();
        return -1;
    }
    i2c_stop();
    return 0;
#else 
#ifdef RC522_INTERFACE_SF_SPI
    uint8_t i = 0;

    RC522_SCK = 0;
    RC522_SS = 0;
    reg = ((reg << 1) & 0x7E);

    for (i = 8;i > 0; i--) {
        RC522_MOSI = ((reg & 0x80) == 0x80);
        RC522_SCK = 1;
        reg <<= 1;
        RC522_SCK = 0;
    }

    for (i = 8; i > 0; i--) {
        RC522_MOSI = ((value & 0x80) == 0x80);
        RC522_SCK = 1;
        value <<= 1;
        RC522_SCK = 0;
    }
    RC522_SS = 1;
    RC522_SCK = 1;
#else
    RC522_SS = 0;
    reg = ((reg << 1) & 0x7E);
    stc15_spi_wr_byte(reg);
    stc15_spi_wr_byte(value);
    RC522_SS = 1;
#endif 
    return 0;
#endif
}


/**
 * \brief 读RC522寄存器
 *
 * \param[in] reg：要读取的寄存器地址
 *
 * \retval 寄存器值
 *
 * \note 地址字节格式：
 *       B[7] 0：写；1：读
 *       B[6-1] 地址
 *       B[0] RFU，恒为0
 */
static uint8_t __read_reg (uint8_t reg)
{
#ifdef RC522_INTERFACE_SF_I2C
    uint8_t value = 0;

    //reg = ((reg << 1) & 0x7E) | 0x80;
    i2c_start();           /* 读寄存器先指定寄存器，再读 */
    if (i2c_send_byte(RC522_I2C_ADDR << 1)) {
        i2c_stop();
        return -1;
    }

    if (i2c_send_byte(reg)) {
        i2c_stop();
        return -1; 
    }

    i2c_start(); 
    if (i2c_send_byte((RC522_I2C_ADDR << 1) | 1)) {
        i2c_stop();
        return -1;
    }

    value = i2c_read_byte();
    i2c_nack();     /* 读完字节发送非应答信号 */
    i2c_stop();

    return value;
#else
#ifdef RC522_INTERFACE_SF_SPI

    uint8_t i = 0;
    uint8_t status = 0;

    RC522_SCK = 0;
    RC522_SS = 0;
    reg = ((reg << 1) & 0x7E) | 0x80;

    for (i = 8; i > 0; i--) {
        RC522_MOSI = ((reg & 0x80) == 0x80);
        RC522_SCK = 1;
        reg <<= 1;
        RC522_SCK = 0;
    }

    for (i = 8; i > 0; i--) {
        RC522_SCK = 1;
        status <<= 1;
        status |= (bit)RC522_MISO;
        RC522_SCK = 0;
    }

    RC522_SS = 1;
    RC522_SCK = 1;
#else
    uint8_t status = 0;

    RC522_SS = 0;
    reg = ((reg << 1) & 0x7E) | 0x80;
    stc15_spi_wr_byte(reg);
    status = stc15_spi_wr_byte(0xFF);
    RC522_SS = 1;

#endif
    return status;
#endif
}

/**
 * \brief 设置RC522寄存器位
 *
 * \param[in] reg：要修改的寄存器
 * \param[in] mask：位掩码，为1的位将被置位，其他位保持不变
 */
static void __set_bit_mask (uint8_t reg, uint8_t mask)  
{
    uint8_t tmp = 0;
    tmp = __read_reg(reg);
    __write_reg(reg, tmp | mask);
}

/**
 * \brief 清除RC522寄存器位
 *
 * \param[in] reg：要修改的寄存器
 * \param[in] mask：位掩码，为1的位将被清零，其他位保持不变
 */
static void __clear_bit_mask (uint8_t reg, uint8_t mask)  
{
    uint8_t tmp = 0;
    tmp = __read_reg(reg);
    __write_reg(reg, tmp & (~mask));
} 

/**
 * \brief 延时函数
 */
static void __delay (void)
{
    uint8_t i = 10;
    while (i--);
}

/**
 * \brief 设定定时器的延时时间
 *
 * \param[in] ms：延时时间，单位ms
 */
static void __rc522_set_tmo (uint16_t ms)
{
    uint32_t timereload = 0;
    uint16_t prescaler = 0;
    uint8_t  reg_tmp = 0;

    while (prescaler < 0xFFf) {
        timereload = ((ms * 13560ul) - 1) / (prescaler * 2 + 1);

        if (timereload < 0xFFff) {
            break;
        }
        prescaler++;
    }

    
    __set_bit_mask(RC522_REG_CONTROL, 0x80);              /* 停止定时器 */

    reg_tmp = __read_reg(RC522_REG_TMODE);
    reg_tmp &= ~0x0F;
    reg_tmp |= (prescaler >> 8) & 0x0F;
    __write_reg(RC522_REG_TMODE, reg_tmp);                /* TMODE的第四位为定时器分频器的高四位 */
    __write_reg(RC522_REG_TPRESCALER, prescaler & 0xFF);  /* 设定定时器分频系数 */
    __write_reg(RC522_REG_TRELOADH, timereload >> 8);
    __write_reg(RC522_REG_TRELOADL, timereload & 0xFF);   /* 设定定时器重载值 */
}

/**
 * \brief 关闭天线
 */
static void __pcd_ante_off (void)
{
    __clear_bit_mask(RC522_REG_TXCONTROL, 0x03);
}

/**
 * \brief 开启天线
 */
static void __pcd_ante_on (void)
{
    uint8_t temp = 0;

    temp = __read_reg(RC522_REG_TXCONTROL);
    if (temp & 0x03 != 0x03) {
         __set_bit_mask(RC522_REG_TXCONTROL, 0x03);
    }
}

/**
 * \brief 初始化RC522
 */
void rc522_init (void)
{
#ifdef RC522_INTERFACE_SF_I2C
    i2c_pin_init();
#else
#ifdef RC522_INTERFACE_HW_SPI
    stc15_spi_init();
#endif
#endif

    /* 硬件复位 */
    RC522_NRSTPD = 1;
    __delay();
    RC522_NRSTPD = 0;
    __delay();
    delay_ms(1);               
    RC522_NRSTPD = 1;

    delay_ms(1);               

    __write_reg(RC522_REG_COMMAND, RC522_PCD_RESETPHASE); /* 软件复位 */
    __delay();
    
    __write_reg(RC522_REG_MODE,     0x3D);                /* 定义SIGIN管脚高电平有效；CRC初始值0x6363 */
    
    __rc522_set_tmo(30);                                  /* 设定超时时间为30ms */
    
    __set_bit_mask(RC522_REG_TMODE, 0x80);                /* 发送结束后自动启动定时器 */

    __write_reg(RC522_REG_TXAUTO,   0x40);

    __pcd_ante_off();                                     /* 关开天线 */
    delay_ms(1);
    __pcd_ante_on();
}

/**
 * \brief 通过RC522和ISO14443卡通讯
 *
 * \param[in]  command：RC522命令字
 * \param[in]  p_indata：通过RC522发送到卡片的数据
 * \param[in]  in_len：发送数据的字节长度
 * \param[out] *p_outdata：接收到的卡片返回数据
 * \param[out] *p_out_bits：返回数据的位长度
 *
 * \retval 见RC522状态码
 */
static int8_t __picc_cmd (uint8_t   command, 
                          uint8_t  *p_indata, 
                          uint8_t   in_len,
                          uint8_t  *p_outdata, 
                          uint8_t  *p_out_bits)
{
    uint8_t status = MIFARE_ST_ERROR;
    uint8_t irq_en   = 0x00;
    uint8_t wait = 0x00;
    uint8_t lastbits = 0;
    uint8_t n = 0;
    uint16_t i = 0;
    
    switch (command) {
        case RC522_PCD_AUTHENT:
            irq_en   = 0x12;
            wait = 0x10;
            break;
          
        case RC522_PCD_TRANSCEIVE:
        case RC522_PCD_TRANSMIT:
            irq_en   = 0x77;
            wait = 0x30;
            break;
          
        default:
            break;
    }
   
    __write_reg(RC522_REG_COMIEN, irq_en | 0x80);
    __clear_bit_mask(RC522_REG_COMIRQ, 0x80);
    __write_reg(RC522_REG_COMMAND, RC522_PCD_IDLE);
    __set_bit_mask(RC522_REG_FIFOLEVEL, 0x80);
    
    for (i = 0; i < in_len; i++) {
        __write_reg(RC522_REG_FIFODATA, p_indata[i]);    
    }
    __write_reg(RC522_REG_COMMAND, command);   
    
    if (command == RC522_PCD_TRANSCEIVE) { 
        __set_bit_mask(RC522_REG_BITFRAMING, 0x80);  
    }
    
    i = 6000;/* 根据时钟频率调整，操作M1卡最大等待时间25ms */
    do {
        n = __read_reg(RC522_REG_COMIRQ);
        i--;
    }
    while ((i != 0) && !(n & 0x01) && !(n & wait));
    
    __clear_bit_mask(RC522_REG_BITFRAMING, 0x80);    /* 启动数据发送 */
    
    if (i != 0) {    
        if(!(__read_reg(RC522_REG_ERROR) & 0x1B)) {
            status = MIFARE_ST_OK;
            if (n & irq_en & 0x01) {   
                status = MIFARE_ST_TIMEOUT;   
            }
            if (command == RC522_PCD_TRANSCEIVE) {
                n = __read_reg(RC522_REG_FIFOLEVEL);
                lastbits = __read_reg(RC522_REG_CONTROL) & 0x07;
                if (lastbits) {   
                    *p_out_bits = (n - 1) * 8 + lastbits;   
                } else {   
                    *p_out_bits = n * 8;   
                }
                if (n == 0) {   
                    n = 1;    
                }
                for (i = 0; i < n; i++) {   
                    p_outdata[i] = __read_reg(RC522_REG_FIFODATA);    
                }
           }
        } else {   
            status = MIFARE_ST_ERROR;   
        }
   }

   __set_bit_mask(RC522_REG_CONTROL, 0x80);           /* 立即停止定时器 */
   __write_reg(RC522_REG_COMMAND, RC522_PCD_IDLE);    /* 停止命令 */
   
   return status;
}


/**
 * \brief 用RC522计算CRC16
 *
 * \param[in] p_indat：要计算CRC的数据
 * \param[in] len：要计算CRC的数据的长度
 * \param[out] p_crc16：计算结果
 */
static void __calulate_crc (uint8_t *p_indat, uint8_t len, uint8_t p_crc16[2])
{
    uint8_t i = 0, n = 0;

    __clear_bit_mask(RC522_REG_DIVIRQ, 0x04);
    __write_reg(RC522_REG_COMMAND, RC522_PCD_IDLE);
    __set_bit_mask(RC522_REG_FIFOLEVEL, 0x80);
    for (i = 0; i < len; i++) {   
        __write_reg(RC522_REG_FIFODATA, *(p_indat + i));   
    }
    __write_reg(RC522_REG_COMMAND, RC522_PCD_CALCCRC);
    i = 0xFF;
    do {
        n = __read_reg(RC522_REG_DIVIRQ);
        i--;
    }
    while ((i != 0) && !(n & 0x04));

    p_crc16[0] = __read_reg(RC522_REG_CRCRESULTL);
    p_crc16[1] = __read_reg(RC522_REG_CRCRESULTM);
}


/**
 * \brief 寻卡，得到卡类型
 */
int8_t rc522_picca_request (uint8_t req_mode, uint8_t *p_atq)
{
    uint8_t status = 0;  
    uint8_t uidlen = 0;
    xdata uint8_t sendbuf[1] = { 0 }; 
    
    __clear_bit_mask(RC522_REG_STATUS2, 0x08);
    __write_reg(RC522_REG_BITFRAMING,   0x07);
    __set_bit_mask(RC522_REG_TXCONTROL, 0x03);
    sendbuf[0] = req_mode;

    status = __picc_cmd(RC522_PCD_TRANSCEIVE, 
                        sendbuf, 
                        sizeof(sendbuf), 
                        p_atq, 
                        &uidlen);
      
    return status;
}

/**
 * \brief 防冲撞
 */
int8_t rc522_picca_anticoll (uint8_t p_uid[4])
{
    int8_t  status = 0;
    uint8_t i, uid_check = 0;
    uint8_t uidlen = 0;
    xdata uint8_t buf[7] = { 0 }; 
    
    __clear_bit_mask(RC522_REG_STATUS2, 0x08);
    __write_reg(RC522_REG_BITFRAMING,   0x00);
    __clear_bit_mask(RC522_REG_COLL,    0x80);
 
    buf[0] = PICCA_ANTICOLL1;
    buf[1] = 0x20;

    status = __picc_cmd(RC522_PCD_TRANSCEIVE, buf, 2, buf, &uidlen);

    if (status == MIFARE_ST_OK) {
    	 for (i = 0; i < 4; i++) {   
             *(p_uid + i)  = buf[i];
             uid_check ^= buf[i];
         }
         if (uid_check != buf[i]) {   
            status = MIFARE_ST_ERROR;    
         }
    }
    
    __set_bit_mask(RC522_REG_COLL, 0x80);

    return status;
}


/**
 * \brief Mifare卡的选择操作
 */
int8_t rc522_picca_select (uint8_t p_uid[4], uint8_t p_sak[1])
{
    int8_t status = 0;
    uint8_t i = 0;
    uint8_t sak_len = 0;
    uint8_t buf[9] = { 0 }; 
    
    buf[0] = PICCA_ANTICOLL1;
    buf[1] = 0x70;
    buf[6] = 0;

    for (i = 0; i < 4; i++) {
    	buf[i + 2] = *(p_uid + i);
    	buf[6] ^= *(p_uid + i);
    }
    __calulate_crc(buf, 7, &buf[7]);
  
    __clear_bit_mask(RC522_REG_STATUS2, 0x08);

    status = __picc_cmd(RC522_PCD_TRANSCEIVE, buf, 9, buf, &sak_len);
    
    if ((status == MIFARE_ST_OK) && (sak_len == 0x18)) {   
        *p_sak = buf[0];
        status = MIFARE_ST_OK; 
    } else {   
        status = MIFARE_ST_ERROR;   
    }

    return status;
}

/**
 * \brief Mifare卡的挂起操作，使所选择的卡进入HALT状态
 */
int8_t rc522_picca_halt (void)
{
    int8_t status = 0;
    uint8_t unlen = 0;
    xdata uint8_t buf[4] = { 0 }; 

    buf[0] = PICCA_HALT;
    buf[1] = 0;

    __calulate_crc(buf, 2, &buf[2]);
 
    status = __picc_cmd(RC522_PCD_TRANSCEIVE, buf, 4, buf, &unlen);

    return status;
}

/**
 * \brief Mifare卡密钥验证，将传入的密钥与卡的密钥进行验证
 */
int8_t rc522_picca_authent (uint8_t            key_type,
                            const uint8_t      p_uid[4],
                            const uint8_t      p_key[6],
                            uint8_t            nblock)
{
    int8_t status = 0;
    uint8_t unlen = 0;
    uint8_t i = 0;
    xdata uint8_t buf[18] = { 0 }; 

    buf[0] = key_type;
    buf[1] = nblock;
    for (i = 0; i < 6; i++) {    
        buf[i + 2] = *(p_key + i);  
    }
    for (i = 0; i < 6; i++) {    
        buf[i + 8] = *(p_uid + i);   
    }
    
    status = __picc_cmd(RC522_PCD_AUTHENT, buf, 12, buf, &unlen);
    if ((status != MIFARE_ST_OK) || (!(__read_reg(RC522_REG_STATUS2) & 0x08))) {   
        status = MIFARE_ST_ERROR;   
    }
    
    return status;
}

/**
 * \brief Mifare卡数据读取
 */
int8_t rc522_picca_read(uint8_t nblock, uint8_t p_data[16])
{
    int8_t status = 0;
    uint8_t unlen = 0;
    uint8_t i = 0;
    xdata uint8_t buf[18] = { 0 }; 

    buf[0] = PICCA_READ;
    buf[1] = nblock;

    __calulate_crc(buf, 2, &buf[2]);
   
    status = __picc_cmd(RC522_PCD_TRANSCEIVE, buf, 4, buf, &unlen);
    if ((status == MIFARE_ST_OK) && (unlen == 0x90)) {
        for (i = 0; i < 16; i++) {    
            *(p_data + i) = buf[i];  
        }
    } else {   
        status = MIFARE_ST_ERROR;   
    }
    
    return status;
}

/**
 * \brief Mifare卡写数据，写之前必需成功进行密钥验证。
 */
int8_t rc522_picca_write (uint8_t nblock, const uint8_t p_buf[16])
{
    int8_t status = 0;
    uint8_t unlen = 0;
    uint8_t i = 0;
    xdata uint8_t buf[18] = { 0 }; 
    
    buf[0] = PICCA_WRITE;
    buf[1] = nblock;

    __calulate_crc(buf, 2, &buf[2]);
 
    status = __picc_cmd(RC522_PCD_TRANSCEIVE, buf, 4, buf, &unlen);

    if ((status != MIFARE_ST_OK) || (unlen != 4) || ((buf[0] & 0x0F) != 0x0A))  {   
        status = MIFARE_ST_ERROR;  
    }
        
    if (status == MIFARE_ST_OK) {
        for (i = 0; i < 16; i++) {    
            buf[i] = *(p_buf + i);  
        }

        __calulate_crc(buf, 16, &buf[16]);

        status = __picc_cmd(RC522_PCD_TRANSCEIVE, buf, 18, buf, &unlen);
        if ((status != MIFARE_ST_OK) || (unlen != 4) || ((buf[0] & 0x0F) != 0x0A)) {   
            status = MIFARE_ST_ERROR;   
        }
    }
    
    return status;
}

/**
 * \brief Mifare值操作，设置值块的值
 *
 * \param[in] p_dev      : fm175xx设备
 * \param[in] nblock     : 读取数据的值块地址
 * \param[in] value      : 设置的值
 *
 * \retval 操作结果(详细说明见rc522.h中的状态码定义)
 *
 * \note 该函数使用直接写数据的方式，将指定的块格式转换为数值块，并初始化数值块的值，也可以使用该函数改
 *       变数值块的值。对卡内某一块进行验证成功后，并且访问条件允许才能进行此操作。
 */
//int8_t rc522_picca_val_set (uint8_t nblock, int32_t value)
//{
//    xdata uint8_t buf[16] = { 0 };
//
//    buf[0] = value & 0xFF;
//    buf[1] = (value >> 8) & 0xFF;
//    buf[2] = (value >> 16) & 0xFF;
//    buf[3] = (value >> 24) & 0xFF;
//    buf[8] = value & 0xFF;
//    buf[9] = (value >> 8) & 0xFF;
//    buf[10] = (value >> 16) & 0xFF;
//    buf[11] = (value >> 24) & 0xFF;
//    value = ~value;
//    buf[4] = value & 0xFF;
//    buf[5] = (value >> 8) & 0xFF;
//    buf[6] = (value >> 16) & 0xFF;
//    buf[7] = (value >> 24) & 0xFF;
//
//    buf[14] = buf[12] = nblock;
//    buf[15] = buf[13] = ~nblock;
//
//    return rc522_picca_write(nblock, buf);
//}
/**
 * \brief Mifare值操作，获取值块的值
 */
//int8_t rc522_picca_val_get (uint8_t nblock, int32_t *p_value)
//{
//    uint8_t addr = 0, addr_b = 0;
//    int32_t value = 0, value_b = 0;
//    uint8_t result = 0;
//    uint8_t *p_temp = NULL;
//    xdata uint8_t buf[16] = { 0 };
//
//    result = rc522_picca_read(nblock, buf);
//    if (result != MIFARE_ST_OK) {
//        return result;
//    }
//
//    value = buf[3];
//    value <<= 8;
//    value |= buf[2];
//    value <<= 8;
//    value |= buf[1];
//    value <<= 8;
//    value |= buf[0];
//
//    value_b = buf[7];
//    value_b <<= 8;
//    value_b |= buf[6];
//    value_b <<= 8;
//    value_b |= buf[5];
//    value_b <<= 8;
//    value_b |= buf[4];
//
//    if (value != ~value_b) {
//        return MIFARE_ST_INVALID_VALUE;
//    }
//
//    value = buf[11];
//    value <<= 8;
//    value |= buf[10];
//    value <<= 8;
//    value |= buf[9];
//    value <<= 8;
//    value |= buf[8];
//
//    if (value != ~value_b) {
//        return MIFARE_ST_INVALID_VALUE;
//    }
//    addr = buf[12];
//    if ((addr & 0xFF) != (nblock & 0xFF)) {
//        return MIFARE_ST_INVALID_VALUE;
//    }
//
//    addr_b = buf[13];
//    if ((addr_b & 0xFF) != ((~nblock) & 0xFF)) {
//        return MIFARE_ST_INVALID_VALUE;
//    }
//
//    addr = buf[14];
//    if ((addr & 0xFF) != (nblock & 0xFF)) {
//       return MIFARE_ST_INVALID_VALUE;
//    }
//
//    addr_b = buf[15];
//    if ((addr_b & 0xFF) != ((~nblock) & 0xFF)) {
//        return MIFARE_ST_INVALID_VALUE;
//    }
//
//    p_temp = (uint8_t *)p_value;
//    p_temp[0] = buf[3];
//    p_temp[1] = buf[2];
//    p_temp[2] = buf[1];
//    p_temp[3] = buf[0];
//
//    return result;
//}

/**
 * \brief Mifare值操作，对Mifare卡的值块进行加减操作
 */
//int8_t rc522_picca_val_operate (uint8_t            mode,
//                                uint8_t            nblock,
//                                uint8_t            ntransblk,
//                                int32_t            value)
//{
//    int8_t status = 0;
//    uint8_t unlen = 0;
//    xdata uint8_t buf[6] = { 0 };
//
//    buf[0] = mode;
//    buf[1] = nblock;
//
//    __calulate_crc(buf, 2, &buf[2]);
// 
//    status = __picc_cmd(RC522_PCD_TRANSCEIVE, buf, 4, buf, &unlen);
//
//    if ((status != MIFARE_ST_OK) || ((buf[0] & 0x0F) != 0x0A)) {
//        return MIFARE_ST_ERROR;
//    }
//    if (status == MIFARE_ST_OK) {
//        buf[0] = value & 0xFF;
//        buf[1] = (value >> 8)  & 0xFF;
//        buf[2] = (value >> 16) & 0xFF;
//        buf[3] = (value >> 24) & 0xFF;
//
//        __calulate_crc(buf, 4, &buf[4]);
//
//       status = __picc_cmd(RC522_PCD_TRANSMIT, buf, 6, buf, &unlen);
//
//        if (status != MIFARE_ST_OK) {
//            return status;
//        }
//    }
//
//    if (status == MIFARE_ST_OK) {
//        buf[0] = PICCA_TRANSFER;
//        buf[1] = ntransblk;
//
//        __calulate_crc(buf, 2, &buf[2]);
//
//        status = __picc_cmd(RC522_PCD_TRANSCEIVE, buf, 4, buf, &unlen);
//
//        if ((buf[0] & 0x0F) != 0x0A) {
//            return MIFARE_ST_ERROR;
//        }
//    }
//    return status;
//}
