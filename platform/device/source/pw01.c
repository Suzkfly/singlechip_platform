/*==============================================================================
文件名：pw01.c
修改记录：
    - 朱凯飞 2018.10.03 创建
文件说明：pw01蓝牙模块串口透传包接收程序
==============================================================================*/
#include "pw01.h"
#include "delay_work.h"
#include "stc15_uart2.h"

/* 定义消息缓冲区 */
static xdata uint8_t __g_msg_buf[PW01_MAX_PACK_CNT][PW01_MAX_MSG_LEN];

/* 准备接收新包的标识 */
static volatile uint8_t __g_new_pack_flag = 1;

/* 正准备接收的包序号 */
static volatile uint8_t __g_cur_pack = 0;

/* 定义超时处理延迟作业 */
static delay_work_reg_t  __g_tmo_work;

/* 超时判定是否开启的标识，为1表示进行超时判定 */
static xdata volatile uint8_t __g_tmo_det = 0; 

/* 剩余超时判定个数，减到0表示超时 */
static xdata volatile uint8_t __g_tmo_cnt = 2;

static uint8_t __g_dat_cnt = 0;
static uint8_t __g_cur_pack_len = 0;    /* 本包数据应该收到的数据个数 */

/**
 * \brief 超时处理函数
 */
void timeout_handler (void *p_arg)
{       
    if (__g_tmo_det == 1) {         /* 开启了超时判定 */
        __g_tmo_cnt--;
        if (__g_tmo_cnt == 0) {     /* 判定为超时 */
            __g_tmo_det = 0;
            __g_dat_cnt = 0;
            __g_cur_pack_len = 0;
            __g_new_pack_flag = 1;
        }
    }
    delay_work_register(&__g_tmo_work, timeout_handler, p_arg, 5);
}


/**
 * \brief PW01模块初始化
 */
void pw01_init (void)
{
    stc15_uart2_init(9600);

    delay_work_init();
    delay_work_register(&__g_tmo_work, 
                        timeout_handler, 
                        (void *)timeout_handler, 
                        5);
    delay_work_start();
}

/**
 * \brief 获取数据
 *
 * \param[out] pp_pack：保存数据的地址
 *
 * \retval 获取到的数据包个数
 */
uint8_t pw01_get_pack (uint8_t pp_pack[PW01_MAX_PACK_CNT][PW01_MAX_MSG_LEN])
{
    uint8_t i, j;
    uint8_t pack_cnt;

    while (__g_new_pack_flag == 0);    /* 等待当前包中数据获取完毕 */

    pack_cnt = __g_cur_pack;

    for (i = 0; i < pack_cnt; i++) {
        for (j = 0; j < __g_msg_buf[i][2] + 4; j++) {
            pp_pack[i][j] = __g_msg_buf[i][j];
        }
    }

    __g_cur_pack = 0;

    return pack_cnt;
}

/**
 * \brief 对数据包进行和校验
 *
 * \param[in] p_pack：需要校验的数据地址
 *
 * \retval 0：校验通过 1：校验错误
 */
int8_t pw01_sum_check (uint8_t *p_pack)
{
    uint8_t sum = 0;
    uint8_t i;

    for (i = 0; i < p_pack[2] + 3; i++) {
        sum += p_pack[i];
    }

    if (sum == p_pack[i]) {
        return 0;
    }

    return -1;
}



/**
 * \brief 串口2中断函数 
 */
void stc15_uart2_interrupt (void) interrupt 8
{
    if (S2CON & S2CON_S2RI) {
        S2CON &= ~S2CON_S2RI;

        __g_tmo_cnt = 2;                /* 只要收到数据就重置超时判定时间 */

        if (__g_new_pack_flag == 1) {
            if (S2BUF == PW01_HEAD) {
                __g_new_pack_flag = 0;
                __g_tmo_det = 1;        /* 开启超时判定 */
            }
        } else {
            __g_msg_buf[__g_cur_pack][__g_dat_cnt++] = S2BUF;

            if (__g_dat_cnt == 3) {
                 __g_cur_pack_len = __g_msg_buf[__g_cur_pack][2];
            } else if (__g_dat_cnt == __g_cur_pack_len + 4) {     /* 接收完一包数据 */
                if (__g_msg_buf[__g_cur_pack][1] != 0x00) {
                    __g_cur_pack++;                       /* 不是心跳包才加包 */
                }
                __g_tmo_det = 0;                          /* 关闭超时判定 */
                __g_dat_cnt = 0;
                __g_cur_pack_len = 0;
                __g_new_pack_flag = 1;
            }
        }
    }
}