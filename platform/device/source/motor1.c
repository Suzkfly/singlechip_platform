/*==============================================================================
文件名：motor1.c
修改记录：
    - 朱凯飞 2018.10.23 创建
文件说明：直流电机驱动程序
==============================================================================*/
#include "motor1.h"

/**
 * \brief 前进
 */
void motor1_forward (void)
{       
    MOTOR1_INA = 1;
    MOTOR1_INB = 0;
}

/**
 * \brief 后退
 */
void motor1_back (void)
{       
    MOTOR1_INA = 0;
    MOTOR1_INB = 1;
}

/**
 * \brief 滑行
 */
void motor1_skidding (void)
{       
    MOTOR1_INA = 0;
    MOTOR1_INB = 0;
}

/**
 * \brief 刹车
 */
void motor1_brake (void)
{       
    MOTOR1_INA = 1;
    MOTOR1_INB = 1;
}
