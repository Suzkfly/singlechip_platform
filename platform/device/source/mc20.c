/*==============================================================================
文件名：mc20.c
修改记录：
    - 朱凯飞 2018.09.27 创建
文件说明：MC20驱动程序 。 本模块使用了延迟作业（定时器1）串口2（定时器2）（P）用于
          收发AT指令。
==============================================================================*/
#include "mc20.h"
#include "stc15_uart2.h"
#include "stc15_uart3.h"
#include "string.h"
#include "delay_work.h"

/* MC20波特率初始化完成标识，为0表示未完成，为1表示已完成 */
static xdata uint8_t __g_mc20_baud_init_finish_flag = 0;

/* AT消息盒子（一个"\r\n"作为一条消息的结尾，一行只有"\r\n"无效 */
static xdata uint8_t __g_at_msg[MAX_AT_MSG_CNT][MAX_AT_MSG_LEN] = { 0 };

/* 当前正在（正准备）接收的消息序号 */
static xdata uint8_t __g_msg_num = 0;

/* 本条消息已经接收到的字节数 */
static xdata uint8_t __g_dat_len = 0;

/* 接收超时剩余时间，当该值减到0时表接收超时 */
static xdata uint16_t __g_rcv_timer_cnt;

/* 接收超时标识，为1表示接收超时 */
static xdata uint8_t __g_rcv_tmo_flag = 0;

/* 定义需要注册的任务 */
static xdata delay_work_reg_t __g_send_at_work;           /* 上点前10秒每秒发送一次"AT\r\n"用于稳定模块波特率 */
static xdata delay_work_reg_t __g_get_msg_work;           /* 持续获取串口消息 */
static xdata delay_work_reg_t __g_if_rcv_tmo_work;        /* 判断串口接收是否超时 */
static xdata delay_work_reg_t __g_tcp_init_work;          /* TCP初始化任务 */
                                                        
/* 异步发送AT指令 */
void mc20_send_at_cmd_async (const uint8_t *p_cmd)
{
    stc15_uart2_sendstr(p_cmd);
}

/* 持续发送AT指令任务 */
static void __send_at_task (void *p_arg)
{
    if ((uint8_t)p_arg < 10) {
        mc20_send_at_cmd("AT\r\n");

        delay_work_register(&__g_send_at_work,     /* 注册延迟作业，持续发送消息 */
                             __send_at_task, 
                             (void *)((uint8_t)p_arg + 1), 
                             1000);  
    } else {
        __g_mc20_baud_init_finish_flag = 1;
    }
}

/**
 * \brief MC20 TCP初始化任务
 */
void mc20_tcp_init_task (void *p_arg)
{

    mc20_send_at_cmd("ATE1");
    delay_work_register(&__g_tcp_init_work, mc20_tcp_init_task, NULL, 10);    /* TCP初始化任务 */

}

/* 持续获取消息任务 */
static void __get_msg_task (void *p_arg)
{
    uint8_t msg_cnt;    /* 获取到的消息数量 */
    uint8_t *pp_msg[MAX_AT_MSG_CNT];
    uint8_t i;

    msg_cnt = mc20_get_msg(pp_msg);
    for (i = 0; i < msg_cnt; i++) {
        stc15_uart3_sendbyte('*');
        stc15_uart3_sendbyte(i + '1');
        stc15_uart3_sendstr(pp_msg[i]);
        stc15_uart3_sendbyte('#');
    }

    delay_work_register(&__g_get_msg_work, __get_msg_task, 0, 500);          /* 注册延迟作业，持续获取消息 */
}

/* 该任务用于判断接收消息是否超时 */
/* 这么做的目的是，如果某条消息没有接收完全就超时了，那么它也会作为一条独立的消息被获取到 */
static void __if_rcv_tmo_task (void *p_arg)
{
    if (__g_rcv_timer_cnt > 0) {
        __g_rcv_timer_cnt--;
    }

    if (__g_rcv_timer_cnt == 0) {
        if (__g_dat_len != 0) {
            __g_dat_len = 0;
            __g_rcv_tmo_flag = 1;   /* 超时标识置位 */ 
        }
        __g_rcv_timer_cnt = RCV_TMOx10MS;          
    }

    delay_work_register(&__g_if_rcv_tmo_work, __if_rcv_tmo_task, NULL, 10);    /* 注册延迟作业，判断接收是否超时，10ms检查一次 */
}

/**
 * \brief mc20初始化
 *
 * \note 由于mc20使用自动波特率，因此在使用前应先等待模块波特率匹配成功。具体做法为
 *       每隔1秒发送一次"AT\r\n"，直到收到10个"OK\r\n"为止 
 */
void mc20_init (void)
{
    __g_mc20_baud_init_finish_flag = 0;

    stc15_uart2_init(9600); /* 使用串口2发送AT指令 */
   
    delay_work_init();
    delay_work_register(&__g_send_at_work, __send_at_task, (void *)0, 1000);  /* 注册延迟作业，连续发送10条"AT\r\n" */
    delay_work_register(&__g_get_msg_work, __get_msg_task, NULL, 500);         /* 注册延迟作业，持续获取消息 */

    /*_get_msg_task的调用时间一定要是if_rcv_tmo_task的倍数，且先于if_rcv_tmo_task注册 */
    delay_work_register(&__g_if_rcv_tmo_work, __if_rcv_tmo_task, NULL, 10);    /* 注册延迟作业，判断接收是否超时，10ms检查一次 */
    delay_work_start();
}

/**
 * \brief 检查mc20是否初始化完成
 */
uint8_t mc20_baud_init_is_finished (void)
{
    return __g_mc20_baud_init_finish_flag;
}

/**
 * \brief 获取mc20接收到的消息  
 */
uint8_t mc20_get_msg (uint8_t *pp_msg[MAX_AT_MSG_CNT])
{
    uint8_t msg_num_temp = __g_msg_num;
    uint8_t i;

    /* 如果某条消息超时了，那么也将它作为一条消息获取到 */
    if ((__g_rcv_tmo_flag == 1) && (__g_dat_len != 0) && (msg_num_temp < MAX_AT_MSG_CNT)) {
        __g_rcv_tmo_flag = 0;
        msg_num_temp++;
    }

    for (i = 0; i < msg_num_temp; i++) {
        pp_msg[i] = __g_at_msg[i];
    }

    if (__g_msg_num > 0) {
        __g_msg_num = 0;
    }

    return msg_num_temp;
}

/**
 * \brief 串口2中断函数 
 */
void stc15_uart2_interrupt (void) interrupt 8
{
    if (S2CON & S2CON_S2RI) {
        S2CON &= ~S2CON_S2RI;

        __g_rcv_timer_cnt = RCV_TMOx10MS; /* 每次接收到数据则重置超时时间 */
        __g_rcv_tmo_flag = 0;

        /* 保存数据到消息盒子中 */
        if (__g_dat_len < MAX_AT_MSG_LEN) {
            __g_at_msg[__g_msg_num][__g_dat_len] = S2BUF;
        } else {
           if (__g_msg_num < MAX_AT_MSG_CNT) {
               __g_msg_num++;
               __g_dat_len = 0;
               __g_at_msg[__g_msg_num][__g_dat_len] = S2BUF;
           }
        }

        if (__g_at_msg[__g_msg_num][__g_dat_len] == '\n') {  /* 接收到行尾 */
            if (__g_dat_len == 0) {                          /* 其实是上一条消息的行尾 */
                 /* 什么也不做（这样将导致上条消息末尾不是"\r\n"） */
            } else {
                if (__g_at_msg[__g_msg_num][__g_dat_len - 1] == '\r') {   
                    if (__g_dat_len == 1) {    /* 接收到空行 */
                        __g_dat_len = 0;
                    } else {         /* 接收完一行数据 */
                        __g_at_msg[__g_msg_num][__g_dat_len + 1] = '\0'; /* 末尾补0 */
                        __g_dat_len = 0;
                        if (__g_msg_num < MAX_AT_MSG_CNT) {
                            __g_msg_num++;
                        }
                    }
                }     
            }
        } else {
            __g_dat_len++;
        }
    }
}