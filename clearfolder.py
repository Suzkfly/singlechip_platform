
import os
import shutil


DIR_NAME = 'output'

RETAIN_FILE = ['.uvproj', '.Uv2']

rootdir = os.path.abspath('.')

def delete_appointed_folder():  
    for item in os.listdir('.'):
        if os.path.isdir(item):
            if item == DIR_NAME:        #remove output folder
                print os.path.relpath(os.path.abspath('.'), rootdir)
                shutil.rmtree(DIR_NAME)
            elif item == 'project':     #project folder just retain jroject files
                os.chdir(item)
                for fm in os.listdir('.'):
                    (filename, extension) = os.path.splitext(fm)
                    if extension not in RETAIN_FILE:
                        os.remove(fm)
                os.chdir('..')
            else:
                os.chdir(item)
                delete_appointed_folder();
                os.chdir('..')

delete_appointed_folder()

print ('remove succeed')
